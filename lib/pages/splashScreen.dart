import 'package:flutter/material.dart';
import 'package:conductor/SizeConfig.dart';

import 'usuario/login.dart';


class SplashScreenPage extends StatefulWidget {
  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreenPage> {
  @override
  void initState() {
    Future.delayed(
        Duration(seconds: 5),
            () => Navigator.pushReplacement(
          context, MaterialPageRoute(builder: (context) => LoginScreen()),
        ));
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return Scaffold(
      body: SafeArea(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Spacer(),
            Center(
              child: FractionallySizedBox(
                  widthFactor: SizeConfig.safeBlockHorizontal * 0.13, child: Image.asset('assets/logos/logoPrincipal.png')),
            ),
            Spacer(),
            CircularProgressIndicator(
              backgroundColor: Colors.black12,
            ),
            SizedBox(height: 20),
            Text(
                'Bienvenido',
                style: TextStyle(
                  color: Colors.black26,
                  fontSize: 18.0,
                ),
            ),
            SizedBox(height: 20),
          ],
        ),
      ),
    );
  }
}
