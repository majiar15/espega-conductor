import 'dart:convert';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:http/http.dart' as http;
import 'package:conductor/helpers/AppData.dart';
import 'package:conductor/pages/usuario/VerificarHoraServicio.dart';
import 'package:conductor/pages/usuario/serviciosUsuario.dart';

import '../../SizeConfig.dart';
import '../componentes.dart';

class ListarServicioScreen extends StatefulWidget {
  @override
  _ListarServicioScreenState createState() => _ListarServicioScreenState();
}

class _ListarServicioScreenState extends State<ListarServicioScreen> {


  String _nombre;

  TextEditingController _description = TextEditingController();


  void initState() {
    // TODO: implement initState
    super.initState();
  }

  Future obtenerUsuario() async {
    Map parametros = ModalRoute.of(context).settings.arguments;
    final url = "http://transporte-paciente.herokuapp.com/api/conductor/login";
    var data = {
      "username": parametros['username'],
      "password": parametros['password'],
    };
    final response = await http.post(url, body: data);
    return jsonDecode(response.body);
  }

  Future<void> showMyDialogSignOut() async {
    return showDialog<void>(
      context: context,
      barrierDismissible: false, // user must tap button!
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text(
            'Cuenta',
            style: TextStyle(
              color: Colors.grey[700],
              fontFamily: 'OpenSans',
            ),
          ),
          content: SingleChildScrollView(
            child: ListBody(
              children: <Widget>[
                Text(
                  'Quieres cerrar sesion ?',
                  style: TextStyle(
                    color: Colors.grey,
                    fontFamily: 'OpenSans',
                  ),
                ),
              ],
            ),
          ),
          actions: <Widget>[
            TextButton(
              child: Text(
                'Si',
                style: TextStyle(
                  color: Colors.grey[600],
                  fontFamily: 'OpenSans',
                ),
              ),
              onPressed: () {

                stopActualizarServicio();
                stopVerificarHoraServicio();
                appData.timer?.cancel();

                Navigator.of(context).pushNamedAndRemoveUntil(
                    "/login", (Route<dynamic> route) => false);
              },
            ),
            TextButton(
              child: Text(
                'No',
                style: TextStyle(
                  color: Colors.grey[600],
                  fontFamily: 'OpenSans',
                ),
              ),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }

  Future<void> _showMyDialogCampos() async {
    return showDialog<void>(
      context: context,
      barrierDismissible: false, // user must tap button!
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text(
            'Error',
            style: TextStyle(
              color: Colors.grey[600],
              fontFamily: 'OpenSans',
              fontSize: SizeConfig.safeBlockHorizontal * 5.5,
            ),
          ),
          content: SingleChildScrollView(
            child: ListBody(
              children: <Widget>[
                Text(
                  'Campo "porque" vacio.',
                  style: TextStyle(
                    color: Colors.grey,
                    fontFamily: 'OpenSans',
                    fontSize: SizeConfig.safeBlockHorizontal * 4,
                  ),
                ),
              ],
            ),
          ),
          actions: <Widget>[
            TextButton(
              child: Text(
                'Aceptar',
                style: TextStyle(
                  color: Colors.grey[600],
                  fontFamily: 'OpenSans',
                ),
              ),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }

  void Rechazado() async {

    if(_description.text.isEmpty){
      _showMyDialogCampos();
    }else{
      Map parametros = ModalRoute.of(context).settings.arguments;
      var url = "http://transporte-paciente.herokuapp.com/api/conductor/servicios/rejected";

      var data = {
        "id_horario": parametros['id_horario'],
        "id_conductor": parametros['id'],
        "anotacion": _description.text,
        "username": parametros['username']
      };

      final response = await http.post(url, body: data);
      var cont = jsonDecode(response.body);

      if(cont['msj'] ==  "Registro guardado"){
        showDialog(
          context: context,
          barrierDismissible: false,
          builder: (BuildContext context) {
            return Container(
              width: SizeConfig.safeBlockHorizontal * 100,
              height: SizeConfig.blockSizeVertical * 100,
              child: Center(
                  child: CircularProgressIndicator()
              ),
            );
          },
        );
        new Future.delayed(new Duration(seconds: 3), () {
          Navigator.pop(context);//pop dialog
          _showMyDialogRechazo();
        });
      }else{
      }

    }

  }

  Future<void> _showMyDialogRechazo() async {
    return showDialog<void>(
      context: context,
      barrierDismissible: false, // user must tap button!
      builder: (BuildContext context) {
        Future.delayed(Duration(seconds: 3), () {
          Navigator.of(context).pop(false);
          pasarInicio();
        });
        return AlertDialog(
          title: Text(
            'Servicio',
            style: TextStyle(
              color: Colors.grey[600],
              fontFamily: 'OpenSans',
            ),
          ),
          content: SingleChildScrollView(
            child: ListBody(
              children: <Widget>[
                Text(
                  'Servicio Rechazado exitosamente',
                  style: TextStyle(
                    color: Colors.grey,
                    fontFamily: 'OpenSans',
                  ),
                ),
              ],
            ),
          ),
        );
      },
    );
  }

  void Realizado() async {

      Map parametros = ModalRoute.of(context).settings.arguments;
      var url = "http://transporte-paciente.herokuapp.com/api/conductor/servicios/done";

      var data = {
        "id_horario": parametros['id_horario'],
        "id_conductor": parametros['id'],
        "anotacion": "",
        "username": parametros['username']
      };

      final response = await http.post(url, body: data);
      var cont = jsonDecode(response.body);

      if(cont['msj'] ==  "Registro guardado"){
        showDialog(
          context: context,
          barrierDismissible: false,
          builder: (BuildContext context) {
            return Container(
              width: SizeConfig.safeBlockHorizontal * 100,
              height: SizeConfig.blockSizeVertical * 100,
              child: Center(
                  child: CircularProgressIndicator()
              ),
            );
          },
        );
        new Future.delayed(new Duration(seconds: 3), () {
          Navigator.pop(context);//pop dialog
          _showMyDialogRealizado();
        });
      }else{
      }

  }

  Future<void> _showMyDialogRealizado() async {
    return showDialog<void>(
      context: context,
      barrierDismissible: false, // user must tap button!
      builder: (BuildContext context) {
        Future.delayed(Duration(seconds: 3), () {
          Navigator.of(context).pop(false);
          pasarInicio();
        });
        return AlertDialog(
          title: Text(
            'Servicio',
            style: TextStyle(
              color: Colors.grey[600],
              fontFamily: 'OpenSans',
            ),
          ),
          content: SingleChildScrollView(
            child: ListBody(
              children: <Widget>[
                Text(
                  'Servicio Realizado exitosamente',
                  style: TextStyle(
                    color: Colors.grey,
                    fontFamily: 'OpenSans',
                  ),
                ),
              ],
            ),
          ),
        );
      },
    );
  }

  Widget _buildDescripcionTF() {
    return Container(
      alignment: Alignment.centerLeft,
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.circular(SizeConfig.safeBlockHorizontal * 4),
          border: Border.all(
              width: 1,
              color: Colors.grey[300]
          )
      ),
      child: new TextField(
        maxLines: null,
        keyboardType: TextInputType.multiline,
        controller: _description,
        style: TextStyle(
          color: Colors.black38,
          fontFamily: 'OpenSans',
        ),
        decoration: new InputDecoration(
          border: InputBorder.none,
          contentPadding: EdgeInsets.only(bottom: SizeConfig.safeBlockVertical * 4, left: SizeConfig.safeBlockHorizontal * 3),
          hintText: 'Porque',
          hintStyle: TextStyle(
            color: Colors.black26,
            fontFamily: 'Opensans',
            fontSize: SizeConfig.safeBlockHorizontal * 4,
          ),
        ),
      ),
    );
  }

  Widget buildRechazadoBtn() {
    return Container(
      child: RaisedButton(
        elevation: 0.0,
        onPressed: () {
          Rechazado();
        },
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(SizeConfig.safeBlockHorizontal * 10),
        ),
        color: Colors.blue[200],
        child: Text(
          'Rechazado',
          style: TextStyle(
            color: Colors.white,
            letterSpacing: 1.0,
            fontSize: SizeConfig.safeBlockHorizontal * 4,
            fontWeight: FontWeight.bold,
            fontFamily: 'OpenSans',
          ),
        ),
      ),
    );
  }

  Widget buildAtendidoBtn() {
    return Container(
      child: RaisedButton(
        elevation: 0.0,
        onPressed: () {
          Realizado();
        },
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(SizeConfig.safeBlockHorizontal * 10),
        ),
        color: Colors.blue[500],
        child: Text(
          'Realizado',
          style: TextStyle(
            color: Colors.white,
            letterSpacing: 1.0,
            fontSize: SizeConfig.safeBlockHorizontal * 4.5,
            fontWeight: FontWeight.bold,
            fontFamily: 'OpenSans',
          ),
        ),
      ),
    );
  }

  Widget buildSocialBtn() {
    return GestureDetector(
      onTap: () {
      },
      child: Container(
        margin: EdgeInsets.only(left: SizeConfig.safeBlockHorizontal * 2.0),
        width: 20,
        height: 20,
        decoration: BoxDecoration(
          shape: BoxShape.circle,
          image: DecorationImage(
              image: AssetImage("assets/perfil2.jpg"), fit: BoxFit.cover),
        ),
      ),
    );
  }

  Widget buildBuscarBtn() {
    return GestureDetector(
      onTap: () {},
      child: Container(
        margin: EdgeInsets.only(right: SizeConfig.safeBlockHorizontal * 2),
        width: 20,
        height: 20,
        decoration: BoxDecoration(
          boxShadow: [
            BoxShadow(
              color: Colors.black26,
              blurRadius: 6.0,
              offset: Offset(0, 2),
            ),
          ],
          shape: BoxShape.circle,
          image: DecorationImage(
              image: AssetImage("assets/buscar.jpg"), fit: BoxFit.cover),
        ),
      ),
    );
  }

  Widget buildInicioBtn() {
    Map parametros = ModalRoute.of(context).settings.arguments;
    return GestureDetector(
      onTap: () {

        stopVerificarHoraServicio();
        appData.timer?.cancel();

        Navigator.pushNamed(context, "/inicio", arguments: {
          'id': parametros['id'],
          'nombre': parametros['nombre'],
          'documento': parametros['documento'],
          'username': parametros['username'],
          'password': parametros['password'],
        });
      },
      child: Container(
        margin: EdgeInsets.only(left: SizeConfig.safeBlockHorizontal * 1.0),
        width: 20,
        height: 20,
        decoration: BoxDecoration(
          shape: BoxShape.circle,
          image: DecorationImage(
              image: AssetImage("assets/inicio2.jpg"), fit: BoxFit.cover),
        ),
      ),
    );
  }

  Widget buildPerfilBtn() {
    Map parametros = ModalRoute.of(context).settings.arguments;
    return GestureDetector(
      onTap: () {
        Navigator.pushNamed(context, "/inicio/perfil", arguments: {
          'id': parametros['id'],
          'nombre': parametros['nombre'],
          'documento': parametros['documento'],
          'username': parametros['username'],
          'password': parametros['password'],
        });
      },
      child: Container(
        margin: EdgeInsets.only(left: SizeConfig.safeBlockHorizontal * 1.0),
        width: 20,
        height: 20,
        decoration: BoxDecoration(
          shape: BoxShape.circle,
          image: DecorationImage(
              image: AssetImage("assets/perfil_inicio.jpg"), fit: BoxFit.cover),
        ),
      ),
    );
  }

  Widget buildMapBtn() {
    return GestureDetector(
      onTap: () {
        showMyDialogSignOut();
      },
      child: Container(
        margin: EdgeInsets.only(left: SizeConfig.safeBlockHorizontal * 1.0),
        width: 20,
        height: 20,
        decoration: BoxDecoration(
          shape: BoxShape.circle,
          image: DecorationImage(
              image: AssetImage("assets/cerrar_inicio.jpg"), fit: BoxFit.cover),
        ),
      ),
    );
  }

  Widget buildInformationBtn() {
    Map parametros = ModalRoute.of(context).settings.arguments;
    return GestureDetector(
      onTap: () {
        Navigator.pushNamed(context, "/inicio/informacion", arguments: {
          'id': parametros['id'],
          'nombre': parametros['nombre'],
          'documento': parametros['documento'],
          'username': parametros['username'],
          'password': parametros['password'],
        });
      },
      child: Container(
        margin: EdgeInsets.only(left: SizeConfig.safeBlockHorizontal * 1.0),
        width: 20,
        height: 20,
        decoration: BoxDecoration(
          shape: BoxShape.circle,
          image: DecorationImage(
              image: AssetImage("assets/informacion.jpg"), fit: BoxFit.cover),
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    Map parametros = ModalRoute.of(context).settings.arguments;
    SizeConfig().init(context);
    return new WillPopScope(
        onWillPop: () async => false,
        child: Scaffold(
          floatingActionButton: Padding(
            padding: EdgeInsets.only(
                bottom: SizeConfig.safeBlockVertical * 55,
                right: SizeConfig.safeBlockHorizontal * 5
            ),
            child: FloatingActionButton(
              backgroundColor: Colors.white,
              child: Container(
                width: SizeConfig.safeBlockVertical * 25,
                height: SizeConfig.safeBlockHorizontal * 25,
                decoration: BoxDecoration(
                  shape: BoxShape.circle,
                  image: DecorationImage(
                      image: AssetImage("assets/flecha1.jpg"), fit: BoxFit.cover),
                ),
              ),
              onPressed: () => {

                stopVerificarHoraServicio(),
                appData.timer?.cancel(),

                Navigator.pushNamed(context, "/inicio", arguments: {
                  'id': parametros['id'],
                  'nombre': parametros['nombre'],
                  'documento': parametros['documento'],
                  'username': parametros['username'],
                  'password': parametros['password']
                })
              },
            ),
          ),
          body: AnnotatedRegion<SystemUiOverlayStyle>(
            value: SystemUiOverlayStyle.light,
            child: GestureDetector(
              onTap: () => FocusScope.of(context).unfocus(),
              child: Stack(
                children: <Widget>[
                  Container(
                    height: double.infinity,
                    width: double.infinity,
                    color: Colors.white,
                  ),
                  SingleChildScrollView(
                    child: Center(
                      child: Container(
                        width: SizeConfig.safeBlockHorizontal * 100,
                        height: SizeConfig.blockSizeVertical * 100,
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: <Widget>[
                            Container(
                              width: SizeConfig.safeBlockHorizontal * 100,
                              height: SizeConfig.safeBlockVertical * 5,
                            ),
                            Center(
                              child: Container(
                                width: SizeConfig.safeBlockHorizontal * 96,
                                height: SizeConfig.safeBlockVertical * 14,
                                decoration: BoxDecoration(
                                  color: Colors.white,
                                  borderRadius: BorderRadius.circular(SizeConfig.safeBlockHorizontal * 4),
                                    border: Border.all(
                                        width: 1,
                                        color: Colors.grey
                                    )
                                ),
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.end,
                                  children: [
                                    Container(
                                      width: SizeConfig.safeBlockHorizontal * 100,
                                      height: SizeConfig.safeBlockVertical * 2,
                                    ),
                                    Container(
                                      width: SizeConfig.safeBlockHorizontal * 100,
                                      height: SizeConfig.safeBlockVertical * 9,
                                      child: Row(
                                        children: [
                                          Container(
                                            width: SizeConfig.safeBlockHorizontal * 5,
                                            height: SizeConfig.safeBlockVertical * 100,
                                          ),
                                          Container(
                                            width: SizeConfig.safeBlockHorizontal * 15,
                                            height: SizeConfig.safeBlockVertical * 100,
                                            child: buildSocialBtn(),
                                          ),
                                          Container(
                                            width: SizeConfig.safeBlockHorizontal * 2,
                                            height: SizeConfig.safeBlockVertical * 100,
                                          ),
                                          Container(
                                            width: SizeConfig.safeBlockHorizontal * 73,
                                            height: SizeConfig.safeBlockVertical * 100,
                                            child: Column(
                                              children: [
                                                Container(
                                                  width: SizeConfig.safeBlockHorizontal * 100,
                                                  height: SizeConfig.safeBlockVertical * 5.5,
                                                  child: Padding(
                                                    padding: EdgeInsets.only(top: SizeConfig.safeBlockVertical * 2),
                                                    child: FutureBuilder(
                                                        future: obtenerUsuario(),
                                                        builder: (context, snapshot) {
                                                          if (snapshot.hasError) print(snapshot.error);
                                                          return snapshot.hasData
                                                              ? new getByIdUsuario(list: snapshot.data)
                                                              : new Container(
                                                            width: SizeConfig.safeBlockHorizontal * 100,
                                                            height: SizeConfig.safeBlockVertical * 5.5,
                                                            child: Row(
                                                              children: [
                                                                Container(
                                                                  width: SizeConfig.safeBlockHorizontal * 3,
                                                                  height: SizeConfig.safeBlockVertical * 100,
                                                                ),
                                                                Container(
                                                                  width: SizeConfig.safeBlockHorizontal * 5,
                                                                  height: SizeConfig.safeBlockVertical * 100,
                                                                  child: CircularProgressIndicator(),
                                                                ),
                                                                Container(
                                                                  width: SizeConfig.safeBlockHorizontal * 65,
                                                                  height: SizeConfig.safeBlockVertical * 100,
                                                                ),
                                                              ],
                                                            ),
                                                          );
                                                        }),
                                                  ),
                                                ),
                                                Container(
                                                  width: SizeConfig.safeBlockHorizontal * 100,
                                                  height: SizeConfig.safeBlockVertical * 3.5,
                                                  child: Padding(
                                                    padding: EdgeInsets.only(top: SizeConfig.safeBlockVertical * 0.5),
                                                    child: Text(
                                                      "Que tal tu dia hoy ?",
                                                      style: TextStyle(
                                                        color: Colors.black38,
                                                        fontWeight: FontWeight.bold,
                                                        fontFamily: 'OpenSans',
                                                        fontSize: SizeConfig.safeBlockHorizontal * 4,
                                                      ),
                                                    ),
                                                  ),
                                                ),
                                              ],
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),
                                    Container(
                                      width: SizeConfig.safeBlockHorizontal * 100,
                                      height: SizeConfig.safeBlockVertical * 2,
                                    ),
                                  ],
                                ),
                              ),
                            ),
                            Container(
                              width: SizeConfig.safeBlockHorizontal * 100,
                              height: SizeConfig.safeBlockVertical * 1,
                            ),
                            Center(
                              child: Container(
                                width: SizeConfig.safeBlockHorizontal * 94,
                                height: SizeConfig.safeBlockVertical * 72.5,
                                child: Row(
                                  children: [
                                    Container(
                                      width: SizeConfig.safeBlockHorizontal * 1.5,
                                      height: SizeConfig.safeBlockVertical * 100,
                                    ),
                                    Container(
                                      width: SizeConfig.safeBlockHorizontal * 91,
                                      height: SizeConfig.safeBlockVertical * 100,
                                      decoration: BoxDecoration(
                                        color: Colors.white,
                                        borderRadius: BorderRadius.circular(SizeConfig.safeBlockHorizontal * 4),
                                          border: Border.all(
                                              width: 1,
                                              color: Colors.grey
                                          )
                                      ),
                                      child: Column(
                                        children: [
                                          Container(
                                              width: SizeConfig.safeBlockHorizontal * 100,
                                              height: SizeConfig.safeBlockVertical * 14,
                                              child: Column(
                                                children: [
                                                  Container(
                                                    width: SizeConfig.safeBlockHorizontal * 100,
                                                    height: SizeConfig.safeBlockVertical * 2,
                                                  ),
                                                  Container(
                                                    width: SizeConfig.safeBlockHorizontal * 100,
                                                    height: SizeConfig.safeBlockVertical * 10,
                                                    child: Row(
                                                      children: [
                                                        Container(
                                                          width: SizeConfig.safeBlockHorizontal * 4.5,
                                                          height: SizeConfig.safeBlockVertical * 100,
                                                        ),
                                                        Container(
                                                          width: SizeConfig.safeBlockHorizontal * 81,
                                                          height: SizeConfig.safeBlockVertical * 100,
                                                          decoration: BoxDecoration(
                                                            color: Colors.grey[100],
                                                            borderRadius: BorderRadius.circular(SizeConfig.safeBlockHorizontal * 4),
                                                          ),
                                                          child: Padding(
                                                            padding: EdgeInsets.all(SizeConfig.safeBlockVertical * 3),
                                                            child: Text(
                                                              "Servicio",
                                                              style: TextStyle(
                                                                color: Colors.black26,
                                                                fontWeight: FontWeight.bold,
                                                                fontFamily: 'OpenSans',
                                                                fontSize: SizeConfig.safeBlockHorizontal * 5,
                                                              ),
                                                            ),
                                                          ),
                                                        ),
                                                        Container(
                                                          width: SizeConfig.safeBlockHorizontal * 4.5,
                                                          height: SizeConfig.safeBlockVertical * 100,
                                                        ),
                                                      ],
                                                    ),
                                                  ),
                                                  Container(
                                                    width: SizeConfig.safeBlockHorizontal * 100,
                                                    height: SizeConfig.safeBlockVertical * 2,
                                                  ),
                                                ],
                                              )
                                          ),
                                          Container(
                                            width: SizeConfig.safeBlockHorizontal * 100,
                                            height: SizeConfig.safeBlockVertical * 1,
                                            child: Row(
                                              children: [
                                                Container(
                                                  width: SizeConfig.safeBlockHorizontal * 3.5,
                                                  height: SizeConfig.safeBlockVertical * 100,
                                                ),
                                                Container(
                                                  width: SizeConfig.safeBlockHorizontal * 83,
                                                  height: SizeConfig.safeBlockVertical * 100,
                                                  child: Column(
                                                    children: [
                                                      Container(
                                                        width: SizeConfig.safeBlockHorizontal * 100,
                                                        height: SizeConfig.safeBlockVertical * 0.8,
                                                      ),
                                                      Container(
                                                        width: SizeConfig.safeBlockHorizontal * 100,
                                                        height: SizeConfig.safeBlockVertical * 0.2,
                                                        color: Colors.grey[200],
                                                      ),
                                                    ],
                                                  ),
                                                ),
                                                Container(
                                                  width: SizeConfig.safeBlockHorizontal * 3.5,
                                                  height: SizeConfig.safeBlockVertical * 100,
                                                ),
                                              ],
                                            ),
                                          ),
                                          Container(
                                            width: SizeConfig.safeBlockHorizontal * 100,
                                            height: SizeConfig.safeBlockVertical * 55.5,
                                            child: ListView(
                                              children: [
                                                Container(
                                                  width: SizeConfig.safeBlockHorizontal * 100,
                                                  height: SizeConfig.safeBlockVertical * 7,
                                                  child: Row(
                                                    children: [
                                                      Container(
                                                        width: SizeConfig.safeBlockHorizontal * 4.5,
                                                        height: SizeConfig.safeBlockVertical * 100,
                                                      ),
                                                      Container(
                                                        width: SizeConfig.safeBlockHorizontal * 81,
                                                        height: SizeConfig.safeBlockVertical * 100,
                                                        child: Column(
                                                          children: [
                                                            Container(
                                                              width: SizeConfig.safeBlockHorizontal * 100,
                                                              height: SizeConfig.safeBlockVertical * 3,
                                                              child: Padding(
                                                                padding: EdgeInsets.only(top: SizeConfig.safeBlockVertical * 0, left: SizeConfig.safeBlockHorizontal * 1),
                                                                child: Text(
                                                                  "Conductor asignado",
                                                                  style: TextStyle(
                                                                    color: Colors.black38,
                                                                    fontWeight: FontWeight.bold,
                                                                    fontFamily: 'OpenSans',
                                                                    fontSize: SizeConfig.safeBlockHorizontal * 4,
                                                                  ),
                                                                ),
                                                              ),
                                                            ),
                                                            Container(
                                                              width: SizeConfig.safeBlockHorizontal * 100,
                                                              height: SizeConfig.safeBlockVertical * 4,
                                                              child: Padding(
                                                                padding: EdgeInsets.only(top: SizeConfig.safeBlockVertical * 1, left: SizeConfig.safeBlockHorizontal * 1),
                                                                child: FutureBuilder(
                                                                    future: obtenerUsuario(),
                                                                    builder: (context, snapshot) {
                                                                      if (snapshot.hasError) print(snapshot.error);
                                                                      return snapshot.hasData
                                                                          ? new getByIdNombre(list: snapshot.data)
                                                                          : new Container(
                                                                        width: SizeConfig.safeBlockHorizontal * 100,
                                                                        height: SizeConfig.safeBlockVertical * 5.5,
                                                                        child: Row(
                                                                          children: [
                                                                            Container(
                                                                              width: SizeConfig.safeBlockHorizontal * 3,
                                                                              height: SizeConfig.safeBlockVertical * 100,
                                                                            ),
                                                                            Container(
                                                                              width: SizeConfig.safeBlockHorizontal * 5,
                                                                              height: SizeConfig.safeBlockVertical * 100,
                                                                              child: CircularProgressIndicator(),
                                                                            ),
                                                                            Container(
                                                                              width: SizeConfig.safeBlockHorizontal * 65,
                                                                              height: SizeConfig.safeBlockVertical * 100,
                                                                            ),
                                                                          ],
                                                                        ),
                                                                      );
                                                                    }),
                                                              ),
                                                            ),
                                                          ],
                                                        ),
                                                      ),
                                                      Container(
                                                        width: SizeConfig.safeBlockHorizontal * 4.5,
                                                        height: SizeConfig.safeBlockVertical * 100,

                                                      ),
                                                    ],
                                                  ),
                                                ),
                                                Container(
                                                  width: SizeConfig.safeBlockHorizontal * 100,
                                                  height: SizeConfig.safeBlockVertical * 7,
                                                  child: Row(
                                                    children: [
                                                      Container(
                                                        width: SizeConfig.safeBlockHorizontal * 4.5,
                                                        height: SizeConfig.safeBlockVertical * 100,
                                                      ),
                                                      Container(
                                                        width: SizeConfig.safeBlockHorizontal * 81,
                                                        height: SizeConfig.safeBlockVertical * 100,
                                                        child: Column(
                                                          children: [
                                                            Container(
                                                              width: SizeConfig.safeBlockHorizontal * 100,
                                                              height: SizeConfig.safeBlockVertical * 3,
                                                              child: Padding(
                                                                padding: EdgeInsets.only(top: SizeConfig.safeBlockVertical * 0, left: SizeConfig.safeBlockHorizontal * 1),
                                                                child: Text(
                                                                  "Documento",
                                                                  style: TextStyle(
                                                                    color: Colors.black38,
                                                                    fontWeight: FontWeight.bold,
                                                                    fontFamily: 'OpenSans',
                                                                    fontSize: SizeConfig.safeBlockHorizontal * 4,
                                                                  ),
                                                                ),
                                                              ),
                                                            ),
                                                            Container(
                                                              width: SizeConfig.safeBlockHorizontal * 100,
                                                              height: SizeConfig.safeBlockVertical * 4,
                                                              child: Padding(
                                                                padding: EdgeInsets.only(top: SizeConfig.safeBlockVertical * 1, left: SizeConfig.safeBlockHorizontal * 1),
                                                                child: FutureBuilder(
                                                                    future: obtenerUsuario(),
                                                                    builder: (context, snapshot) {
                                                                      if (snapshot.hasError) print(snapshot.error);
                                                                      return snapshot.hasData
                                                                          ? new getByIdDocumento(list: snapshot.data)
                                                                          : new Container(
                                                                        width: SizeConfig.safeBlockHorizontal * 100,
                                                                        height: SizeConfig.safeBlockVertical * 5.5,
                                                                        child: Row(
                                                                          children: [
                                                                            Container(
                                                                              width: SizeConfig.safeBlockHorizontal * 3,
                                                                              height: SizeConfig.safeBlockVertical * 100,
                                                                            ),
                                                                            Container(
                                                                              width: SizeConfig.safeBlockHorizontal * 5,
                                                                              height: SizeConfig.safeBlockVertical * 100,
                                                                              child: CircularProgressIndicator(),
                                                                            ),
                                                                            Container(
                                                                              width: SizeConfig.safeBlockHorizontal * 65,
                                                                              height: SizeConfig.safeBlockVertical * 100,
                                                                            ),
                                                                          ],
                                                                        ),
                                                                      );
                                                                    }),
                                                              ),
                                                            ),
                                                          ],
                                                        ),
                                                      ),
                                                      Container(
                                                        width: SizeConfig.safeBlockHorizontal * 4.5,
                                                        height: SizeConfig.safeBlockVertical * 100,

                                                      ),
                                                    ],
                                                  ),
                                                ),
                                                Container(
                                                  width: SizeConfig.safeBlockHorizontal * 100,
                                                  height: SizeConfig.safeBlockVertical * 3,
                                                ),
                                                Container(
                                                  width: SizeConfig.safeBlockHorizontal * 100,
                                                  height: SizeConfig.safeBlockVertical * 8,
                                                  child: Row(
                                                    children: [
                                                      Container(
                                                        width: SizeConfig.safeBlockHorizontal * 4.5,
                                                        height: SizeConfig.safeBlockVertical * 100,
                                                      ),
                                                      Container(
                                                        width: SizeConfig.safeBlockHorizontal * 81,
                                                        height: SizeConfig.safeBlockVertical * 100,
                                                        child: Column(
                                                          children: [
                                                            Container(
                                                              width: SizeConfig.safeBlockHorizontal * 100,
                                                              height: SizeConfig.safeBlockVertical * 4,
                                                              child: Padding(
                                                                padding: EdgeInsets.only(top: SizeConfig.safeBlockVertical * 1, left: SizeConfig.safeBlockHorizontal * 1),
                                                                child: Text(
                                                                  "Paciente",
                                                                  style: TextStyle(
                                                                    color: Colors.black38,
                                                                    fontWeight: FontWeight.bold,
                                                                    fontFamily: 'OpenSans',
                                                                    fontSize: SizeConfig.safeBlockHorizontal * 4,
                                                                  ),
                                                                ),
                                                              ),
                                                            ),
                                                            Container(
                                                              width: SizeConfig.safeBlockHorizontal * 100,
                                                              height: SizeConfig.safeBlockVertical * 4,
                                                              child: Padding(
                                                                padding: EdgeInsets.only(top: SizeConfig.safeBlockVertical * 1, left: SizeConfig.safeBlockHorizontal * 1),
                                                                child: Text(
                                                                  parametros['nom_paciente'],
                                                                  style: TextStyle(
                                                                    color: Colors.black26,
                                                                    fontWeight: FontWeight.bold,
                                                                    fontFamily: 'OpenSans',
                                                                    fontSize: SizeConfig.safeBlockHorizontal * 3.5,
                                                                  ),
                                                                ),
                                                              ),
                                                            ),
                                                          ],
                                                        ),
                                                      ),
                                                      Container(
                                                        width: SizeConfig.safeBlockHorizontal * 4.5,
                                                        height: SizeConfig.safeBlockVertical * 100,
                                                      ),
                                                    ],
                                                  ),
                                                ),
                                                Container(
                                                  width: SizeConfig.safeBlockHorizontal * 100,
                                                  height: SizeConfig.safeBlockVertical * 8,
                                                  child: Row(
                                                    children: [
                                                      Container(
                                                        width: SizeConfig.safeBlockHorizontal * 4.5,
                                                        height: SizeConfig.safeBlockVertical * 100,
                                                      ),
                                                      Container(
                                                          width: SizeConfig.safeBlockHorizontal * 81,
                                                          height: SizeConfig.safeBlockVertical * 100,
                                                          child: Row(
                                                            children: [
                                                              Container(
                                                                width: SizeConfig.safeBlockHorizontal * 40.5,
                                                                height: SizeConfig.safeBlockVertical * 100,
                                                                child: Column(
                                                                  children: [
                                                                    Container(
                                                                      width: SizeConfig.safeBlockHorizontal * 100,
                                                                      height: SizeConfig.safeBlockVertical * 4,
                                                                      child: Padding(
                                                                        padding: EdgeInsets.only(top: SizeConfig.safeBlockVertical * 1, left: SizeConfig.safeBlockHorizontal * 1),
                                                                        child: Text(
                                                                          "Direcion Salidad",
                                                                          style: TextStyle(
                                                                            color: Colors.black38,
                                                                            fontWeight: FontWeight.bold,
                                                                            fontFamily: 'OpenSans',
                                                                            fontSize: SizeConfig.safeBlockHorizontal * 4,
                                                                          ),
                                                                        ),
                                                                      ),
                                                                    ),
                                                                    Container(
                                                                      width: SizeConfig.safeBlockHorizontal * 100,
                                                                      height: SizeConfig.safeBlockVertical * 4,
                                                                      child: Padding(
                                                                        padding: EdgeInsets.only(top: SizeConfig.safeBlockVertical * 1, left: SizeConfig.safeBlockHorizontal * 1),
                                                                        child: Text(
                                                                          parametros['direccion_salida'],
                                                                          style: TextStyle(
                                                                            color: Colors.black26,
                                                                            fontWeight: FontWeight.bold,
                                                                            fontFamily: 'OpenSans',
                                                                            fontSize: SizeConfig.safeBlockHorizontal * 3.5,
                                                                          ),
                                                                        ),
                                                                      ),
                                                                    ),
                                                                  ],
                                                                ),
                                                              ),
                                                              Container(
                                                                width: SizeConfig.safeBlockHorizontal * 40.5,
                                                                height: SizeConfig.safeBlockVertical * 100,
                                                                child: Column(
                                                                  children: [
                                                                    Container(
                                                                      width: SizeConfig.safeBlockHorizontal * 100,
                                                                      height: SizeConfig.safeBlockVertical * 4,
                                                                      child: Padding(
                                                                        padding: EdgeInsets.only(top: SizeConfig.safeBlockVertical * 1, left: SizeConfig.safeBlockHorizontal * 1),
                                                                        child: Text(
                                                                          "Direcion Destino",
                                                                          style: TextStyle(
                                                                            color: Colors.black38,
                                                                            fontWeight: FontWeight.bold,
                                                                            fontFamily: 'OpenSans',
                                                                            fontSize: SizeConfig.safeBlockHorizontal * 4,
                                                                          ),
                                                                        ),
                                                                      ),
                                                                    ),
                                                                    Container(
                                                                      width: SizeConfig.safeBlockHorizontal * 100,
                                                                      height: SizeConfig.safeBlockVertical * 4,
                                                                      child: Padding(
                                                                        padding: EdgeInsets.only(top: SizeConfig.safeBlockVertical * 1, left: SizeConfig.safeBlockHorizontal * 1),
                                                                        child: Text(
                                                                          parametros['direccion_llegada'],
                                                                          style: TextStyle(
                                                                            color: Colors.black26,
                                                                            fontWeight: FontWeight.bold,
                                                                            fontFamily: 'OpenSans',
                                                                            fontSize: SizeConfig.safeBlockHorizontal * 3.5,
                                                                          ),
                                                                        ),
                                                                      ),
                                                                    ),
                                                                  ],
                                                                ),
                                                              ),
                                                            ],
                                                          )
                                                      ),
                                                      Container(
                                                        width: SizeConfig.safeBlockHorizontal * 4.5,
                                                        height: SizeConfig.safeBlockVertical * 100,
                                                      ),
                                                    ],
                                                  ),
                                                ),
                                                Container(
                                                  width: SizeConfig.safeBlockHorizontal * 100,
                                                  height: SizeConfig.safeBlockVertical * 8,
                                                  child: Row(
                                                    children: [
                                                      Container(
                                                        width: SizeConfig.safeBlockHorizontal * 4.5,
                                                        height: SizeConfig.safeBlockVertical * 100,
                                                      ),
                                                      Container(
                                                        width: SizeConfig.safeBlockHorizontal * 81,
                                                        height: SizeConfig.safeBlockVertical * 100,
                                                        child: Column(
                                                          children: [
                                                            Container(
                                                              width: SizeConfig.safeBlockHorizontal * 100,
                                                              height: SizeConfig.safeBlockVertical * 4,
                                                              child: Padding(
                                                                padding: EdgeInsets.only(top: SizeConfig.safeBlockVertical * 1, left: SizeConfig.safeBlockHorizontal * 1),
                                                                child: Text(
                                                                  "Fecha y Horario",
                                                                  style: TextStyle(
                                                                    color: Colors.black38,
                                                                    fontWeight: FontWeight.bold,
                                                                    fontFamily: 'OpenSans',
                                                                    fontSize: SizeConfig.safeBlockHorizontal * 4,
                                                                  ),
                                                                ),
                                                              ),
                                                            ),
                                                            Container(
                                                              width: SizeConfig.safeBlockHorizontal * 100,
                                                              height: SizeConfig.safeBlockVertical * 4,
                                                              child: Padding(
                                                                padding: EdgeInsets.only(top: SizeConfig.safeBlockVertical * 1, left: SizeConfig.safeBlockHorizontal * 1),
                                                                child: Text(
                                                                  parametros['fecha_hora_servicio'],
                                                                  style: TextStyle(
                                                                    color: Colors.black26,
                                                                    fontWeight: FontWeight.bold,
                                                                    fontFamily: 'OpenSans',
                                                                    fontSize: SizeConfig.safeBlockHorizontal * 3.5,
                                                                  ),
                                                                ),
                                                              ),
                                                            ),
                                                          ],
                                                        ),
                                                      ),
                                                      Container(
                                                        width: SizeConfig.safeBlockHorizontal * 4.5,
                                                        height: SizeConfig.safeBlockVertical * 100,
                                                      ),
                                                    ],
                                                  ),
                                                ),
                                                Container(
                                                  width: SizeConfig.safeBlockHorizontal * 100,
                                                  height: SizeConfig.safeBlockVertical * 10,
                                                  child: Row(
                                                    children: [
                                                      Container(
                                                        width: SizeConfig.safeBlockHorizontal * 4.5,
                                                        height: SizeConfig.safeBlockVertical * 100,
                                                      ),
                                                      Container(
                                                        width: SizeConfig.safeBlockHorizontal * 81,
                                                        height: SizeConfig.safeBlockVertical * 100,
                                                        child: Column(
                                                          children: [
                                                            Container(
                                                              width: SizeConfig.safeBlockHorizontal * 100,
                                                              height: SizeConfig.safeBlockVertical * 4,
                                                              child: Padding(
                                                                padding: EdgeInsets.only(top: SizeConfig.safeBlockVertical * 1, left: SizeConfig.safeBlockHorizontal * 1),
                                                                child: Text(
                                                                  "Descripcion",
                                                                  style: TextStyle(
                                                                    color: Colors.black38,
                                                                    fontWeight: FontWeight.bold,
                                                                    fontFamily: 'OpenSans',
                                                                    fontSize: SizeConfig.safeBlockHorizontal * 4,
                                                                  ),
                                                                ),
                                                              ),
                                                            ),
                                                            Container(
                                                              width: SizeConfig.safeBlockHorizontal * 100,
                                                              height: SizeConfig.safeBlockVertical * 6,
                                                              child: Row(
                                                                  children: [
                                                                    Container(
                                                                      width: SizeConfig.safeBlockHorizontal * 1,
                                                                      height: SizeConfig.safeBlockVertical * 100,
                                                                    ),
                                                                    Container(
                                                                      width: SizeConfig.safeBlockHorizontal * 60,
                                                                      height: SizeConfig.safeBlockVertical * 100,
                                                                      child: Padding(
                                                                        padding: EdgeInsets.only(top: SizeConfig.safeBlockVertical * 0),
                                                                        child: Text(
                                                                          parametros['descripcion'],
                                                                          style: TextStyle(
                                                                            color: Colors.black26,
                                                                            fontWeight: FontWeight.bold,
                                                                            fontFamily: 'OpenSans',
                                                                            fontSize: SizeConfig.safeBlockHorizontal * 4,
                                                                          ),
                                                                        ),
                                                                      ),
                                                                    ),
                                                                  ]
                                                              ),
                                                            ),
                                                          ],
                                                        ),
                                                      ),
                                                      Container(
                                                        width: SizeConfig.safeBlockHorizontal * 4.5,
                                                        height: SizeConfig.safeBlockVertical * 100,
                                                      ),
                                                    ],
                                                  ),
                                                ),
                                                Container(
                                                  width: SizeConfig.safeBlockHorizontal * 100,
                                                  height: SizeConfig.safeBlockVertical * 2,
                                                ),
                                                Container(
                                                  width: SizeConfig.safeBlockHorizontal * 100,
                                                  height: SizeConfig.safeBlockVertical * 10,
                                                  child: Row(
                                                    children: [
                                                      Container(
                                                        width: SizeConfig.safeBlockHorizontal * 4.5,
                                                        height: SizeConfig.safeBlockVertical * 100,
                                                      ),
                                                      Container(
                                                        width: SizeConfig.safeBlockHorizontal * 81,
                                                        height: SizeConfig.safeBlockVertical * 100,
                                                        child: Row(
                                                          children: [
                                                            Container(
                                                              width: SizeConfig.safeBlockHorizontal * 55,
                                                              height: SizeConfig.safeBlockVertical * 100,
                                                              child: _buildDescripcionTF(),
                                                            ),
                                                          ],
                                                        ),
                                                      ),
                                                      Container(
                                                        width: SizeConfig.safeBlockHorizontal * 4.5,
                                                        height: SizeConfig.safeBlockVertical * 100,
                                                      ),
                                                    ],
                                                  ),
                                                ),
                                                Container(
                                                  width: SizeConfig.safeBlockHorizontal * 100,
                                                  height: SizeConfig.safeBlockVertical * 2,
                                                ),
                                                Container(
                                                  width: SizeConfig.safeBlockHorizontal * 100,
                                                  height: SizeConfig.safeBlockVertical * 7,
                                                  child: Row(
                                                    children: [
                                                      Container(
                                                        width: SizeConfig.safeBlockHorizontal * 4.5,
                                                        height: SizeConfig.safeBlockVertical * 100,
                                                      ),
                                                      Container(
                                                        width: SizeConfig.safeBlockHorizontal * 81,
                                                        height: SizeConfig.safeBlockVertical * 100,
                                                        child: Row(
                                                          children: [
                                                            Container(
                                                              width: SizeConfig.safeBlockHorizontal * 35,
                                                              height: SizeConfig.safeBlockVertical * 100,
                                                              child: buildRechazadoBtn(),
                                                            ),
                                                            Container(
                                                              width: SizeConfig.safeBlockHorizontal * 14,
                                                              height: SizeConfig.safeBlockVertical * 100,
                                                            ),
                                                            Container(
                                                              width: SizeConfig.safeBlockHorizontal * 32,
                                                              height: SizeConfig.safeBlockVertical * 100,
                                                              child: buildAtendidoBtn(),
                                                            ),
                                                          ],
                                                        ),
                                                      ),
                                                      Container(
                                                        width: SizeConfig.safeBlockHorizontal * 4.5,
                                                        height: SizeConfig.safeBlockVertical * 100,
                                                      ),
                                                    ],
                                                  ),
                                                ),
                                                Container(
                                                  width: SizeConfig.safeBlockHorizontal * 100,
                                                  height: SizeConfig.safeBlockVertical * 2,
                                                ),
                                              ],
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),
                                    Container(
                                      width: SizeConfig.safeBlockHorizontal * 1.5,
                                      height: SizeConfig.safeBlockVertical * 100,
                                    ),
                                  ],
                                ),
                              ),
                            ),
                            Container(
                              width: SizeConfig.safeBlockHorizontal * 100,
                              height: SizeConfig.safeBlockVertical * 1,
                            ),
                            Center(
                              child: Container(
                                width: SizeConfig.safeBlockHorizontal * 98,
                                height: SizeConfig.safeBlockVertical * 10,
                                decoration: BoxDecoration(
                                  color: Colors.blue[200],
                                  borderRadius: BorderRadius.circular(SizeConfig.safeBlockHorizontal * 4),
                                  boxShadow: [
                                    BoxShadow(
                                      color: Colors.black12,
                                      blurRadius: 6.0,
                                      offset: Offset(0, 2),
                                    ),
                                  ],
                                ),
                                child: Column(
                                  children: [
                                    Container(
                                      width: SizeConfig.safeBlockHorizontal * 100,
                                      height: SizeConfig.safeBlockVertical * 1.5,
                                    ),
                                    Container(
                                      width: SizeConfig.safeBlockHorizontal * 100,
                                      height: SizeConfig.safeBlockVertical * 7,
                                      child: Row(
                                        children: [
                                          Container(
                                            width: SizeConfig.safeBlockHorizontal * 11,
                                            height: SizeConfig.safeBlockVertical * 100,
                                          ),
                                          Container(
                                            width: SizeConfig.safeBlockHorizontal * 16,
                                            height: SizeConfig.safeBlockVertical * 100,
                                            child: buildInicioBtn(),
                                          ),
                                          Container(
                                            width: SizeConfig.safeBlockHorizontal * 4,
                                            height: SizeConfig.safeBlockVertical * 100,
                                          ),
                                          Container(
                                            width: SizeConfig.safeBlockHorizontal * 16,
                                            height: SizeConfig.safeBlockVertical * 100,
                                            child: buildPerfilBtn(),
                                          ),
                                          Container(
                                            width: SizeConfig.safeBlockHorizontal * 4,
                                            height: SizeConfig.safeBlockVertical * 100,
                                          ),
                                          Container(
                                            width: SizeConfig.safeBlockHorizontal * 16,
                                            height: SizeConfig.safeBlockVertical * 100,
                                            child: buildInformationBtn(),
                                          ),
                                          Container(
                                            width: SizeConfig.safeBlockHorizontal * 4,
                                            height: SizeConfig.safeBlockVertical * 100,
                                          ),
                                          Container(
                                            width: SizeConfig.safeBlockHorizontal * 16,
                                            height: SizeConfig.safeBlockVertical * 100,
                                            child: buildMapBtn(),
                                          ),
                                          Container(
                                            width: SizeConfig.safeBlockHorizontal * 11,
                                            height: SizeConfig.safeBlockVertical * 100,
                                          ),
                                        ],
                                      ),
                                    ),
                                    Container(
                                      width: SizeConfig.safeBlockHorizontal * 100,
                                      height: SizeConfig.safeBlockVertical * 1.5,
                                    ),
                                  ],
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
        ));
  }

  void pasarInicio() {
    Map parametros = ModalRoute.of(context).settings.arguments;
    Navigator.pushNamed(context, "/inicio", arguments: {
      'id': parametros['id'],
      'nombre': parametros['nombre'],
      'username': parametros['username'],
      'password': parametros['password'],
    });
  }

}

extension StringExtension on String {
  String capitalize2() {
    return "${this[0].toUpperCase()}${this.substring(1)}";
  }
}

class getByIdNombre extends StatelessWidget {

  final list;
  getByIdNombre({this.list});

  @override
  Widget build(BuildContext context) {
    String nombre = list['nombre'];
    String userName = nombre.toUpperCase();
    return Text(
      userName,
      style: TextStyle(
        color: Colors.black26,
        fontWeight: FontWeight.bold,
        fontFamily: 'OpenSans',
        fontSize: SizeConfig.safeBlockHorizontal * 3.5,
      ),
    );
  }
}

class getByIdDocumento extends StatelessWidget {

  final list;
  getByIdDocumento({this.list});

  @override
  Widget build(BuildContext context) {
    return Text(
      list['documento'].toString(),
      style: TextStyle(
        color: Colors.black26,
        fontWeight: FontWeight.bold,
        fontFamily: 'OpenSans',
        fontSize: SizeConfig.safeBlockHorizontal * 3.5,
      ),
    );
  }
}

class getByIdUsuario extends StatelessWidget {

  final list;
  getByIdUsuario({this.list});

  @override
  Widget build(BuildContext context) {
    var pasarString = list['nombre'].toString();
    var pasarArray = pasarString.split(" ");
    var contenedorNombre = pasarArray[0];
    var convertir = contenedorNombre.toLowerCase();
    var mayuscula = convertir.capitalize2();
    return Text(
      mayuscula,
      style: TextStyle(
        color: Colors.black26,
        fontWeight: FontWeight.bold,
        fontFamily: 'OpenSans',
        fontSize: SizeConfig.safeBlockHorizontal * 5,
      ),
    );
  }
}
