import 'package:flutter/material.dart';

class HeaderStyle extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      height: double.infinity,
      width: double.infinity,
      child: CustomPaint(
        painter: _HesderPainter(),
      ),
    );
  }
}

class _HesderPainter extends CustomPainter {
  @override
  void paint(Canvas canvas, Size size) {
    final cursor = Paint();
    cursor.color = Colors.lightGreen;
    cursor.style = PaintingStyle.fill;
    cursor.strokeWidth = 20;

    final path = new Path();

    path.lineTo(0, size.height * 0.25);
    path.lineTo(size.width, size.height * 0.25);
    path.lineTo(size.width, 0);

    canvas.drawPath(path, cursor);
  }

  @override
  bool shouldRepaint(covariant CustomPainter oldDelegate) {
    // TODO: implement shouldRepaint
    return true;
  }
}
