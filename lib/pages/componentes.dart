import 'package:flutter/material.dart';
import 'package:conductor/SizeConfig.dart';

final kHintTextStyle = TextStyle(
  color: Colors.green,
  fontFamily: 'OpenSans',
);

final pHintTextStyle = TextStyle(
  color: Colors.black54,
  fontFamily: 'Opensans',
);

final kLabelStyle = TextStyle(
  color: Colors.green,
  fontWeight: FontWeight.bold,
  fontFamily: 'OpenSans',
  fontSize: SizeConfig.safeBlockHorizontal * 20,
);

final kBoxDecorationStyle = BoxDecoration(
  color: Color(0xFF00C251),
  borderRadius: BorderRadius.circular(10.0),
  boxShadow: [
    BoxShadow(
      color: Colors.black12,
      blurRadius: 6.0,
      offset: Offset(0, 2),
    ),
  ],
);
