import 'dart:async';
import 'dart:convert';
import 'dart:isolate';
import 'package:http/http.dart' as http;
import 'package:location/location.dart';

Isolate _isolate;
Timer _timer;

Future<ReceivePort> ActualizarUbicacion(userId, username, latLng) async {
  ReceivePort receivePort = new ReceivePort();
  _isolate = await Isolate.spawn(
      obtenerTodo, [
    receivePort.sendPort,
    userId,
    username,
    latLng
  ]);
  return receivePort;
}

stopActualizarUbicacion(){
  if(_isolate != null){
    _isolate.kill(priority: Isolate.immediate);
  }
}

Future<void> obtenerTodo(List params) async {

  SendPort sendPort = params[0];
  String userId = params[1];
  String userName = params[2];
  String cont = params[3];

  var url = "http://transporte-paciente.herokuapp.com/api/conductor/ubicacion/update";

  var arrayCont = cont.split(" ");
  String lat = arrayCont[1].replaceAll(",", "");
  String lng = arrayCont[3].replaceAll(">", "");

  var data = {
    "id_conductor": userId,
    "lat": lat,
    "lon": lng,
    "username": userName
  };

  var response = await  http.post(url, body: data);
  var value = jsonDecode(response.body);
  sendPort.send(value);

}