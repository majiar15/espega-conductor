import 'dart:async';
import 'dart:convert';
import 'dart:ffi';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:http/http.dart' as http;
import 'package:intl/intl.dart';
import 'package:conductor/helpers/AppData.dart';
import 'package:location/location.dart';
import 'package:path_provider/path_provider.dart';
import '../../SizeConfig.dart';
import '../componentes.dart';
import 'VerificarHoraServicio.dart';
import 'serviciosUsuario.dart';
import 'ubicacionUsuario.dart';

class InicioScreen extends StatefulWidget {
  @override
  _InicioScreenState createState() => _InicioScreenState();
}

class _InicioScreenState extends State<InicioScreen> {
  String _nombre;
  Timer _timer;
  int contCambiar = 1;
  int aumentador1 = 0;
  int aumentador2 = 0;
  var serviciosUsuario;
  var actualizarUbicacion;
  var verificarServicio;
  bool buscar = false;
  bool servicioEstado = true;

  TextEditingController servicio = TextEditingController();

  Future<Null> _selectTime(BuildContext context) async {
    final TimeOfDay hora = await showTimePicker(
      context: context,
      initialTime: TimeOfDay.now(),
    );
    if (hora != null)
      setState(() {
        var pasarString = hora.toString();
        var convertirArray = pasarString.split("(");
        String eliminar = convertirArray[1].replaceAll(")", "");
        servicio.value = TextEditingValue(text: eliminar);
      });
  }

  Future obtenerUsuario() async {
    Map parametros = ModalRoute.of(context).settings.arguments;

    final url = "http://transporte-paciente.herokuapp.com/api/conductor/login";
    var data = {
      "username": parametros['username'],
      "password": parametros['password'],
    };

    final response = await http.post(url, body: data);

    return jsonDecode(response.body);
  }

  Future obtenerServicios() async {
    Location location = new Location();
    bool _serviceEnabled;
    PermissionStatus _permissionGranted;

    _serviceEnabled = await location.serviceEnabled();
    if (!_serviceEnabled) {
      _serviceEnabled = await location.requestService();
      if (!_serviceEnabled) {
        return;
      }
    }

    _permissionGranted = await location.hasPermission();
    if (_permissionGranted == PermissionStatus.DENIED) {
      _permissionGranted = await location.requestPermission();
      if (_permissionGranted != PermissionStatus.GRANTED) {
        return;
      }
    }

    Map parametros = ModalRoute.of(context).settings.arguments;

    Future.delayed(Duration(seconds: 1), () async {
      final url2 =
          "http://transporte-paciente.herokuapp.com/api/conductor/ubicacion/get";

      var data2 = {
        "id_conductor": appData.id_paciente,
        "id_paciente": "1",
        "username": appData.username
      };

      final response2 = await http.post(url2, body: data2);
      var cont = jsonDecode(response2.body);

      appData.ubicacion_principal_lat = cont["lat"].toString();
      appData.ubicacion_principal_lng = cont["lon"].toString();

      var servicios =
          await ActualizarServicios(parametros['id'], parametros['username']);
      servicios.listen((data) {
        if (mounted)
          setState(() {
            serviciosUsuario = jsonDecode(data);
          });
      });
    });
  }

  void obtenerUbicacionUsuario() async {
    Location location = new Location();
    bool _serviceEnabled;
    PermissionStatus _permissionGranted;
    LocationData _locationData;

    _serviceEnabled = await location.serviceEnabled();
    if (!_serviceEnabled) {
      _serviceEnabled = await location.requestService();
      if (!_serviceEnabled) {
        return;
      }
    }

    _permissionGranted = await location.hasPermission();
    if (_permissionGranted == PermissionStatus.DENIED) {
      _permissionGranted = await location.requestPermission();
      if (_permissionGranted != PermissionStatus.GRANTED) {
        return;
      }
    }

    _locationData = await location.getLocation();

    String cont = _locationData.toString();

    var arrayCont = cont.split(" ");
    String lat = arrayCont[1].replaceAll(",", "");
    String lng = arrayCont[3].replaceAll(">", "");

    appData.ubicacion_principal_lat = lat;
    appData.ubicacion_principal_lng = lng;

    Map parametros = ModalRoute.of(context).settings.arguments;
    var servicios = await ActualizarUbicacion(
        parametros['id'], parametros['username'], _locationData.toString());
    servicios.listen((data) {});
  }

  Future Contenedor() async {
    Map parametros = ModalRoute.of(context).settings.arguments;

    if (servicioEstado == true) {
      if (serviciosUsuario != null) {
        Future.delayed(Duration(seconds: 1), () async {
          var servicios = await VerificarHoraServicio(
              parametros['username'], serviciosUsuario);
          servicios.listen((data2) {
            if (mounted)
              setState(() {
                actualizarUbicacion = jsonDecode(data2);
              });
            print(actualizarUbicacion);
          });
        });
      }
    }

    if (actualizarUbicacion != null) {
      if (actualizarUbicacion[0]["servicio"] == false) {
        servicioEstado = false;
      }

      if (actualizarUbicacion[0]["tiempo"] > 0) {
        servicioEstado = false;
        aumentador1++;
        aumentador2++;

        if (actualizarUbicacion[0]["ubicacion"] == "no ubicar") {
          if (aumentador2 == 15) {
            aumentador2 = 0;
            print("no ubicar");
          }

          if (aumentador1 == actualizarUbicacion[0]["tiempo"]) {
            aumentador1 = 0;
            servicioEstado = true;
          }
        } else if (actualizarUbicacion[0]["ubicacion"] == "ubicar") {
          if (aumentador2 == 15) {
            aumentador2 = 0;
            obtenerUbicacionUsuario();
            print("ubicar");
          }

          if (aumentador1 == actualizarUbicacion[0]["tiempo"]) {
            aumentador1 = 0;
            servicioEstado = true;
          }
        }
      }
    }

    final cacheDir = await getTemporaryDirectory();

    if (cacheDir.existsSync()) {
      cacheDir.deleteSync(recursive: true);
    }
  }

  Future<void> showMyDialogSignOut() async {
    return showDialog<void>(
      context: context,
      barrierDismissible: false, // user must tap button!
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text(
            'Cuenta',
            style: TextStyle(
              color: Colors.grey[600],
              fontFamily: 'OpenSans',
            ),
          ),
          content: SingleChildScrollView(
            child: ListBody(
              children: <Widget>[
                Text(
                  'Quieres cerrar sesion ?',
                  style: TextStyle(
                    color: Colors.grey,
                    fontFamily: 'OpenSans',
                  ),
                ),
              ],
            ),
          ),
          actions: <Widget>[
            TextButton(
              child: Text(
                'Si',
                style: TextStyle(
                  color: Colors.grey[600],
                  fontFamily: 'OpenSans',
                ),
              ),
              onPressed: () {
                stopActualizarServicio();
                stopVerificarHoraServicio();
                appData.timer?.cancel();

                Navigator.of(context).pushNamedAndRemoveUntil(
                    "/login", (Route<dynamic> route) => false);
              },
            ),
            TextButton(
              child: Text(
                'No',
                style: TextStyle(
                  color: Colors.grey[600],
                  fontFamily: 'OpenSans',
                ),
              ),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }

  Widget _buildFechaTF() {
    return new GestureDetector(
        onTap: () => _selectTime(context),
        child: Container(
            alignment: Alignment.centerLeft,
            decoration: BoxDecoration(
              color: Colors.grey[100],
              borderRadius:
                  BorderRadius.circular(SizeConfig.safeBlockHorizontal * 4),
            ),
            child: Padding(
              padding: EdgeInsets.only(left: 0),
              child: AbsorbPointer(
                child: TextFormField(
                  controller: servicio,
                  keyboardType: TextInputType.text,
                  style: TextStyle(
                    color: Colors.black38,
                    fontFamily: 'Opensans',
                    fontSize: SizeConfig.safeBlockHorizontal * 4,
                  ),
                  decoration: InputDecoration(
                    contentPadding:
                        EdgeInsets.all(SizeConfig.safeBlockVertical * 2),
                    border: InputBorder.none,
                    hintText: 'Buscar',
                    hintStyle: TextStyle(
                      color: Colors.black26,
                      fontFamily: 'Opensans',
                      fontSize: SizeConfig.safeBlockHorizontal * 4,
                    ),
                  ),
                ),
              ),
            )));
  }

  Widget buildBuscarTF() {
    return Container(
      alignment: Alignment.centerLeft,
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.circular(SizeConfig.safeBlockHorizontal * 4),
        boxShadow: [
          BoxShadow(
            color: Colors.black12,
            blurRadius: 6.0,
            offset: Offset(0, 2),
          ),
        ],
      ),
      child: new TextField(
        controller: servicio,
        keyboardType: TextInputType.emailAddress,
        style: TextStyle(
          color: Colors.black38,
          fontFamily: 'OpenSans',
          fontSize: SizeConfig.safeBlockHorizontal * 4,
        ),
        decoration: new InputDecoration(
          border: InputBorder.none,
          contentPadding: EdgeInsets.all(SizeConfig.safeBlockVertical * 2),
          hintText: 'Buscar',
          hintStyle: TextStyle(
            color: Colors.black26,
            fontFamily: 'Opensans',
          ),
        ),
      ),
    );
  }

  Widget buildSocialBtn() {
    return GestureDetector(
      onTap: () {},
      child: Container(
        margin: EdgeInsets.only(left: SizeConfig.safeBlockHorizontal * 2.0),
        width: 20,
        height: 20,
        decoration: BoxDecoration(
          shape: BoxShape.circle,
          image: DecorationImage(
              image: AssetImage("assets/perfil2.jpg"), fit: BoxFit.cover),
        ),
      ),
    );
  }

  Widget buildBuscarBtn() {
    return GestureDetector(
      onTap: () {
        if (mounted)
          setState(() {
            buscar = true;
          });

        if (buscar == true) {
          ServicioUsuario();
        }
      },
      child: Container(
        margin: EdgeInsets.only(right: SizeConfig.safeBlockHorizontal * 2),
        width: 20,
        height: 20,
        decoration: BoxDecoration(
          shape: BoxShape.circle,
          image: DecorationImage(
              image: AssetImage("assets/buscar.jpg"), fit: BoxFit.cover),
        ),
      ),
    );
  }

  Widget buildActualizarBtn() {
    Map parametros = ModalRoute.of(context).settings.arguments;
    return GestureDetector(
      onTap: () {
        stopActualizarServicio();
        stopVerificarHoraServicio();
        appData.timer?.cancel();

        Navigator.pushNamed(context, "/inicio", arguments: {
          'id': parametros['id'],
          'nombre': parametros['nombre'],
          'documento': parametros['documento'],
          'username': parametros['username'],
          'password': parametros['password'],
        });
      },
      child: Container(
        margin: EdgeInsets.only(left: SizeConfig.safeBlockHorizontal * 1.0),
        width: 20,
        height: 20,
        decoration: BoxDecoration(
          shape: BoxShape.circle,
          image: DecorationImage(
              image: AssetImage("assets/actualizar.png"), fit: BoxFit.cover),
        ),
      ),
    );
  }

  Widget buildInicioBtn() {
    Map parametros = ModalRoute.of(context).settings.arguments;
    return GestureDetector(
      onTap: () {
        stopActualizarServicio();
        stopVerificarHoraServicio();
        appData.timer?.cancel();

        Navigator.pushNamed(context, "/inicio", arguments: {
          'id': parametros['id'],
          'nombre': parametros['nombre'],
          'documento': parametros['documento'],
          'username': parametros['username'],
          'password': parametros['password'],
        });
      },
      child: Container(
        margin: EdgeInsets.only(left: SizeConfig.safeBlockHorizontal * 1.0),
        width: 20,
        height: 20,
        decoration: BoxDecoration(
          shape: BoxShape.circle,
          image: DecorationImage(
              image: AssetImage("assets/inicio2.jpg"), fit: BoxFit.cover),
        ),
      ),
    );
  }

  Widget buildPerfilBtn() {
    Map parametros = ModalRoute.of(context).settings.arguments;
    return GestureDetector(
      onTap: () {
        stopActualizarServicio();

        Navigator.pushNamed(context, "/inicio/perfil", arguments: {
          'id': parametros['id'],
          'nombre': parametros['nombre'],
          'documento': parametros['documento'],
          'username': parametros['username'],
          'password': parametros['password'],
        });
      },
      child: Container(
        margin: EdgeInsets.only(left: SizeConfig.safeBlockHorizontal * 1.0),
        width: 20,
        height: 20,
        decoration: BoxDecoration(
          shape: BoxShape.circle,
          image: DecorationImage(
              image: AssetImage("assets/perfil_inicio.jpg"), fit: BoxFit.cover),
        ),
      ),
    );
  }

  Widget buildMapBtn() {
    return GestureDetector(
      onTap: () {
        showMyDialogSignOut();
      },
      child: Container(
        margin: EdgeInsets.only(left: SizeConfig.safeBlockHorizontal * 1.0),
        width: 20,
        height: 20,
        decoration: BoxDecoration(
          shape: BoxShape.circle,
          image: DecorationImage(
              image: AssetImage("assets/cerrar_inicio.jpg"), fit: BoxFit.cover),
        ),
      ),
    );
  }

  Widget buildInformationBtn() {
    Map parametros = ModalRoute.of(context).settings.arguments;
    return GestureDetector(
      onTap: () {
        stopActualizarServicio();

        Navigator.pushNamed(context, "/inicio/informacion", arguments: {
          'id': parametros['id'],
          'nombre': parametros['nombre'],
          'documento': parametros['documento'],
          'username': parametros['username'],
          'password': parametros['password'],
        });
      },
      child: Container(
        margin: EdgeInsets.only(left: SizeConfig.safeBlockHorizontal * 1.0),
        width: 20,
        height: 20,
        decoration: BoxDecoration(
          shape: BoxShape.circle,
          image: DecorationImage(
              image: AssetImage("assets/informacion.jpg"), fit: BoxFit.cover),
        ),
      ),
    );
  }

  Widget ServicioUsuario() {
    String element;
    int aumentador = 1;
    bool verificador = false;
    element = serviciosUsuario.toString();
    for (int i = 0; i < element.length; i++) {
      String caracter = "";
      caracter = element.substring(i, aumentador);
      if (caracter == "[") {
        verificador = true;
      }
      aumentador++;
    }
    if (verificador == false) {
      return Container(
        width: SizeConfig.safeBlockHorizontal * 100,
        height: SizeConfig.safeBlockVertical * 48.5,
        child: Center(
          child: Text(
            "No hay servicios",
            style: TextStyle(
              color: Colors.black26,
              fontWeight: FontWeight.bold,
              fontFamily: 'OpenSans',
              fontSize: SizeConfig.safeBlockHorizontal * 4.5,
            ),
          ),
        ),
      );
    } else {
      if (servicio.text.isEmpty || buscar == false) {
        return ListView.builder(
            itemCount: serviciosUsuario == null ? 0 : serviciosUsuario.length,
            itemBuilder: (context, i) {
              Map parametros = ModalRoute.of(context).settings.arguments;
              String id_horario = serviciosUsuario[i]['id_horario'].toString();
              String id_conductor = parametros['id'].toString();
              String userNameConductor = parametros['username'];
              String nombre = serviciosUsuario[i]['nom_paciente'];
              String userName = nombre.toUpperCase();
              return Column(
                children: [
                  if (serviciosUsuario[i]['estado'] == "activo") ...[
                    new GestureDetector(
                        onTap: () => Navigator.pushNamed(
                                context, "/inicio/servicio",
                                arguments: {
                                  'id_horario': serviciosUsuario[i]
                                          ['id_horario']
                                      .toString(),
                                  'descripcion': serviciosUsuario[i]
                                      ['descripcion'],
                                  'direccion_llegada': serviciosUsuario[i]
                                      ['direccion_llegada'],
                                  'direccion_salida': serviciosUsuario[i]
                                      ['direccion_salida'],
                                  'ciudad': serviciosUsuario[i]['ciudad'],
                                  'nom_paciente': userName,
                                  'documento': serviciosUsuario[i]['documento']
                                      .toString(),
                                  'id_paciente': serviciosUsuario[i]
                                          ['id_paciente']
                                      .toString(),
                                  'fecha_hora_servicio': serviciosUsuario[i]
                                      ['fecha_hora_servicio'],
                                  'id': parametros['id'],
                                  'nombre': parametros['nombre'],
                                  'username': parametros['username'],
                                  'password': parametros['password'],
                                }),
                        child: Column(
                          children: [
                            Container(
                              width: SizeConfig.safeBlockHorizontal * 100,
                              height: SizeConfig.safeBlockVertical * 13,
                              child: Row(
                                children: [
                                  Container(
                                    width: SizeConfig.safeBlockHorizontal * 4.5,
                                    height: SizeConfig.safeBlockVertical * 100,
                                  ),
                                  Container(
                                      width:
                                          SizeConfig.safeBlockHorizontal * 81,
                                      height:
                                          SizeConfig.safeBlockVertical * 100,
                                      decoration: BoxDecoration(
                                        color: Colors.green[100],
                                        borderRadius: BorderRadius.circular(
                                            SizeConfig.safeBlockHorizontal * 4),
                                      ),
                                      child: Row(
                                        children: [
                                          Container(
                                            width:
                                                SizeConfig.safeBlockHorizontal *
                                                    5,
                                            height:
                                                SizeConfig.safeBlockVertical *
                                                    100,
                                          ),
                                          Container(
                                            width:
                                                SizeConfig.safeBlockHorizontal *
                                                    71,
                                            height:
                                                SizeConfig.safeBlockVertical *
                                                    100,
                                            child: Column(
                                              children: [
                                                Container(
                                                  width: SizeConfig
                                                          .safeBlockHorizontal *
                                                      100,
                                                  height: SizeConfig
                                                          .safeBlockVertical *
                                                      5,
                                                  child: Padding(
                                                    padding: EdgeInsets.only(
                                                        top: SizeConfig
                                                                .safeBlockVertical *
                                                            2),
                                                    child: Text(
                                                      userName,
                                                      style: TextStyle(
                                                        color: Colors.black38,
                                                        fontWeight:
                                                            FontWeight.bold,
                                                        fontFamily: 'OpenSans',
                                                        fontSize: SizeConfig
                                                                .safeBlockHorizontal *
                                                            3.7,
                                                      ),
                                                    ),
                                                  ),
                                                ),
                                                Container(
                                                  width: SizeConfig
                                                          .safeBlockHorizontal *
                                                      100,
                                                  height: SizeConfig
                                                          .safeBlockVertical *
                                                      8,
                                                  child: Column(
                                                    children: [
                                                      Row(
                                                        children: [
                                                          Container(
                                                            width: SizeConfig
                                                                    .safeBlockHorizontal *
                                                                45,
                                                            height: SizeConfig
                                                                    .safeBlockVertical *
                                                                6,
                                                            child: Padding(
                                                              padding: EdgeInsets.only(
                                                                  top: SizeConfig
                                                                          .safeBlockVertical *
                                                                      2),
                                                              child: Text(
                                                                serviciosUsuario[
                                                                        i][
                                                                    'fecha_hora_servicio'],
                                                                style:
                                                                    TextStyle(
                                                                  color: Colors
                                                                      .black26,
                                                                  fontWeight:
                                                                      FontWeight
                                                                          .bold,
                                                                  fontFamily:
                                                                      'OpenSans',
                                                                  fontSize:
                                                                      SizeConfig
                                                                              .safeBlockHorizontal *
                                                                          4,
                                                                ),
                                                              ),
                                                            ),
                                                          ),
                                                          Container(
                                                            width: SizeConfig
                                                                    .safeBlockHorizontal *
                                                                25,
                                                            height: SizeConfig
                                                                    .safeBlockVertical *
                                                                6,
                                                            child: Padding(
                                                              padding: EdgeInsets.only(
                                                                  top: SizeConfig
                                                                          .safeBlockVertical *
                                                                      2),
                                                              child: Text(
                                                                "( Vigente )",
                                                                style:
                                                                    TextStyle(
                                                                  color: Colors
                                                                      .white,
                                                                  fontWeight:
                                                                      FontWeight
                                                                          .bold,
                                                                  fontFamily:
                                                                      'OpenSans',
                                                                  fontSize:
                                                                      SizeConfig
                                                                              .safeBlockHorizontal *
                                                                          4,
                                                                ),
                                                              ),
                                                            ),
                                                          ),
                                                        ],
                                                      ),
                                                      Container(
                                                        width: SizeConfig
                                                                .safeBlockHorizontal *
                                                            100,
                                                        height: SizeConfig
                                                                .safeBlockVertical *
                                                            2,
                                                      ),
                                                    ],
                                                  ),
                                                ),
                                              ],
                                            ),
                                          ),
                                          Container(
                                            width:
                                                SizeConfig.safeBlockHorizontal *
                                                    5,
                                            height:
                                                SizeConfig.safeBlockVertical *
                                                    100,
                                          ),
                                        ],
                                      )),
                                  Container(
                                    width: SizeConfig.safeBlockHorizontal * 4.5,
                                    height: SizeConfig.safeBlockVertical * 100,
                                  ),
                                ],
                              ),
                            ),
                            Container(
                              width: SizeConfig.safeBlockHorizontal * 100,
                              height: SizeConfig.safeBlockVertical * 1,
                            ),
                          ],
                        )),
                  ] else ...[
                    new GestureDetector(
                        onTap: () => Navigator.pushNamed(
                                context, "/inicio/servicio",
                                arguments: {
                                  'id_horario': serviciosUsuario[i]
                                          ['id_horario']
                                      .toString(),
                                  'descripcion': serviciosUsuario[i]
                                      ['descripcion'],
                                  'direccion_llegada': serviciosUsuario[i]
                                      ['direccion_llegada'],
                                  'direccion_salida': serviciosUsuario[i]
                                      ['direccion_salida'],
                                  'ciudad': serviciosUsuario[i]['ciudad'],
                                  'nom_paciente': userName,
                                  'documento': serviciosUsuario[i]['documento']
                                      .toString(),
                                  'id_paciente': serviciosUsuario[i]
                                          ['id_paciente']
                                      .toString(),
                                  'fecha_hora_servicio': serviciosUsuario[i]
                                      ['fecha_hora_servicio'],
                                  'id': parametros['id'],
                                  'nombre': parametros['nombre'],
                                  'username': parametros['username'],
                                  'password': parametros['password'],
                                }),
                        child: Column(
                          children: [
                            Container(
                              width: SizeConfig.safeBlockHorizontal * 100,
                              height: SizeConfig.safeBlockVertical * 13,
                              child: Row(
                                children: [
                                  Container(
                                    width: SizeConfig.safeBlockHorizontal * 4.5,
                                    height: SizeConfig.safeBlockVertical * 100,
                                  ),
                                  Container(
                                      width:
                                          SizeConfig.safeBlockHorizontal * 81,
                                      height:
                                          SizeConfig.safeBlockVertical * 100,
                                      decoration: BoxDecoration(
                                        color: Colors.red[100],
                                        borderRadius: BorderRadius.circular(
                                            SizeConfig.safeBlockHorizontal * 4),
                                      ),
                                      child: Row(
                                        children: [
                                          Container(
                                            width:
                                                SizeConfig.safeBlockHorizontal *
                                                    5,
                                            height:
                                                SizeConfig.safeBlockVertical *
                                                    100,
                                          ),
                                          Container(
                                            width:
                                                SizeConfig.safeBlockHorizontal *
                                                    71,
                                            height:
                                                SizeConfig.safeBlockVertical *
                                                    100,
                                            child: Column(
                                              children: [
                                                Container(
                                                  width: SizeConfig
                                                          .safeBlockHorizontal *
                                                      100,
                                                  height: SizeConfig
                                                          .safeBlockVertical *
                                                      5,
                                                  child: Padding(
                                                    padding: EdgeInsets.only(
                                                        top: SizeConfig
                                                                .safeBlockVertical *
                                                            2),
                                                    child: Text(
                                                      userName,
                                                      style: TextStyle(
                                                        color: Colors.black38,
                                                        fontWeight:
                                                            FontWeight.bold,
                                                        fontFamily: 'OpenSans',
                                                        fontSize: SizeConfig
                                                                .safeBlockHorizontal *
                                                            3.7,
                                                      ),
                                                    ),
                                                  ),
                                                ),
                                                Container(
                                                  width: SizeConfig
                                                          .safeBlockHorizontal *
                                                      100,
                                                  height: SizeConfig
                                                          .safeBlockVertical *
                                                      8,
                                                  child: Column(
                                                    children: [
                                                      Row(
                                                        children: [
                                                          Container(
                                                            width: SizeConfig
                                                                    .safeBlockHorizontal *
                                                                45,
                                                            height: SizeConfig
                                                                    .safeBlockVertical *
                                                                6,
                                                            child: Padding(
                                                              padding: EdgeInsets.only(
                                                                  top: SizeConfig
                                                                          .safeBlockVertical *
                                                                      2),
                                                              child: Text(
                                                                serviciosUsuario[
                                                                        i][
                                                                    'fecha_hora_servicio'],
                                                                style:
                                                                    TextStyle(
                                                                  color: Colors
                                                                      .black26,
                                                                  fontWeight:
                                                                      FontWeight
                                                                          .bold,
                                                                  fontFamily:
                                                                      'OpenSans',
                                                                  fontSize:
                                                                      SizeConfig
                                                                              .safeBlockHorizontal *
                                                                          4,
                                                                ),
                                                              ),
                                                            ),
                                                          ),
                                                          Container(
                                                            width: SizeConfig
                                                                    .safeBlockHorizontal *
                                                                25,
                                                            height: SizeConfig
                                                                    .safeBlockVertical *
                                                                6,
                                                            child: Padding(
                                                              padding: EdgeInsets.only(
                                                                  top: SizeConfig
                                                                          .safeBlockVertical *
                                                                      2),
                                                              child: Text(
                                                                "( Vencido )",
                                                                style:
                                                                    TextStyle(
                                                                  color: Colors
                                                                      .white,
                                                                  fontWeight:
                                                                      FontWeight
                                                                          .bold,
                                                                  fontFamily:
                                                                      'OpenSans',
                                                                  fontSize:
                                                                      SizeConfig
                                                                              .safeBlockHorizontal *
                                                                          4,
                                                                ),
                                                              ),
                                                            ),
                                                          ),
                                                        ],
                                                      ),
                                                      Container(
                                                        width: SizeConfig
                                                                .safeBlockHorizontal *
                                                            100,
                                                        height: SizeConfig
                                                                .safeBlockVertical *
                                                            2,
                                                      ),
                                                    ],
                                                  ),
                                                ),
                                              ],
                                            ),
                                          ),
                                          Container(
                                            width:
                                                SizeConfig.safeBlockHorizontal *
                                                    5,
                                            height:
                                                SizeConfig.safeBlockVertical *
                                                    100,
                                          ),
                                        ],
                                      )),
                                  Container(
                                    width: SizeConfig.safeBlockHorizontal * 4.5,
                                    height: SizeConfig.safeBlockVertical * 100,
                                  ),
                                ],
                              ),
                            ),
                            Container(
                              width: SizeConfig.safeBlockHorizontal * 100,
                              height: SizeConfig.safeBlockVertical * 1,
                            ),
                          ],
                        )),
                  ]
                ],
              );
            });
      } else {
        stopActualizarServicio();

        String servicioHora = servicio.text.toString();
        var arrayServicioHora = servicioHora.split(":");
        String conntenedor_hora = "";
        String contenedorEstadoDia = "";
        String servicio_hora = arrayServicioHora[0].toString();
        String servicio_min = arrayServicioHora[1].toString();
        for (int i = 0; i <= 23; i++) {
          String contenedor = "";
          if (i <= 9) {
            contenedor = "0" + i.toString();
          } else {
            contenedor = i.toString();
          }

          if (contenedor == servicio_hora) {
            if (i == 0) {
              contenedorEstadoDia = "AM";
              conntenedor_hora = "12";
            } else if (i <= 11) {
              contenedorEstadoDia = "AM";
              if (i <= 9) {
                conntenedor_hora = "0" + i.toString();
              } else {
                conntenedor_hora = i.toString();
              }
            } else if (i >= 12) {
              contenedorEstadoDia = "PM";
              if (i == 12) {
                conntenedor_hora = i.toString();
              } else if (i == 13) {
                conntenedor_hora = "01";
              } else if (i == 14) {
                conntenedor_hora = "02";
              } else if (i == 15) {
                conntenedor_hora = "03";
              } else if (i == 16) {
                conntenedor_hora = "04";
              } else if (i == 17) {
                conntenedor_hora = "05";
              } else if (i == 18) {
                conntenedor_hora = "06";
              } else if (i == 19) {
                conntenedor_hora = "07";
              } else if (i == 20) {
                conntenedor_hora = "08";
              } else if (i == 21) {
                conntenedor_hora = "09";
              } else if (i == 22) {
                conntenedor_hora = "10";
              } else if (i == 23) {
                conntenedor_hora = "11";
              }
            }
          }
        }

        if (arrayServicioHora[1].toString() == "00") {
          var arrayServicio = [];
          for (int i = 0; i < serviciosUsuario.length; i++) {
            String contenedorEstadoDiaServicio =
                serviciosUsuario[i]["fecha_hora_servicio"];
            var fecha_servicio = contenedorEstadoDiaServicio.split(" ");
            String estadoServicio = fecha_servicio[2].toString();

            if (contenedorEstadoDia == estadoServicio) {
              String array = "";
              array = fecha_servicio[1];
              var contenedorArray = array.split(":");
              String hora = contenedorArray[0].toString();
              if (conntenedor_hora == hora) {
                arrayServicio.add(serviciosUsuario[i]);
              }
            }
          }
          if (arrayServicio.length == 0) {
            return Container(
              width: SizeConfig.safeBlockHorizontal * 100,
              height: SizeConfig.safeBlockVertical * 48.5,
              child: Center(
                child: Text(
                  "No hay servicios",
                  style: TextStyle(
                    color: Colors.black26,
                    fontWeight: FontWeight.bold,
                    fontFamily: 'OpenSans',
                    fontSize: SizeConfig.safeBlockHorizontal * 4.5,
                  ),
                ),
              ),
            );
          } else {
            var convertirArray = jsonEncode(arrayServicio);
            var servicio_fecha = jsonDecode(convertirArray);
            return ListView.builder(
                itemCount: servicio_fecha == null ? 0 : servicio_fecha.length,
                itemBuilder: (context, i) {
                  Map parametros = ModalRoute.of(context).settings.arguments;
                  String id_horario =
                      servicio_fecha[i]['id_horario'].toString();
                  String id_conductor = parametros['id'].toString();
                  String userNameConductor = parametros['username'];
                  String nombre = servicio_fecha[i]['nom_paciente'];
                  String userName = nombre.toUpperCase();
                  return Column(
                    children: [
                      if (serviciosUsuario[i]['estado'] == "activo") ...[
                        new GestureDetector(
                            onTap: () => Navigator.pushNamed(
                                    context, "/inicio/servicio",
                                    arguments: {
                                      'id_horario': serviciosUsuario[i]
                                              ['id_horario']
                                          .toString(),
                                      'descripcion': serviciosUsuario[i]
                                          ['descripcion'],
                                      'direccion_llegada': serviciosUsuario[i]
                                          ['direccion_llegada'],
                                      'direccion_salida': serviciosUsuario[i]
                                          ['direccion_salida'],
                                      'ciudad': serviciosUsuario[i]['ciudad'],
                                      'nom_paciente': userName,
                                      'documento': serviciosUsuario[i]
                                              ['documento']
                                          .toString(),
                                      'id_paciente': serviciosUsuario[i]
                                              ['id_paciente']
                                          .toString(),
                                      'fecha_hora_servicio': serviciosUsuario[i]
                                          ['fecha_hora_servicio'],
                                      'id': parametros['id'],
                                      'nombre': parametros['nombre'],
                                      'username': parametros['username'],
                                      'password': parametros['password'],
                                    }),
                            child: Column(
                              children: [
                                Container(
                                  width: SizeConfig.safeBlockHorizontal * 100,
                                  height: SizeConfig.safeBlockVertical * 13,
                                  child: Row(
                                    children: [
                                      Container(
                                        width: SizeConfig.safeBlockHorizontal *
                                            4.5,
                                        height:
                                            SizeConfig.safeBlockVertical * 100,
                                      ),
                                      Container(
                                          width:
                                              SizeConfig.safeBlockHorizontal *
                                                  81,
                                          height: SizeConfig.safeBlockVertical *
                                              100,
                                          decoration: BoxDecoration(
                                            color: Colors.green[100],
                                            borderRadius: BorderRadius.circular(
                                                SizeConfig.safeBlockHorizontal *
                                                    4),
                                          ),
                                          child: Row(
                                            children: [
                                              Container(
                                                width: SizeConfig
                                                        .safeBlockHorizontal *
                                                    5,
                                                height: SizeConfig
                                                        .safeBlockVertical *
                                                    100,
                                              ),
                                              Container(
                                                width: SizeConfig
                                                        .safeBlockHorizontal *
                                                    71,
                                                height: SizeConfig
                                                        .safeBlockVertical *
                                                    100,
                                                child: Column(
                                                  children: [
                                                    Container(
                                                      width: SizeConfig
                                                              .safeBlockHorizontal *
                                                          100,
                                                      height: SizeConfig
                                                              .safeBlockVertical *
                                                          5,
                                                      child: Padding(
                                                        padding: EdgeInsets.only(
                                                            top: SizeConfig
                                                                    .safeBlockVertical *
                                                                2),
                                                        child: Text(
                                                          userName,
                                                          style: TextStyle(
                                                            color:
                                                                Colors.black38,
                                                            fontWeight:
                                                                FontWeight.bold,
                                                            fontFamily:
                                                                'OpenSans',
                                                            fontSize: SizeConfig
                                                                    .safeBlockHorizontal *
                                                                3.7,
                                                          ),
                                                        ),
                                                      ),
                                                    ),
                                                    Container(
                                                      width: SizeConfig
                                                              .safeBlockHorizontal *
                                                          100,
                                                      height: SizeConfig
                                                              .safeBlockVertical *
                                                          8,
                                                      child: Column(
                                                        children: [
                                                          Row(
                                                            children: [
                                                              Container(
                                                                width: SizeConfig
                                                                        .safeBlockHorizontal *
                                                                    45,
                                                                height: SizeConfig
                                                                        .safeBlockVertical *
                                                                    6,
                                                                child: Padding(
                                                                  padding: EdgeInsets.only(
                                                                      top: SizeConfig
                                                                              .safeBlockVertical *
                                                                          2),
                                                                  child: Text(
                                                                    serviciosUsuario[
                                                                            i][
                                                                        'fecha_hora_servicio'],
                                                                    style:
                                                                        TextStyle(
                                                                      color: Colors
                                                                          .black26,
                                                                      fontWeight:
                                                                          FontWeight
                                                                              .bold,
                                                                      fontFamily:
                                                                          'OpenSans',
                                                                      fontSize:
                                                                          SizeConfig.safeBlockHorizontal *
                                                                              4,
                                                                    ),
                                                                  ),
                                                                ),
                                                              ),
                                                              Container(
                                                                width: SizeConfig
                                                                        .safeBlockHorizontal *
                                                                    25,
                                                                height: SizeConfig
                                                                        .safeBlockVertical *
                                                                    6,
                                                                child: Padding(
                                                                  padding: EdgeInsets.only(
                                                                      top: SizeConfig
                                                                              .safeBlockVertical *
                                                                          2),
                                                                  child: Text(
                                                                    "( Vigente )",
                                                                    style:
                                                                        TextStyle(
                                                                      color: Colors
                                                                          .white,
                                                                      fontWeight:
                                                                          FontWeight
                                                                              .bold,
                                                                      fontFamily:
                                                                          'OpenSans',
                                                                      fontSize:
                                                                          SizeConfig.safeBlockHorizontal *
                                                                              4,
                                                                    ),
                                                                  ),
                                                                ),
                                                              ),
                                                            ],
                                                          ),
                                                          Container(
                                                            width: SizeConfig
                                                                    .safeBlockHorizontal *
                                                                100,
                                                            height: SizeConfig
                                                                    .safeBlockVertical *
                                                                2,
                                                          ),
                                                        ],
                                                      ),
                                                    ),
                                                  ],
                                                ),
                                              ),
                                              Container(
                                                width: SizeConfig
                                                        .safeBlockHorizontal *
                                                    5,
                                                height: SizeConfig
                                                        .safeBlockVertical *
                                                    100,
                                              ),
                                            ],
                                          )),
                                      Container(
                                        width: SizeConfig.safeBlockHorizontal *
                                            4.5,
                                        height:
                                            SizeConfig.safeBlockVertical * 100,
                                      ),
                                    ],
                                  ),
                                ),
                                Container(
                                  width: SizeConfig.safeBlockHorizontal * 100,
                                  height: SizeConfig.safeBlockVertical * 1,
                                ),
                              ],
                            )),
                      ] else ...[
                        new GestureDetector(
                            onTap: () => Navigator.pushNamed(
                                    context, "/inicio/servicio",
                                    arguments: {
                                      'id_horario': serviciosUsuario[i]
                                              ['id_horario']
                                          .toString(),
                                      'descripcion': serviciosUsuario[i]
                                          ['descripcion'],
                                      'direccion_llegada': serviciosUsuario[i]
                                          ['direccion_llegada'],
                                      'direccion_salida': serviciosUsuario[i]
                                          ['direccion_salida'],
                                      'ciudad': serviciosUsuario[i]['ciudad'],
                                      'nom_paciente': userName,
                                      'documento': serviciosUsuario[i]
                                              ['documento']
                                          .toString(),
                                      'id_paciente': serviciosUsuario[i]
                                              ['id_paciente']
                                          .toString(),
                                      'fecha_hora_servicio': serviciosUsuario[i]
                                          ['fecha_hora_servicio'],
                                      'id': parametros['id'],
                                      'nombre': parametros['nombre'],
                                      'username': parametros['username'],
                                      'password': parametros['password'],
                                    }),
                            child: Column(
                              children: [
                                Container(
                                  width: SizeConfig.safeBlockHorizontal * 100,
                                  height: SizeConfig.safeBlockVertical * 13,
                                  child: Row(
                                    children: [
                                      Container(
                                        width: SizeConfig.safeBlockHorizontal *
                                            4.5,
                                        height:
                                            SizeConfig.safeBlockVertical * 100,
                                      ),
                                      Container(
                                          width:
                                              SizeConfig.safeBlockHorizontal *
                                                  81,
                                          height: SizeConfig.safeBlockVertical *
                                              100,
                                          decoration: BoxDecoration(
                                            color: Colors.red[100],
                                            borderRadius: BorderRadius.circular(
                                                SizeConfig.safeBlockHorizontal *
                                                    4),
                                          ),
                                          child: Row(
                                            children: [
                                              Container(
                                                width: SizeConfig
                                                        .safeBlockHorizontal *
                                                    5,
                                                height: SizeConfig
                                                        .safeBlockVertical *
                                                    100,
                                              ),
                                              Container(
                                                width: SizeConfig
                                                        .safeBlockHorizontal *
                                                    71,
                                                height: SizeConfig
                                                        .safeBlockVertical *
                                                    100,
                                                child: Column(
                                                  children: [
                                                    Container(
                                                      width: SizeConfig
                                                              .safeBlockHorizontal *
                                                          100,
                                                      height: SizeConfig
                                                              .safeBlockVertical *
                                                          5,
                                                      child: Padding(
                                                        padding: EdgeInsets.only(
                                                            top: SizeConfig
                                                                    .safeBlockVertical *
                                                                2),
                                                        child: Text(
                                                          userName,
                                                          style: TextStyle(
                                                            color:
                                                                Colors.black38,
                                                            fontWeight:
                                                                FontWeight.bold,
                                                            fontFamily:
                                                                'OpenSans',
                                                            fontSize: SizeConfig
                                                                    .safeBlockHorizontal *
                                                                3.7,
                                                          ),
                                                        ),
                                                      ),
                                                    ),
                                                    Container(
                                                      width: SizeConfig
                                                              .safeBlockHorizontal *
                                                          100,
                                                      height: SizeConfig
                                                              .safeBlockVertical *
                                                          8,
                                                      child: Column(
                                                        children: [
                                                          Row(
                                                            children: [
                                                              Container(
                                                                width: SizeConfig
                                                                        .safeBlockHorizontal *
                                                                    45,
                                                                height: SizeConfig
                                                                        .safeBlockVertical *
                                                                    6,
                                                                child: Padding(
                                                                  padding: EdgeInsets.only(
                                                                      top: SizeConfig
                                                                              .safeBlockVertical *
                                                                          2),
                                                                  child: Text(
                                                                    serviciosUsuario[
                                                                            i][
                                                                        'fecha_hora_servicio'],
                                                                    style:
                                                                        TextStyle(
                                                                      color: Colors
                                                                          .black26,
                                                                      fontWeight:
                                                                          FontWeight
                                                                              .bold,
                                                                      fontFamily:
                                                                          'OpenSans',
                                                                      fontSize:
                                                                          SizeConfig.safeBlockHorizontal *
                                                                              4,
                                                                    ),
                                                                  ),
                                                                ),
                                                              ),
                                                              Container(
                                                                width: SizeConfig
                                                                        .safeBlockHorizontal *
                                                                    25,
                                                                height: SizeConfig
                                                                        .safeBlockVertical *
                                                                    6,
                                                                child: Padding(
                                                                  padding: EdgeInsets.only(
                                                                      top: SizeConfig
                                                                              .safeBlockVertical *
                                                                          2),
                                                                  child: Text(
                                                                    "( Vencido )",
                                                                    style:
                                                                        TextStyle(
                                                                      color: Colors
                                                                          .white,
                                                                      fontWeight:
                                                                          FontWeight
                                                                              .bold,
                                                                      fontFamily:
                                                                          'OpenSans',
                                                                      fontSize:
                                                                          SizeConfig.safeBlockHorizontal *
                                                                              4,
                                                                    ),
                                                                  ),
                                                                ),
                                                              ),
                                                            ],
                                                          ),
                                                          Container(
                                                            width: SizeConfig
                                                                    .safeBlockHorizontal *
                                                                100,
                                                            height: SizeConfig
                                                                    .safeBlockVertical *
                                                                2,
                                                          ),
                                                        ],
                                                      ),
                                                    ),
                                                  ],
                                                ),
                                              ),
                                              Container(
                                                width: SizeConfig
                                                        .safeBlockHorizontal *
                                                    5,
                                                height: SizeConfig
                                                        .safeBlockVertical *
                                                    100,
                                              ),
                                            ],
                                          )),
                                      Container(
                                        width: SizeConfig.safeBlockHorizontal *
                                            4.5,
                                        height:
                                            SizeConfig.safeBlockVertical * 100,
                                      ),
                                    ],
                                  ),
                                ),
                                Container(
                                  width: SizeConfig.safeBlockHorizontal * 100,
                                  height: SizeConfig.safeBlockVertical * 1,
                                ),
                              ],
                            )),
                      ]
                    ],
                  );
                });
          }
        } else {
          var arrayServicio = [];
          for (int i = 0; i < serviciosUsuario.length; i++) {
            String contenedorEstadoDiaServicio =
                serviciosUsuario[i]["fecha_hora_servicio"];
            var fecha_servicio = contenedorEstadoDiaServicio.split(" ");
            String estadoServicio = fecha_servicio[2].toString();

            if (contenedorEstadoDia == estadoServicio) {
              String array = "";
              array = fecha_servicio[1];
              String hora = conntenedor_hora + ":" + servicio_min;
              if (hora == array) {
                arrayServicio.add(serviciosUsuario[i]);
              }
            }
          }

          if (arrayServicio.length == 0) {
            return Container(
              width: SizeConfig.safeBlockHorizontal * 100,
              height: SizeConfig.safeBlockVertical * 48.5,
              child: Center(
                child: Text(
                  "No hay servicios",
                  style: TextStyle(
                    color: Colors.black26,
                    fontWeight: FontWeight.bold,
                    fontFamily: 'OpenSans',
                    fontSize: SizeConfig.safeBlockHorizontal * 4.5,
                  ),
                ),
              ),
            );
          } else {
            var convertirArray = jsonEncode(arrayServicio);
            var servicio_fecha = jsonDecode(convertirArray);
            return ListView.builder(
                itemCount: servicio_fecha == null ? 0 : servicio_fecha.length,
                itemBuilder: (context, i) {
                  Map parametros = ModalRoute.of(context).settings.arguments;
                  String id_horario =
                      servicio_fecha[i]['id_horario'].toString();
                  String id_conductor = parametros['id'].toString();
                  String userNameConductor = parametros['username'];
                  String nombre = servicio_fecha[i]['nom_paciente'];
                  String userName = nombre.toUpperCase();
                  return Column(
                    children: [
                      if (serviciosUsuario[i]['estado'] == "activo") ...[
                        new GestureDetector(
                            onTap: () => Navigator.pushNamed(
                                    context, "/inicio/servicio",
                                    arguments: {
                                      'id_horario': serviciosUsuario[i]
                                              ['id_horario']
                                          .toString(),
                                      'descripcion': serviciosUsuario[i]
                                          ['descripcion'],
                                      'direccion_llegada': serviciosUsuario[i]
                                          ['direccion_llegada'],
                                      'direccion_salida': serviciosUsuario[i]
                                          ['direccion_salida'],
                                      'ciudad': serviciosUsuario[i]['ciudad'],
                                      'nom_paciente': userName,
                                      'documento': serviciosUsuario[i]
                                              ['documento']
                                          .toString(),
                                      'id_paciente': serviciosUsuario[i]
                                              ['id_paciente']
                                          .toString(),
                                      'fecha_hora_servicio': serviciosUsuario[i]
                                          ['fecha_hora_servicio'],
                                      'id': parametros['id'],
                                      'nombre': parametros['nombre'],
                                      'username': parametros['username'],
                                      'password': parametros['password'],
                                    }),
                            child: Column(
                              children: [
                                Container(
                                  width: SizeConfig.safeBlockHorizontal * 100,
                                  height: SizeConfig.safeBlockVertical * 13,
                                  child: Row(
                                    children: [
                                      Container(
                                        width: SizeConfig.safeBlockHorizontal *
                                            4.5,
                                        height:
                                            SizeConfig.safeBlockVertical * 100,
                                      ),
                                      Container(
                                          width:
                                              SizeConfig.safeBlockHorizontal *
                                                  81,
                                          height: SizeConfig.safeBlockVertical *
                                              100,
                                          decoration: BoxDecoration(
                                            color: Colors.green[100],
                                            borderRadius: BorderRadius.circular(
                                                SizeConfig.safeBlockHorizontal *
                                                    4),
                                          ),
                                          child: Row(
                                            children: [
                                              Container(
                                                width: SizeConfig
                                                        .safeBlockHorizontal *
                                                    5,
                                                height: SizeConfig
                                                        .safeBlockVertical *
                                                    100,
                                              ),
                                              Container(
                                                width: SizeConfig
                                                        .safeBlockHorizontal *
                                                    71,
                                                height: SizeConfig
                                                        .safeBlockVertical *
                                                    100,
                                                child: Column(
                                                  children: [
                                                    Container(
                                                      width: SizeConfig
                                                              .safeBlockHorizontal *
                                                          100,
                                                      height: SizeConfig
                                                              .safeBlockVertical *
                                                          5,
                                                      child: Padding(
                                                        padding: EdgeInsets.only(
                                                            top: SizeConfig
                                                                    .safeBlockVertical *
                                                                2),
                                                        child: Text(
                                                          userName,
                                                          style: TextStyle(
                                                            color:
                                                                Colors.black38,
                                                            fontWeight:
                                                                FontWeight.bold,
                                                            fontFamily:
                                                                'OpenSans',
                                                            fontSize: SizeConfig
                                                                    .safeBlockHorizontal *
                                                                3.7,
                                                          ),
                                                        ),
                                                      ),
                                                    ),
                                                    Container(
                                                      width: SizeConfig
                                                              .safeBlockHorizontal *
                                                          100,
                                                      height: SizeConfig
                                                              .safeBlockVertical *
                                                          8,
                                                      child: Column(
                                                        children: [
                                                          Container(
                                                            width: SizeConfig
                                                                    .safeBlockHorizontal *
                                                                100,
                                                            height: SizeConfig
                                                                    .safeBlockVertical *
                                                                6,
                                                            child: Padding(
                                                              padding: EdgeInsets.only(
                                                                  top: SizeConfig
                                                                          .safeBlockVertical *
                                                                      2),
                                                              child: Text(
                                                                serviciosUsuario[
                                                                        i][
                                                                    'fecha_hora_servicio'],
                                                                style:
                                                                    TextStyle(
                                                                  color: Colors
                                                                      .black26,
                                                                  fontWeight:
                                                                      FontWeight
                                                                          .bold,
                                                                  fontFamily:
                                                                      'OpenSans',
                                                                  fontSize:
                                                                      SizeConfig
                                                                              .safeBlockHorizontal *
                                                                          4,
                                                                ),
                                                              ),
                                                            ),
                                                          ),
                                                          Container(
                                                            width: SizeConfig
                                                                    .safeBlockHorizontal *
                                                                100,
                                                            height: SizeConfig
                                                                    .safeBlockVertical *
                                                                2,
                                                          ),
                                                        ],
                                                      ),
                                                    ),
                                                  ],
                                                ),
                                              ),
                                              Container(
                                                width: SizeConfig
                                                        .safeBlockHorizontal *
                                                    4.5,
                                                height: SizeConfig
                                                        .safeBlockVertical *
                                                    100,
                                              ),
                                            ],
                                          )),
                                      Container(
                                        width:
                                            SizeConfig.safeBlockHorizontal * 5,
                                        height:
                                            SizeConfig.safeBlockVertical * 100,
                                      ),
                                    ],
                                  ),
                                ),
                                Container(
                                  width: SizeConfig.safeBlockHorizontal * 100,
                                  height: SizeConfig.safeBlockVertical * 1,
                                ),
                              ],
                            )),
                      ] else ...[
                        new GestureDetector(
                            onTap: () => Navigator.pushNamed(
                                    context, "/inicio/servicio",
                                    arguments: {
                                      'id_horario': serviciosUsuario[i]
                                              ['id_horario']
                                          .toString(),
                                      'descripcion': serviciosUsuario[i]
                                          ['descripcion'],
                                      'direccion_llegada': serviciosUsuario[i]
                                          ['direccion_llegada'],
                                      'direccion_salida': serviciosUsuario[i]
                                          ['direccion_salida'],
                                      'ciudad': serviciosUsuario[i]['ciudad'],
                                      'nom_paciente': userName,
                                      'documento': serviciosUsuario[i]
                                              ['documento']
                                          .toString(),
                                      'id_paciente': serviciosUsuario[i]
                                              ['id_paciente']
                                          .toString(),
                                      'fecha_hora_servicio': serviciosUsuario[i]
                                          ['fecha_hora_servicio'],
                                      'id': parametros['id'],
                                      'nombre': parametros['nombre'],
                                      'username': parametros['username'],
                                      'password': parametros['password'],
                                    }),
                            child: Column(
                              children: [
                                Container(
                                  width: SizeConfig.safeBlockHorizontal * 100,
                                  height: SizeConfig.safeBlockVertical * 13,
                                  child: Row(
                                    children: [
                                      Container(
                                        width: SizeConfig.safeBlockHorizontal *
                                            4.5,
                                        height:
                                            SizeConfig.safeBlockVertical * 100,
                                      ),
                                      Container(
                                          width:
                                              SizeConfig.safeBlockHorizontal *
                                                  81,
                                          height: SizeConfig.safeBlockVertical *
                                              100,
                                          decoration: BoxDecoration(
                                            color: Colors.red[100],
                                            borderRadius: BorderRadius.circular(
                                                SizeConfig.safeBlockHorizontal *
                                                    4),
                                          ),
                                          child: Row(
                                            children: [
                                              Container(
                                                width: SizeConfig
                                                        .safeBlockHorizontal *
                                                    5,
                                                height: SizeConfig
                                                        .safeBlockVertical *
                                                    100,
                                              ),
                                              Container(
                                                width: SizeConfig
                                                        .safeBlockHorizontal *
                                                    71,
                                                height: SizeConfig
                                                        .safeBlockVertical *
                                                    100,
                                                child: Column(
                                                  children: [
                                                    Container(
                                                      width: SizeConfig
                                                              .safeBlockHorizontal *
                                                          100,
                                                      height: SizeConfig
                                                              .safeBlockVertical *
                                                          5,
                                                      child: Padding(
                                                        padding: EdgeInsets.only(
                                                            top: SizeConfig
                                                                    .safeBlockVertical *
                                                                2),
                                                        child: Text(
                                                          userName,
                                                          style: TextStyle(
                                                            color:
                                                                Colors.black38,
                                                            fontWeight:
                                                                FontWeight.bold,
                                                            fontFamily:
                                                                'OpenSans',
                                                            fontSize: SizeConfig
                                                                    .safeBlockHorizontal *
                                                                3.7,
                                                          ),
                                                        ),
                                                      ),
                                                    ),
                                                    Container(
                                                      width: SizeConfig
                                                              .safeBlockHorizontal *
                                                          100,
                                                      height: SizeConfig
                                                              .safeBlockVertical *
                                                          8,
                                                      child: Column(
                                                        children: [
                                                          Container(
                                                            width: SizeConfig
                                                                    .safeBlockHorizontal *
                                                                100,
                                                            height: SizeConfig
                                                                    .safeBlockVertical *
                                                                6,
                                                            child: Padding(
                                                              padding: EdgeInsets.only(
                                                                  top: SizeConfig
                                                                          .safeBlockVertical *
                                                                      2),
                                                              child: Text(
                                                                serviciosUsuario[
                                                                        i][
                                                                    'fecha_hora_servicio'],
                                                                style:
                                                                    TextStyle(
                                                                  color: Colors
                                                                      .black26,
                                                                  fontWeight:
                                                                      FontWeight
                                                                          .bold,
                                                                  fontFamily:
                                                                      'OpenSans',
                                                                  fontSize:
                                                                      SizeConfig
                                                                              .safeBlockHorizontal *
                                                                          4,
                                                                ),
                                                              ),
                                                            ),
                                                          ),
                                                          Container(
                                                            width: SizeConfig
                                                                    .safeBlockHorizontal *
                                                                100,
                                                            height: SizeConfig
                                                                    .safeBlockVertical *
                                                                2,
                                                          ),
                                                        ],
                                                      ),
                                                    ),
                                                  ],
                                                ),
                                              ),
                                              Container(
                                                width: SizeConfig
                                                        .safeBlockHorizontal *
                                                    5,
                                                height: SizeConfig
                                                        .safeBlockVertical *
                                                    100,
                                              ),
                                            ],
                                          )),
                                      Container(
                                        width: SizeConfig.safeBlockHorizontal *
                                            4.5,
                                        height:
                                            SizeConfig.safeBlockVertical * 100,
                                      ),
                                    ],
                                  ),
                                ),
                                Container(
                                  width: SizeConfig.safeBlockHorizontal * 100,
                                  height: SizeConfig.safeBlockVertical * 1,
                                ),
                              ],
                            )),
                      ]
                    ],
                  );
                });
          }
        }
      }
    }
  }

  /*
  Widget ServicioUsuarioMapa() {

    String element;
    int aumentador = 1;
    bool verificador = false;
    element = serviciosUsuario.toString();
    for(int i = 0; i < element.length; i++){
      String caracter = "";
      caracter = element.substring(i, aumentador);
      if(caracter == "["){
        verificador = true;
      }
      aumentador++;
    }
    if(verificador == false){
      return Container(
        width: SizeConfig.safeBlockHorizontal * 100,
        height: SizeConfig.safeBlockVertical * 48.5,
        child: Center(
          child: Text(
            "No hay servicios",
            style: TextStyle(
              color: Colors.black26,
              fontWeight: FontWeight.bold,
              fontFamily: 'OpenSans',
              fontSize: SizeConfig.safeBlockHorizontal * 4.5,
            ),
          ),
        ),
      );
    }else{
      return ListView.builder(
          itemCount: serviciosUsuario == null ? 0 : serviciosUsuario.length,
          itemBuilder: (context, i){
            Map parametros = ModalRoute.of(context).settings.arguments;
            String nombre = serviciosUsuario[i]['nom_paciente'];
            String userName = nombre.toUpperCase();
            return new GestureDetector(
                onTap: () {
                  pasarMapa(serviciosUsuario[i]['id_paciente'].toString());
                },
                child: Column(
                  children: [
                    Container(
                      width: SizeConfig.blockSizeHorizontal * 100,
                      height: SizeConfig.blockSizeVertical * 12,
                      child: Row(
                        children: [
                          Container(
                            width: SizeConfig.blockSizeHorizontal * 67,
                            height: SizeConfig.blockSizeVertical * 100,
                            child: Container(
                              width: SizeConfig.safeBlockHorizontal * 100,
                              height: SizeConfig.safeBlockVertical * 100,
                              decoration: BoxDecoration(
                                color: Colors.white,
                                borderRadius: BorderRadius.circular(SizeConfig.safeBlockHorizontal * 4),
                                boxShadow: [
                                  BoxShadow(
                                    color: Colors.black26,
                                    blurRadius: 6.0,
                                    offset: Offset(0, 0),
                                  ),
                                ],
                              ),
                              child: Column(
                                children: [
                                  Container(
                                    width: SizeConfig.blockSizeHorizontal * 100,
                                    height: SizeConfig.blockSizeVertical * 2,
                                  ),
                                  Container(
                                    width: SizeConfig.blockSizeHorizontal * 100,
                                    height: SizeConfig.blockSizeVertical * 4,
                                    child: Row(
                                      children: [
                                        Container(
                                          width: SizeConfig.blockSizeHorizontal * 5,
                                          height: SizeConfig.blockSizeVertical * 100,
                                        ),
                                        Container(
                                          width: SizeConfig.blockSizeHorizontal * 57,
                                          height: SizeConfig.blockSizeVertical * 100,
                                          child: Text(
                                            userName,
                                            style: TextStyle(
                                              color: Colors.black26,
                                              fontWeight: FontWeight.bold,
                                              fontFamily: 'OpenSans',
                                              fontSize: SizeConfig.safeBlockHorizontal * 4,
                                            ),
                                          ),
                                        ),
                                        Container(
                                          width: SizeConfig.blockSizeHorizontal * 5,
                                          height: SizeConfig.blockSizeVertical * 100,
                                        ),
                                      ],
                                    ),
                                  ),
                                  Container(
                                    width: SizeConfig.blockSizeHorizontal * 100,
                                    height: SizeConfig.blockSizeVertical * 1,
                                  ),
                                  Container(
                                    width: SizeConfig.blockSizeHorizontal * 100,
                                    height: SizeConfig.blockSizeVertical * 3,
                                    child: Row(
                                      children: [
                                        Container(
                                          width: SizeConfig.blockSizeHorizontal * 5,
                                          height: SizeConfig.blockSizeVertical * 100,
                                        ),
                                        Container(
                                          width: SizeConfig.blockSizeHorizontal * 57,
                                          height: SizeConfig.blockSizeVertical * 100,
                                          child: Text(
                                            serviciosUsuario[i]['fecha_hora_servicio'],
                                            style: TextStyle(
                                              color: Colors.black26,
                                              fontWeight: FontWeight.bold,
                                              fontFamily: 'OpenSans',
                                              fontSize: SizeConfig.safeBlockHorizontal * 4,
                                            ),
                                          ),
                                        ),
                                        Container(
                                          width: SizeConfig.blockSizeHorizontal * 5,
                                          height: SizeConfig.blockSizeVertical * 100,
                                        ),
                                      ],
                                    ),
                                  ),
                                  Container(
                                    width: SizeConfig.blockSizeHorizontal * 100,
                                    height: SizeConfig.blockSizeVertical * 2,
                                  ),
                                ],
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                    Container(
                      width: SizeConfig.safeBlockHorizontal * 100,
                      height: SizeConfig.safeBlockVertical * 1,
                    ),
                  ],
                )
            );
          }
      );
    }

  }

   */

  void initState() {
    // TODO: implement initState

    WidgetsBinding.instance.addPostFrameCallback((_) async {
      if (mounted)
        setState(() {
          obtenerServicios();
        });
    });

    appData.timer = Timer.periodic(Duration(seconds: 1), (timer) {
      if (mounted)
        setState(() {
          Contenedor();
        });
    });
  }

  Future<void> showMyDialogEsperarService() async {
    Map parametros = ModalRoute.of(context).settings.arguments;
    return showDialog<void>(
      context: context,
      barrierDismissible: false, // user must tap button!
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text(
            'Conductor',
            style: TextStyle(
              color: Colors.grey[600],
              fontFamily: 'OpenSans',
            ),
          ),
          content: SingleChildScrollView(
            child: ListBody(
              children: <Widget>[
                Text(
                  'Espere que carguen los servicios',
                  style: TextStyle(
                    color: Colors.grey,
                    fontFamily: 'OpenSans',
                  ),
                ),
              ],
            ),
          ),
          actions: <Widget>[
            TextButton(
              child: Text(
                'Aceptar',
                style: TextStyle(
                  color: Colors.grey[600],
                  fontFamily: 'OpenSans',
                ),
              ),
              onPressed: () {
                stopActualizarServicio();
                stopVerificarHoraServicio();
                appData.timer?.cancel();

                Navigator.pushNamed(context, "/inicio", arguments: {
                  'id': parametros['id'],
                  'nombre': parametros['nombre'],
                  'documento': parametros['documento'],
                  'username': parametros['username'],
                  'password': parametros['password'],
                });
              },
            ),
          ],
        );
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    Map parametros = ModalRoute.of(context).settings.arguments;
    SizeConfig().init(context);
    return new WillPopScope(
        onWillPop: () async => false,
        child: Scaffold(
          floatingActionButton: Padding(
            padding: EdgeInsets.only(
                bottom: SizeConfig.safeBlockVertical * 13,
                right: SizeConfig.safeBlockHorizontal * 5),
            child: FloatingActionButton(
              backgroundColor: Colors.white,
              elevation: 5,
              child: Icon(
                Icons.add_location_sharp,
                color: Colors.blue,
              ),
              onPressed: () => {
                stopActualizarServicio(),
                if (serviciosUsuario != null)
                  {
                    appData.servicios_paciente = serviciosUsuario,
                    Navigator.pushNamed(context, "/inicio/map", arguments: {
                      'id': parametros['id'],
                      'nombre': parametros['nombre'],
                      'documento': parametros['documento'],
                      'username': parametros['username'],
                      'password': parametros['password'],
                    })
                  }
                else
                  {showMyDialogEsperarService()}
              },
            ),
          ),
          body: AnnotatedRegion<SystemUiOverlayStyle>(
            value: SystemUiOverlayStyle.light,
            child: GestureDetector(
              onTap: () => FocusScope.of(context).unfocus(),
              child: Stack(
                children: <Widget>[
                  Container(
                    height: double.infinity,
                    width: double.infinity,
                    color: Colors.white,
                  ),
                  SingleChildScrollView(
                    child: Center(
                      child: Container(
                        width: SizeConfig.safeBlockHorizontal * 100,
                        height: SizeConfig.blockSizeVertical * 100,
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: <Widget>[
                            Container(
                              width: SizeConfig.safeBlockHorizontal * 100,
                              height: SizeConfig.safeBlockVertical * 5,
                            ),
                            Center(
                              child: Container(
                                width: SizeConfig.safeBlockHorizontal * 96,
                                height: SizeConfig.safeBlockVertical * 14,
                                decoration: BoxDecoration(
                                    color: Colors.white,
                                    borderRadius: BorderRadius.circular(
                                        SizeConfig.safeBlockHorizontal * 4),
                                    border: Border.all(
                                        width: 1, color: Colors.grey)),
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.end,
                                  children: [
                                    Container(
                                      width:
                                          SizeConfig.safeBlockHorizontal * 100,
                                      height: SizeConfig.safeBlockVertical * 2,
                                    ),
                                    Container(
                                      width:
                                          SizeConfig.safeBlockHorizontal * 100,
                                      height: SizeConfig.safeBlockVertical * 9,
                                      child: Row(
                                        children: [
                                          Container(
                                            width:
                                                SizeConfig.safeBlockHorizontal *
                                                    5,
                                            height:
                                                SizeConfig.safeBlockVertical *
                                                    100,
                                          ),
                                          Container(
                                            width:
                                                SizeConfig.safeBlockHorizontal *
                                                    15,
                                            height:
                                                SizeConfig.safeBlockVertical *
                                                    100,
                                            child: buildSocialBtn(),
                                          ),
                                          Container(
                                            width:
                                                SizeConfig.safeBlockHorizontal *
                                                    2,
                                            height:
                                                SizeConfig.safeBlockVertical *
                                                    100,
                                          ),
                                          Container(
                                            width:
                                                SizeConfig.safeBlockHorizontal *
                                                    73,
                                            height:
                                                SizeConfig.safeBlockVertical *
                                                    100,
                                            child: Column(
                                              children: [
                                                Container(
                                                  width: SizeConfig
                                                          .safeBlockHorizontal *
                                                      100,
                                                  height: SizeConfig
                                                          .safeBlockVertical *
                                                      5.5,
                                                  child: Padding(
                                                    padding: EdgeInsets.only(
                                                        top: SizeConfig
                                                                .safeBlockVertical *
                                                            2),
                                                    child: FutureBuilder(
                                                        future:
                                                            obtenerUsuario(),
                                                        builder: (context,
                                                            snapshot) {
                                                          if (snapshot.hasError)
                                                            print(
                                                                snapshot.error);
                                                          return snapshot
                                                                  .hasData
                                                              ? new getByIdUsuario(
                                                                  list: snapshot
                                                                      .data)
                                                              : new Container(
                                                                  width: SizeConfig
                                                                          .safeBlockHorizontal *
                                                                      100,
                                                                  height: SizeConfig
                                                                          .safeBlockVertical *
                                                                      5.5,
                                                                  child: Row(
                                                                    children: [
                                                                      Container(
                                                                        width:
                                                                            SizeConfig.safeBlockHorizontal *
                                                                                3,
                                                                        height: SizeConfig.safeBlockVertical *
                                                                            100,
                                                                      ),
                                                                      Container(
                                                                        width:
                                                                            SizeConfig.safeBlockHorizontal *
                                                                                5,
                                                                        height: SizeConfig.safeBlockVertical *
                                                                            100,
                                                                        child:
                                                                            CircularProgressIndicator(),
                                                                      ),
                                                                      Container(
                                                                        width: SizeConfig.safeBlockHorizontal *
                                                                            65,
                                                                        height: SizeConfig.safeBlockVertical *
                                                                            100,
                                                                      ),
                                                                    ],
                                                                  ),
                                                                );
                                                        }),
                                                  ),
                                                ),
                                                Container(
                                                  width: SizeConfig
                                                          .safeBlockHorizontal *
                                                      100,
                                                  height: SizeConfig
                                                          .safeBlockVertical *
                                                      3.5,
                                                  child: Padding(
                                                    padding: EdgeInsets.only(
                                                        top: SizeConfig
                                                                .safeBlockVertical *
                                                            0.5),
                                                    child: Text(
                                                      "Que tal tu dia hoy ?",
                                                      style: TextStyle(
                                                        color: Colors.black38,
                                                        fontWeight:
                                                            FontWeight.bold,
                                                        fontFamily: 'OpenSans',
                                                        fontSize: SizeConfig
                                                                .safeBlockHorizontal *
                                                            4,
                                                      ),
                                                    ),
                                                  ),
                                                ),
                                              ],
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),
                                    Container(
                                      width:
                                          SizeConfig.safeBlockHorizontal * 100,
                                      height: SizeConfig.safeBlockVertical * 2,
                                    ),
                                  ],
                                ),
                              ),
                            ),
                            Container(
                              width: SizeConfig.safeBlockHorizontal * 100,
                              height: SizeConfig.safeBlockVertical * 1,
                            ),
                            Center(
                              child: Container(
                                width: SizeConfig.safeBlockHorizontal * 94,
                                height: SizeConfig.safeBlockVertical * 72.5,
                                child: Row(
                                  children: [
                                    Container(
                                      width:
                                          SizeConfig.safeBlockHorizontal * 1.5,
                                      height:
                                          SizeConfig.safeBlockVertical * 100,
                                    ),
                                    Container(
                                      width:
                                          SizeConfig.safeBlockHorizontal * 91,
                                      height:
                                          SizeConfig.safeBlockVertical * 100,
                                      decoration: BoxDecoration(
                                          color: Colors.white,
                                          borderRadius: BorderRadius.circular(
                                              SizeConfig.safeBlockHorizontal *
                                                  4),
                                          border: Border.all(
                                              width: 1, color: Colors.grey)),
                                      child: Column(
                                        children: [
                                          Container(
                                            width:
                                                SizeConfig.safeBlockHorizontal *
                                                    100,
                                            height:
                                                SizeConfig.safeBlockVertical *
                                                    11,
                                            child: Padding(
                                              padding: EdgeInsets.only(
                                                  top: SizeConfig
                                                          .safeBlockVertical *
                                                      5,
                                                  left: SizeConfig
                                                          .safeBlockHorizontal *
                                                      8),
                                              child: Text(
                                                "Servicios",
                                                style: TextStyle(
                                                  color: Colors.black26,
                                                  fontWeight: FontWeight.bold,
                                                  fontFamily: 'OpenSans',
                                                  fontSize: SizeConfig
                                                          .safeBlockHorizontal *
                                                      5,
                                                ),
                                              ),
                                            ),
                                          ),
                                          Container(
                                            width:
                                                SizeConfig.safeBlockHorizontal *
                                                    100,
                                            height:
                                                SizeConfig.safeBlockVertical *
                                                    10,
                                            child: Row(
                                              children: [
                                                Container(
                                                  width: SizeConfig
                                                          .safeBlockHorizontal *
                                                      14,
                                                  height: SizeConfig
                                                          .safeBlockVertical *
                                                      100,
                                                ),
                                                Container(
                                                  width: SizeConfig
                                                          .safeBlockHorizontal *
                                                      74,
                                                  height: SizeConfig
                                                          .safeBlockVertical *
                                                      100,
                                                  child: Column(
                                                    children: [
                                                      Container(
                                                        width: SizeConfig
                                                                .safeBlockHorizontal *
                                                            100,
                                                        height: SizeConfig
                                                                .safeBlockVertical *
                                                            2,
                                                      ),
                                                      Container(
                                                        width: SizeConfig
                                                                .safeBlockHorizontal *
                                                            100,
                                                        height: SizeConfig
                                                                .safeBlockVertical *
                                                            6,
                                                        child: Row(
                                                          children: [
                                                            Container(
                                                              width: SizeConfig
                                                                      .safeBlockHorizontal *
                                                                  58,
                                                              height: SizeConfig
                                                                      .safeBlockVertical *
                                                                  100,
                                                              child:
                                                                  _buildFechaTF(),
                                                            ),
                                                            Container(
                                                              width: SizeConfig
                                                                      .safeBlockHorizontal *
                                                                  2,
                                                              height: SizeConfig
                                                                      .safeBlockVertical *
                                                                  100,
                                                            ),
                                                            Container(
                                                              width: SizeConfig
                                                                      .safeBlockHorizontal *
                                                                  14,
                                                              height: SizeConfig
                                                                      .safeBlockVertical *
                                                                  100,
                                                              child:
                                                                  buildBuscarBtn(),
                                                            ),
                                                          ],
                                                        ),
                                                      ),
                                                      Container(
                                                        width: SizeConfig
                                                                .safeBlockHorizontal *
                                                            100,
                                                        height: SizeConfig
                                                                .safeBlockVertical *
                                                            2,
                                                      ),
                                                    ],
                                                  ),
                                                ),
                                                Container(
                                                  width: SizeConfig
                                                          .safeBlockHorizontal *
                                                      2,
                                                  height: SizeConfig
                                                          .safeBlockVertical *
                                                      100,
                                                ),
                                              ],
                                            ),
                                          ),
                                          Container(
                                            width:
                                                SizeConfig.safeBlockHorizontal *
                                                    100,
                                            height:
                                                SizeConfig.safeBlockVertical *
                                                    2,
                                            child: Row(
                                              children: [
                                                Container(
                                                  width: SizeConfig
                                                          .safeBlockHorizontal *
                                                      3.5,
                                                  height: SizeConfig
                                                          .safeBlockVertical *
                                                      100,
                                                ),
                                                Container(
                                                  width: SizeConfig
                                                          .safeBlockHorizontal *
                                                      83,
                                                  height: SizeConfig
                                                          .safeBlockVertical *
                                                      100,
                                                  child: Column(
                                                    children: [
                                                      Container(
                                                        width: SizeConfig
                                                                .safeBlockHorizontal *
                                                            100,
                                                        height: SizeConfig
                                                                .safeBlockVertical *
                                                            1.8,
                                                      ),
                                                      Container(
                                                        width: SizeConfig
                                                                .safeBlockHorizontal *
                                                            100,
                                                        height: SizeConfig
                                                                .safeBlockVertical *
                                                            0.2,
                                                        color: Colors.grey[200],
                                                      ),
                                                    ],
                                                  ),
                                                ),
                                                Container(
                                                  width: SizeConfig
                                                          .safeBlockHorizontal *
                                                      3.5,
                                                  height: SizeConfig
                                                          .safeBlockVertical *
                                                      100,
                                                ),
                                              ],
                                            ),
                                          ),
                                          Container(
                                            width:
                                                SizeConfig.safeBlockHorizontal *
                                                    100,
                                            height:
                                                SizeConfig.safeBlockVertical *
                                                    8,
                                            child: Row(
                                              children: [
                                                Container(
                                                  width: SizeConfig
                                                          .safeBlockHorizontal *
                                                      45,
                                                  height: SizeConfig
                                                          .safeBlockVertical *
                                                      100,
                                                ),
                                                Container(
                                                  width: SizeConfig
                                                          .safeBlockHorizontal *
                                                      45,
                                                  height: SizeConfig
                                                          .safeBlockVertical *
                                                      100,
                                                  child: Column(
                                                    children: [
                                                      Container(
                                                        width: SizeConfig
                                                                .safeBlockHorizontal *
                                                            100,
                                                        height: SizeConfig
                                                                .safeBlockVertical *
                                                            2.5,
                                                      ),
                                                      Container(
                                                        width: SizeConfig
                                                                .safeBlockHorizontal *
                                                            100,
                                                        height: SizeConfig
                                                                .safeBlockVertical *
                                                            5.5,
                                                        child: Row(
                                                          children: [
                                                            Container(
                                                              width: SizeConfig
                                                                      .safeBlockHorizontal *
                                                                  20,
                                                              height: SizeConfig
                                                                      .safeBlockVertical *
                                                                  100,
                                                            ),
                                                            Container(
                                                              width: SizeConfig
                                                                      .safeBlockHorizontal *
                                                                  13,
                                                              height: SizeConfig
                                                                      .safeBlockVertical *
                                                                  100,
                                                              child:
                                                                  buildActualizarBtn(),
                                                            ),
                                                            Container(
                                                              width: SizeConfig
                                                                      .safeBlockHorizontal *
                                                                  7,
                                                              height: SizeConfig
                                                                      .safeBlockVertical *
                                                                  100,
                                                            ),
                                                          ],
                                                        ),
                                                      ),
                                                    ],
                                                  ),
                                                ),
                                              ],
                                            ),
                                          ),
                                          Container(
                                            width:
                                                SizeConfig.safeBlockHorizontal *
                                                    100,
                                            height:
                                                SizeConfig.safeBlockVertical *
                                                    40.5,
                                            child: Stack(
                                              children: [
                                                if (serviciosUsuario ==
                                                    null) ...[
                                                  Center(
                                                      child:
                                                          CircularProgressIndicator(
                                                    backgroundColor:
                                                        Colors.black12,
                                                  ))
                                                ] else ...[
                                                  ServicioUsuario()
                                                ]
                                              ],
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),
                                    Container(
                                      width:
                                          SizeConfig.safeBlockHorizontal * 1.5,
                                      height:
                                          SizeConfig.safeBlockVertical * 100,
                                    ),
                                  ],
                                ),
                              ),
                            ),
                            Container(
                              width: SizeConfig.safeBlockHorizontal * 100,
                              height: SizeConfig.safeBlockVertical * 1,
                            ),
                            Center(
                              child: Container(
                                width: SizeConfig.safeBlockHorizontal * 98,
                                height: SizeConfig.safeBlockVertical * 10,
                                decoration: BoxDecoration(
                                  color: Colors.blue[200],
                                  borderRadius: BorderRadius.circular(
                                      SizeConfig.safeBlockHorizontal * 4),
                                ),
                                child: Column(
                                  children: [
                                    Container(
                                      width:
                                          SizeConfig.safeBlockHorizontal * 100,
                                      height:
                                          SizeConfig.safeBlockVertical * 1.5,
                                    ),
                                    Container(
                                      width:
                                          SizeConfig.safeBlockHorizontal * 100,
                                      height: SizeConfig.safeBlockVertical * 7,
                                      child: Row(
                                        children: [
                                          Container(
                                            width:
                                                SizeConfig.safeBlockHorizontal *
                                                    11,
                                            height:
                                                SizeConfig.safeBlockVertical *
                                                    100,
                                          ),
                                          Container(
                                            width:
                                                SizeConfig.safeBlockHorizontal *
                                                    16,
                                            height:
                                                SizeConfig.safeBlockVertical *
                                                    100,
                                            child: buildInicioBtn(),
                                          ),
                                          Container(
                                            width:
                                                SizeConfig.safeBlockHorizontal *
                                                    4,
                                            height:
                                                SizeConfig.safeBlockVertical *
                                                    100,
                                          ),
                                          Container(
                                            width:
                                                SizeConfig.safeBlockHorizontal *
                                                    16,
                                            height:
                                                SizeConfig.safeBlockVertical *
                                                    100,
                                            child: buildPerfilBtn(),
                                          ),
                                          Container(
                                            width:
                                                SizeConfig.safeBlockHorizontal *
                                                    4,
                                            height:
                                                SizeConfig.safeBlockVertical *
                                                    100,
                                          ),
                                          Container(
                                            width:
                                                SizeConfig.safeBlockHorizontal *
                                                    16,
                                            height:
                                                SizeConfig.safeBlockVertical *
                                                    100,
                                            child: buildInformationBtn(),
                                          ),
                                          Container(
                                            width:
                                                SizeConfig.safeBlockHorizontal *
                                                    4,
                                            height:
                                                SizeConfig.safeBlockVertical *
                                                    100,
                                          ),
                                          Container(
                                            width:
                                                SizeConfig.safeBlockHorizontal *
                                                    16,
                                            height:
                                                SizeConfig.safeBlockVertical *
                                                    100,
                                            child: buildMapBtn(),
                                          ),
                                          Container(
                                            width:
                                                SizeConfig.safeBlockHorizontal *
                                                    11,
                                            height:
                                                SizeConfig.safeBlockVertical *
                                                    100,
                                          ),
                                        ],
                                      ),
                                    ),
                                    Container(
                                      width:
                                          SizeConfig.safeBlockHorizontal * 100,
                                      height:
                                          SizeConfig.safeBlockVertical * 1.5,
                                    ),
                                  ],
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
        ));
  }
}

extension StringExtension on String {
  String capitalize2() {
    return "${this[0].toUpperCase()}${this.substring(1)}";
  }
}

class getByIdUsuario extends StatelessWidget {
  final list;
  getByIdUsuario({this.list});

  @override
  Widget build(BuildContext context) {
    var pasarString = list['nombre'].toString();
    var pasarArray = pasarString.split(" ");
    var contenedorNombre = pasarArray[0];
    var convertir = contenedorNombre.toLowerCase();
    var mayuscula = convertir.capitalize2();
    return Text(
      mayuscula,
      style: TextStyle(
        color: Colors.black26,
        fontWeight: FontWeight.bold,
        fontFamily: 'OpenSans',
        fontSize: SizeConfig.safeBlockHorizontal * 5,
      ),
    );
  }
}
