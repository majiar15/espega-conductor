import 'dart:convert';
import 'dart:ffi';
import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:http/http.dart' as http;
import 'package:conductor/helpers/AppData.dart';
import 'package:conductor/pages/usuario/serviciosUsuario.dart';
import '../../SizeConfig.dart';
import '../componentes.dart';
import '../header.dart';
import 'VerificarHoraServicio.dart';

class Perfil extends StatefulWidget {
  @override
  _PerfilState createState() => _PerfilState();
}

class _PerfilState extends State<Perfil> {

  Future<void> showMyDialogSignOut() async {
    return showDialog<void>(
      context: context,
      barrierDismissible: false, // user must tap button!
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text(
            'Cuenta',
            style: TextStyle(
              color: Colors.grey[600],
              fontFamily: 'OpenSans',
            ),
          ),
          content: SingleChildScrollView(
            child: ListBody(
              children: <Widget>[
                Text(
                  'Quieres cerrar sesion ?',
                  style: TextStyle(
                    color: Colors.grey,
                    fontFamily: 'OpenSans',
                  ),
                ),
              ],
            ),
          ),
          actions: <Widget>[
            TextButton(
              child: Text(
                'Si',
                style: TextStyle(
                  color: Colors.grey[600],
                  fontFamily: 'OpenSans',
                ),
              ),
              onPressed: () {

                stopActualizarServicio();
                stopVerificarHoraServicio();
                appData.timer?.cancel();

                Navigator.of(context).pushNamedAndRemoveUntil(
                    "/login", (Route<dynamic> route) => false);
              },
            ),
            TextButton(
              child: Text(
                'No',
                style: TextStyle(
                  color: Colors.grey[600],
                  fontFamily: 'OpenSans',
                ),
              ),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }

  Future obtenerPerfil() async {
    Map parametros = ModalRoute.of(context).settings.arguments;
    final url = "http://transporte-paciente.herokuapp.com/api/conductor/login";
    var data = {
      "username": parametros['username'],
      "password": parametros['password']
    };
    final response = await http.post(url, body: data);
    var cont = jsonDecode(response.body);
    var arrays = {
      "nombre" : cont['nombre'].toString(),
      "documento" : cont['documento'].toString(),
      "correo" : cont['correo'].toString(),
      "movil" : cont['movil'].toString(),
      "direcion" : cont['direccion'].toString(),
    };
    return arrays;
  }

  Future obtenerUsuario() async {
    Map parametros = ModalRoute.of(context).settings.arguments;
    final url = "http://transporte-paciente.herokuapp.com/api/conductor/login";
    var data = {
      "username": parametros['username'],
      "password": parametros['password'],
    };
    final response = await http.post(url, body: data);
    return jsonDecode(response.body);
  }

  Widget buildSocialBtn() {
    return GestureDetector(
      onTap: () {
      },
      child: Container(
        margin: EdgeInsets.only(left: SizeConfig.safeBlockHorizontal * 2.0),
        width: 20,
        height: 20,
        decoration: BoxDecoration(
          shape: BoxShape.circle,
          image: DecorationImage(
              image: AssetImage("assets/perfil2.jpg"), fit: BoxFit.cover),
        ),
      ),
    );
  }

  Widget buildperfil() {
    return GestureDetector(
      onTap: () {
      },
      child: Container(
        margin: EdgeInsets.only(left: SizeConfig.safeBlockHorizontal * 2.0),
        width: 110,
        height: 110,
        decoration: BoxDecoration(
          shape: BoxShape.circle,
          image: DecorationImage(
              image: AssetImage("assets/logos/perfil_usuario1.jpg"), fit: BoxFit.cover),
        ),
      ),
    );
  }

  Widget buildBuscarBtn() {
    return GestureDetector(
      onTap: () {},
      child: Container(
        margin: EdgeInsets.only(right: SizeConfig.safeBlockHorizontal * 2),
        width: 20,
        height: 20,
        decoration: BoxDecoration(
          boxShadow: [
            BoxShadow(
              color: Colors.black26,
              blurRadius: 6.0,
              offset: Offset(0, 2),
            ),
          ],
          shape: BoxShape.circle,
          image: DecorationImage(
              image: AssetImage("assets/buscar.jpg"), fit: BoxFit.cover),
        ),
      ),
    );
  }

  Widget buildInicioBtn() {
    Map parametros = ModalRoute.of(context).settings.arguments;
    return GestureDetector(
      onTap: () {

        stopVerificarHoraServicio();
        appData.timer?.cancel();

        Navigator.pushNamed(context, "/inicio", arguments: {
          'id': parametros['id'],
          'nombre': parametros['nombre'],
          'documento': parametros['documento'],
          'username': parametros['username'],
          'password': parametros['password'],
        });
      },
      child: Container(
        margin: EdgeInsets.only(left: SizeConfig.safeBlockHorizontal * 1.0),
        width: 20,
        height: 20,
        decoration: BoxDecoration(
          shape: BoxShape.circle,
          image: DecorationImage(
              image: AssetImage("assets/inicio2.jpg"), fit: BoxFit.cover),
        ),
      ),
    );
  }

  Widget buildPerfilBtn() {
    Map parametros = ModalRoute.of(context).settings.arguments;
    return GestureDetector(
      onTap: () {
        Navigator.pushNamed(context, "/inicio/perfil", arguments: {
          'id': parametros['id'],
          'nombre': parametros['nombre'],
          'documento': parametros['documento'],
          'username': parametros['username'],
          'password': parametros['password'],
        });
      },
      child: Container(
        margin: EdgeInsets.only(left: SizeConfig.safeBlockHorizontal * 1.0),
        width: 20,
        height: 20,
        decoration: BoxDecoration(
          shape: BoxShape.circle,
          image: DecorationImage(
              image: AssetImage("assets/perfil_inicio.jpg"), fit: BoxFit.cover),
        ),
      ),
    );
  }

  Widget buildMapBtn() {
    return GestureDetector(
      onTap: () {
        showMyDialogSignOut();
      },
      child: Container(
        margin: EdgeInsets.only(left: SizeConfig.safeBlockHorizontal * 1.0),
        width: 20,
        height: 20,
        decoration: BoxDecoration(
          shape: BoxShape.circle,
          image: DecorationImage(
              image: AssetImage("assets/cerrar_inicio.jpg"), fit: BoxFit.cover),
        ),
      ),
    );
  }

  Widget buildInformationBtn() {
    Map parametros = ModalRoute.of(context).settings.arguments;
    return GestureDetector(
      onTap: () {
        Navigator.pushNamed(context, "/inicio/informacion", arguments: {
          'id': parametros['id'],
          'nombre': parametros['nombre'],
          'documento': parametros['documento'],
          'username': parametros['username'],
          'password': parametros['password'],
        });
      },
      child: Container(
        margin: EdgeInsets.only(left: SizeConfig.safeBlockHorizontal * 1.0),
        width: 20,
        height: 20,
        decoration: BoxDecoration(
          shape: BoxShape.circle,
          image: DecorationImage(
              image: AssetImage("assets/informacion.jpg"), fit: BoxFit.cover),
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    Map parametros = ModalRoute.of(context).settings.arguments;
    SizeConfig().init(context);
    return new WillPopScope(
        onWillPop: () async => false,
        child: Scaffold(
          body: AnnotatedRegion<SystemUiOverlayStyle>(
            value: SystemUiOverlayStyle.light,
            child: GestureDetector(
              onTap: () => FocusScope.of(context).unfocus(),
              child: Stack(
                children: <Widget>[
                  Container(
                    height: double.infinity,
                    width: double.infinity,
                    color: Colors.white,
                  ),
                  SingleChildScrollView(
                    child: Center(
                      child: Container(
                        width: SizeConfig.safeBlockHorizontal * 100,
                        height: SizeConfig.blockSizeVertical * 100,
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: <Widget>[
                            Container(
                              width: SizeConfig.safeBlockHorizontal * 100,
                              height: SizeConfig.safeBlockVertical * 5,
                            ),
                            Center(
                              child: Container(
                                width: SizeConfig.safeBlockHorizontal * 96,
                                height: SizeConfig.safeBlockVertical * 14,
                                decoration: BoxDecoration(
                                  color: Colors.white,
                                  borderRadius: BorderRadius.circular(SizeConfig.safeBlockHorizontal * 4),
                                    border: Border.all(
                                        width: 1,
                                        color: Colors.grey
                                    )
                                ),
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.end,
                                  children: [
                                    Container(
                                      width: SizeConfig.safeBlockHorizontal * 100,
                                      height: SizeConfig.safeBlockVertical * 2,
                                    ),
                                    Container(
                                      width: SizeConfig.safeBlockHorizontal * 100,
                                      height: SizeConfig.safeBlockVertical * 9,
                                      child: Row(
                                        children: [
                                          Container(
                                            width: SizeConfig.safeBlockHorizontal * 5,
                                            height: SizeConfig.safeBlockVertical * 100,
                                          ),
                                          Container(
                                            width: SizeConfig.safeBlockHorizontal * 15,
                                            height: SizeConfig.safeBlockVertical * 100,
                                            child: buildSocialBtn(),
                                          ),
                                          Container(
                                            width: SizeConfig.safeBlockHorizontal * 2,
                                            height: SizeConfig.safeBlockVertical * 100,
                                          ),
                                          Container(
                                            width: SizeConfig.safeBlockHorizontal * 73,
                                            height: SizeConfig.safeBlockVertical * 100,
                                            child: Column(
                                              children: [
                                                Container(
                                                  width: SizeConfig.safeBlockHorizontal * 100,
                                                  height: SizeConfig.safeBlockVertical * 5.5,
                                                  child: Padding(
                                                    padding: EdgeInsets.only(top: SizeConfig.safeBlockVertical * 2),
                                                    child: FutureBuilder(
                                                        future: obtenerUsuario(),
                                                        builder: (context, snapshot) {
                                                          if (snapshot.hasError) print(snapshot.error);
                                                          return snapshot.hasData
                                                              ? new getByIdUsuario(list: snapshot.data)
                                                              : new Container(
                                                            width: SizeConfig.safeBlockHorizontal * 100,
                                                            height: SizeConfig.safeBlockVertical * 5.5,
                                                            child: Row(
                                                              children: [
                                                                Container(
                                                                  width: SizeConfig.safeBlockHorizontal * 3,
                                                                  height: SizeConfig.safeBlockVertical * 100,
                                                                ),
                                                                Container(
                                                                  width: SizeConfig.safeBlockHorizontal * 5,
                                                                  height: SizeConfig.safeBlockVertical * 100,
                                                                  child: CircularProgressIndicator(),
                                                                ),
                                                                Container(
                                                                  width: SizeConfig.safeBlockHorizontal * 65,
                                                                  height: SizeConfig.safeBlockVertical * 100,
                                                                ),
                                                              ],
                                                            ),
                                                          );
                                                        }),
                                                  ),
                                                ),
                                                Container(
                                                  width: SizeConfig.safeBlockHorizontal * 100,
                                                  height: SizeConfig.safeBlockVertical * 3.5,
                                                  child: Padding(
                                                    padding: EdgeInsets.only(top: SizeConfig.safeBlockVertical * 0.5),
                                                    child: Text(
                                                      "Que tal tu dia hoy ?",
                                                      style: TextStyle(
                                                        color: Colors.black38,
                                                        fontWeight: FontWeight.bold,
                                                        fontFamily: 'OpenSans',
                                                        fontSize: SizeConfig.safeBlockHorizontal * 4,
                                                      ),
                                                    ),
                                                  ),
                                                ),
                                              ],
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),
                                    Container(
                                      width: SizeConfig.safeBlockHorizontal * 100,
                                      height: SizeConfig.safeBlockVertical * 2,
                                    ),
                                  ],
                                ),
                              ),
                            ),
                            Container(
                              width: SizeConfig.safeBlockHorizontal * 100,
                              height: SizeConfig.safeBlockVertical * 1,
                            ),
                            Center(
                              child: Container(
                                width: SizeConfig.safeBlockHorizontal * 94,
                                height: SizeConfig.safeBlockVertical * 72.5,
                                child: Row(
                                  children: [
                                    Container(
                                      width: SizeConfig.safeBlockHorizontal * 1.5,
                                      height: SizeConfig.safeBlockVertical * 100,
                                    ),
                                    Container(
                                      width: SizeConfig.safeBlockHorizontal * 91,
                                      height: SizeConfig.safeBlockVertical * 100,
                                      decoration: BoxDecoration(
                                        color: Colors.white,
                                        borderRadius: BorderRadius.circular(SizeConfig.safeBlockHorizontal * 4),
                                          border: Border.all(
                                              width: 1,
                                              color: Colors.grey
                                          )
                                      ),
                                      child: Column(
                                        children: [
                                          Container(
                                              width: SizeConfig.safeBlockHorizontal * 100,
                                              height: SizeConfig.safeBlockVertical * 28,
                                              child: Row(
                                                children: [
                                                  Container(
                                                    width: SizeConfig.safeBlockHorizontal * 4.5,
                                                    height: SizeConfig.safeBlockVertical * 100,
                                                  ),
                                                  Container(
                                                    width: SizeConfig.safeBlockHorizontal * 81,
                                                    height: SizeConfig.safeBlockVertical * 100,
                                                    child: Column(
                                                      children: [
                                                        Container(
                                                          width: SizeConfig.safeBlockHorizontal * 100,
                                                          height: SizeConfig.safeBlockVertical * 1.5,
                                                        ),
                                                        Container(
                                                          width: SizeConfig.safeBlockHorizontal * 100,
                                                          height: SizeConfig.safeBlockVertical * 25,
                                                          decoration: BoxDecoration(
                                                            color: Colors.grey[100],
                                                            borderRadius: BorderRadius.circular(SizeConfig.safeBlockHorizontal * 4),
                                                          ),
                                                          child: Center(
                                                            child: buildperfil(),
                                                          ),
                                                        ),
                                                        Container(
                                                          width: SizeConfig.safeBlockHorizontal * 100,
                                                          height: SizeConfig.safeBlockVertical * 1.5,
                                                        ),
                                                      ],
                                                    ),
                                                  ),
                                                  Container(
                                                    width: SizeConfig.safeBlockHorizontal * 4.5,
                                                    height: SizeConfig.safeBlockVertical * 100,
                                                  ),
                                                ],
                                              )
                                          ),
                                          Container(
                                            width: SizeConfig.safeBlockHorizontal * 100,
                                            height: SizeConfig.safeBlockVertical * 1,
                                            child: Row(
                                              children: [
                                                Container(
                                                  width: SizeConfig.safeBlockHorizontal * 3.5,
                                                  height: SizeConfig.safeBlockVertical * 100,
                                                ),
                                                Container(
                                                  width: SizeConfig.safeBlockHorizontal * 83,
                                                  height: SizeConfig.safeBlockVertical * 100,
                                                  child: Column(
                                                    children: [
                                                      Container(
                                                        width: SizeConfig.safeBlockHorizontal * 100,
                                                        height: SizeConfig.safeBlockVertical * 0.8,
                                                      ),
                                                      Container(
                                                        width: SizeConfig.safeBlockHorizontal * 100,
                                                        height: SizeConfig.safeBlockVertical * 0.2,
                                                        color: Colors.grey[200],
                                                      ),
                                                    ],
                                                  ),
                                                ),
                                                Container(
                                                  width: SizeConfig.safeBlockHorizontal * 3.5,
                                                  height: SizeConfig.safeBlockVertical * 100,
                                                ),
                                              ],
                                            ),
                                          ),
                                          Container(
                                            width: SizeConfig.safeBlockHorizontal * 100,
                                            height: SizeConfig.safeBlockVertical * 42.5,
                                            child: FutureBuilder(
                                                future: obtenerPerfil(),
                                                builder: (context, snapshot) {
                                                  if (snapshot.hasError) print(snapshot.error);
                                                  return snapshot.hasData
                                                      ? new getByIdPerfilUnico(list: snapshot.data)
                                                      : new Center(
                                                      child: new CircularProgressIndicator.adaptive());
                                                }),
                                          ),
                                        ],
                                      ),
                                    ),
                                    Container(
                                      width: SizeConfig.safeBlockHorizontal * 1.5,
                                      height: SizeConfig.safeBlockVertical * 100,
                                    ),
                                  ],
                                ),
                              ),
                            ),
                            Container(
                              width: SizeConfig.safeBlockHorizontal * 100,
                              height: SizeConfig.safeBlockVertical * 1,
                            ),
                            Center(
                              child: Container(
                                width: SizeConfig.safeBlockHorizontal * 98,
                                height: SizeConfig.safeBlockVertical * 10,
                                decoration: BoxDecoration(
                                  color: Colors.blue[200],
                                  borderRadius: BorderRadius.circular(SizeConfig.safeBlockHorizontal * 4),
                                ),
                                child: Column(
                                  children: [
                                    Container(
                                      width: SizeConfig.safeBlockHorizontal * 100,
                                      height: SizeConfig.safeBlockVertical * 1.5,
                                    ),
                                    Container(
                                      width: SizeConfig.safeBlockHorizontal * 100,
                                      height: SizeConfig.safeBlockVertical * 7,
                                      child: Row(
                                        children: [
                                          Container(
                                            width: SizeConfig.safeBlockHorizontal * 11,
                                            height: SizeConfig.safeBlockVertical * 100,
                                          ),
                                          Container(
                                            width: SizeConfig.safeBlockHorizontal * 16,
                                            height: SizeConfig.safeBlockVertical * 100,
                                            child: buildInicioBtn(),
                                          ),
                                          Container(
                                            width: SizeConfig.safeBlockHorizontal * 4,
                                            height: SizeConfig.safeBlockVertical * 100,
                                          ),
                                          Container(
                                            width: SizeConfig.safeBlockHorizontal * 16,
                                            height: SizeConfig.safeBlockVertical * 100,
                                            child: buildPerfilBtn(),
                                          ),
                                          Container(
                                            width: SizeConfig.safeBlockHorizontal * 4,
                                            height: SizeConfig.safeBlockVertical * 100,
                                          ),
                                          Container(
                                            width: SizeConfig.safeBlockHorizontal * 16,
                                            height: SizeConfig.safeBlockVertical * 100,
                                            child: buildInformationBtn(),
                                          ),
                                          Container(
                                            width: SizeConfig.safeBlockHorizontal * 4,
                                            height: SizeConfig.safeBlockVertical * 100,
                                          ),
                                          Container(
                                            width: SizeConfig.safeBlockHorizontal * 16,
                                            height: SizeConfig.safeBlockVertical * 100,
                                            child: buildMapBtn(),
                                          ),
                                          Container(
                                            width: SizeConfig.safeBlockHorizontal * 11,
                                            height: SizeConfig.safeBlockVertical * 100,
                                          ),
                                        ],
                                      ),
                                    ),
                                    Container(
                                      width: SizeConfig.safeBlockHorizontal * 100,
                                      height: SizeConfig.safeBlockVertical * 1.5,
                                    ),
                                  ],
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
        ));
  }


}

extension StringExtension on String {
  String capitalize2() {
    return "${this[0].toUpperCase()}${this.substring(1)}";
  }
}

class getByIdPerfilUnico extends StatefulWidget {

  final list;
  getByIdPerfilUnico({this.list});
  @override
  _getByIdPerfilUnicoState createState() => _getByIdPerfilUnicoState();
}

class _getByIdPerfilUnicoState extends State<getByIdPerfilUnico> {

  String nombre;

  void cambiar() {
    String string = widget.list['nombre'];
    var cambiar =  string.toLowerCase();
    String uper = cambiar.capitalize2();
    nombre = uper;
  }

  @override
  Widget build(BuildContext context) {
    cambiar();
    return Container(
      width: SizeConfig.safeBlockHorizontal * 100,
      height: SizeConfig.safeBlockVertical * 42.5,
      child: Column(
        children: [
          Container(
            width: SizeConfig.safeBlockHorizontal * 100,
            height: SizeConfig.safeBlockVertical * 15,
            child: Column(
              children: [
                Container(
                  width: SizeConfig.safeBlockHorizontal * 100,
                  height: SizeConfig.safeBlockVertical * 5,
                ),
                Container(
                  width: SizeConfig.safeBlockHorizontal * 100,
                  height: SizeConfig.safeBlockVertical * 10,
                  child: Column(
                    children: [
                      Container(
                        width: SizeConfig.safeBlockHorizontal * 100,
                        height: SizeConfig.safeBlockVertical * 4,
                        child: Row(
                          children: [
                            Container(
                              width: SizeConfig.safeBlockHorizontal * 54,
                              height: SizeConfig.safeBlockVertical * 100,
                              child: Row(
                                children: [
                                  Container(
                                    width: SizeConfig.safeBlockHorizontal * 10,
                                    height: SizeConfig.safeBlockVertical * 100,
                                  ),
                                  Container(
                                    width: SizeConfig.safeBlockHorizontal * 44,
                                    height: SizeConfig.safeBlockVertical * 100,
                                    child: Padding(
                                      padding: EdgeInsets.only(top: SizeConfig.safeBlockVertical * 0.5),
                                      child: Text(
                                        "Nombre",
                                        style: TextStyle(
                                          color: Colors.black38,
                                          fontWeight: FontWeight.bold,
                                          fontFamily: 'OpenSans',
                                          fontSize: SizeConfig.safeBlockHorizontal * 4,
                                        ),
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                      ),
                      Container(
                        width: SizeConfig.safeBlockHorizontal * 100,
                        height: SizeConfig.safeBlockVertical * 6,
                        child: Row(
                          children: [
                            Container(
                              width: SizeConfig.safeBlockHorizontal * 54,
                              height: SizeConfig.safeBlockVertical * 100,
                              child: Row(
                                children: [
                                  Container(
                                    width: SizeConfig.safeBlockHorizontal * 10,
                                    height: SizeConfig.safeBlockVertical * 100,
                                  ),
                                  Container(
                                    width: SizeConfig.safeBlockHorizontal * 44,
                                    height: SizeConfig.safeBlockVertical * 100,
                                    child: Padding(
                                      padding: EdgeInsets.only(top: SizeConfig.safeBlockVertical * 0.5),
                                      child: Text(
                                        nombre,
                                        style: TextStyle(
                                          color: Colors.black26,
                                          fontWeight: FontWeight.bold,
                                          fontFamily: 'OpenSans',
                                          fontSize: SizeConfig.safeBlockHorizontal * 4,
                                        ),
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
          Container(
            width: SizeConfig.safeBlockHorizontal * 100,
            height: SizeConfig.safeBlockVertical * 12,
            child: Column(
              children: [
                Container(
                  width: SizeConfig.safeBlockHorizontal * 100,
                  height: SizeConfig.safeBlockVertical * 10,
                  child: Column(
                    children: [
                      Container(
                        width: SizeConfig.safeBlockHorizontal * 100,
                        height: SizeConfig.safeBlockVertical * 3,
                        child: Row(
                          children: [
                            Container(
                              width: SizeConfig.safeBlockHorizontal * 54,
                              height: SizeConfig.safeBlockVertical * 100,
                              child: Row(
                                children: [
                                  Container(
                                    width: SizeConfig.safeBlockHorizontal * 10,
                                    height: SizeConfig.safeBlockVertical * 100,
                                  ),
                                  Container(
                                    width: SizeConfig.safeBlockHorizontal * 44,
                                    height: SizeConfig.safeBlockVertical * 100,
                                    child: Padding(
                                      padding: EdgeInsets.only(top: SizeConfig.safeBlockVertical * 0.5),
                                      child: Text(
                                        "Direcion",
                                        style: TextStyle(
                                          color: Colors.black38,
                                          fontWeight: FontWeight.bold,
                                          fontFamily: 'OpenSans',
                                          fontSize: SizeConfig.safeBlockHorizontal * 4,
                                        ),
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                            Container(
                                width: SizeConfig.safeBlockHorizontal * 36,
                                height: SizeConfig.safeBlockVertical * 100,
                                child: Row(
                                  children: [
                                    Container(
                                      width: SizeConfig.safeBlockHorizontal * 5,
                                      height: SizeConfig.safeBlockVertical * 100,
                                    ),
                                    Container(
                                      width: SizeConfig.safeBlockHorizontal * 31,
                                      height: SizeConfig.safeBlockVertical * 100,
                                      child: Padding(
                                        padding: EdgeInsets.only(top: SizeConfig.safeBlockVertical * 0.5),
                                        child: Text(
                                          "Documento",
                                          style: TextStyle(
                                            color: Colors.black38,
                                            fontWeight: FontWeight.bold,
                                            fontFamily: 'OpenSans',
                                            fontSize: SizeConfig.safeBlockHorizontal * 4,
                                          ),
                                        ),
                                      ),
                                    ),
                                  ],
                                )
                            ),
                          ],
                        ),
                      ),
                      Container(
                        width: SizeConfig.safeBlockHorizontal * 100,
                        height: SizeConfig.safeBlockVertical * 7,
                        child: Row(
                          children: [
                            Container(
                              width: SizeConfig.safeBlockHorizontal * 54,
                              height: SizeConfig.safeBlockVertical * 100,
                              child: Row(
                                children: [
                                  Container(
                                    width: SizeConfig.safeBlockHorizontal * 10,
                                    height: SizeConfig.safeBlockVertical * 100,
                                  ),
                                  Container(
                                    width: SizeConfig.safeBlockHorizontal * 44,
                                    height: SizeConfig.safeBlockVertical * 100,
                                    child: Padding(
                                      padding: EdgeInsets.only(top: SizeConfig.safeBlockVertical * 1),
                                      child: Text(
                                        widget.list['direcion'],
                                        style: TextStyle(
                                          color: Colors.black26,
                                          fontWeight: FontWeight.bold,
                                          fontFamily: 'OpenSans',
                                          fontSize: SizeConfig.safeBlockHorizontal * 4,
                                        ),
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                            Container(
                                width: SizeConfig.safeBlockHorizontal * 36,
                                height: SizeConfig.safeBlockVertical * 100,
                                child: Row(
                                  children: [
                                    Container(
                                      width: SizeConfig.safeBlockHorizontal * 5,
                                      height: SizeConfig.safeBlockVertical * 100,
                                    ),
                                    Container(
                                      width: SizeConfig.safeBlockHorizontal * 31,
                                      height: SizeConfig.safeBlockVertical * 100,
                                      child: Padding(
                                        padding: EdgeInsets.only(top: SizeConfig.safeBlockVertical * 1),
                                        child: Text(
                                          widget.list['documento'].toString(),
                                          style: TextStyle(
                                            color: Colors.black26,
                                            fontWeight: FontWeight.bold,
                                            fontFamily: 'OpenSans',
                                            fontSize: SizeConfig.safeBlockHorizontal * 4,
                                          ),
                                        ),
                                      ),
                                    ),
                                  ],
                                )
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
          Container(
            width: SizeConfig.safeBlockHorizontal * 100,
            height: SizeConfig.safeBlockVertical * 13,
            child: Column(
              children: [
                Container(
                  width: SizeConfig.safeBlockHorizontal * 100,
                  height: SizeConfig.safeBlockVertical * 10,
                  child: Column(
                    children: [
                      Container(
                        width: SizeConfig.safeBlockHorizontal * 100,
                        height: SizeConfig.safeBlockVertical * 4,
                        child: Row(
                          children: [
                            Container(
                              width: SizeConfig.safeBlockHorizontal * 54,
                              height: SizeConfig.safeBlockVertical * 100,
                              child: Row(
                                children: [
                                  Container(
                                    width: SizeConfig.safeBlockHorizontal * 10,
                                    height: SizeConfig.safeBlockVertical * 100,
                                  ),
                                  Container(
                                    width: SizeConfig.safeBlockHorizontal * 44,
                                    height: SizeConfig.safeBlockVertical * 100,
                                    child: Padding(
                                      padding: EdgeInsets.only(top: SizeConfig.safeBlockVertical * 0.5),
                                      child: Text(
                                        "Correo",
                                        style: TextStyle(
                                          color: Colors.black38,
                                          fontWeight: FontWeight.bold,
                                          fontFamily: 'OpenSans',
                                          fontSize: SizeConfig.safeBlockHorizontal * 4,
                                        ),
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                            Container(
                                width: SizeConfig.safeBlockHorizontal * 36,
                                height: SizeConfig.safeBlockVertical * 100,
                                child: Row(
                                  children: [
                                    Container(
                                      width: SizeConfig.safeBlockHorizontal * 5,
                                      height: SizeConfig.safeBlockVertical * 100,
                                    ),
                                    Container(
                                      width: SizeConfig.safeBlockHorizontal * 31,
                                      height: SizeConfig.safeBlockVertical * 100,
                                      child: Padding(
                                        padding: EdgeInsets.only(top: SizeConfig.safeBlockVertical * 0.5),
                                        child: Text(
                                          "Celular",
                                          style: TextStyle(
                                            color: Colors.black38,
                                            fontWeight: FontWeight.bold,
                                            fontFamily: 'OpenSans',
                                            fontSize: SizeConfig.safeBlockHorizontal * 4,
                                          ),
                                        ),
                                      ),
                                    ),
                                  ],
                                )
                            ),
                          ],
                        ),
                      ),
                      Container(
                        width: SizeConfig.safeBlockHorizontal * 100,
                        height: SizeConfig.safeBlockVertical * 6,
                        child: Row(
                          children: [
                            Container(
                              width: SizeConfig.safeBlockHorizontal * 54,
                              height: SizeConfig.safeBlockVertical * 100,
                              child: Row(
                                children: [
                                  Container(
                                    width: SizeConfig.safeBlockHorizontal * 10,
                                    height: SizeConfig.safeBlockVertical * 100,
                                  ),
                                  Container(
                                    width: SizeConfig.safeBlockHorizontal * 44,
                                    height: SizeConfig.safeBlockVertical * 100,
                                    child: Padding(
                                      padding: EdgeInsets.only(top: SizeConfig.safeBlockVertical * 0.5),
                                      child: Text(
                                        widget.list['correo'],
                                        style: TextStyle(
                                          color: Colors.black26,
                                          fontWeight: FontWeight.bold,
                                          fontFamily: 'OpenSans',
                                          fontSize: SizeConfig.safeBlockHorizontal * 4,
                                        ),
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                            Container(
                                width: SizeConfig.safeBlockHorizontal * 36,
                                height: SizeConfig.safeBlockVertical * 100,
                                child: Row(
                                  children: [
                                    Container(
                                      width: SizeConfig.safeBlockHorizontal * 5,
                                      height: SizeConfig.safeBlockVertical * 100,
                                    ),
                                    Container(
                                      width: SizeConfig.safeBlockHorizontal * 31,
                                      height: SizeConfig.safeBlockVertical * 100,
                                      child: Padding(
                                        padding: EdgeInsets.only(top: SizeConfig.safeBlockVertical * 0.5),
                                        child: Text(
                                          widget.list['movil'],
                                          style: TextStyle(
                                            color: Colors.black26,
                                            fontWeight: FontWeight.bold,
                                            fontFamily: 'OpenSans',
                                            fontSize: SizeConfig.safeBlockHorizontal * 4,
                                          ),
                                        ),
                                      ),
                                    ),
                                  ],
                                )
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
                Container(
                  width: SizeConfig.safeBlockHorizontal * 100,
                  height: SizeConfig.safeBlockVertical * 3,
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }

}


extension StringExtensio on String {
  String capitalize() {
    return "${this[0].toUpperCase()}${this.substring(1)}";
  }
}

class getByIdUsuario extends StatelessWidget {

  final list;
  getByIdUsuario({this.list});

  @override
  Widget build(BuildContext context) {
    var pasarString = list['nombre'].toString();
    var pasarArray = pasarString.split(" ");
    var contenedorNombre = pasarArray[0];
    var convertir = contenedorNombre.toLowerCase();
    var mayuscula = convertir.capitalize();
    return Text(
      mayuscula,
      style: TextStyle(
        color: Colors.black26,
        fontWeight: FontWeight.bold,
        fontFamily: 'OpenSans',
        fontSize: SizeConfig.safeBlockHorizontal * 5,
      ),
    );
  }
}