import 'dart:async';
import 'dart:convert';
import 'dart:isolate';
import 'package:http/http.dart' as http;

Isolate _isolate;
Timer _timer;
bool verificar = false;

Future<ReceivePort> ActualizarServicios(userId, username) async {
  ReceivePort receivePort = new ReceivePort();
  _isolate = await Isolate.spawn(
      obtenerTodo, [receivePort.sendPort, userId, username]);
  return receivePort;
}

stopActualizarServicio() {
  if (_isolate != null) {
    _isolate.kill(priority: Isolate.immediate);
  }
}

void ConsultarServicio(sendPort, userId, userName) async {
  var arrayServicio = [];

  final url = "http://transporte-paciente.herokuapp.com/api/generic/fecha/get";

  var data = {
    "username": userName,
  };

  final response = await http.post(url, body: data);
  var value = jsonDecode(response.body);

  String fechaRamdon = value["fecha_actual"].toString();
  var sacarFecha = fechaRamdon.split(" ");
  String fecha = sacarFecha[0];
  var arrayfecha = fecha.split("/");
  var fechaActual = arrayfecha[2] + "-" + arrayfecha[1] + "-" + arrayfecha[0];

  final url2 =
      "http://transporte-paciente.herokuapp.com/api/conductor/servicios/dia/list";

  var data2 = {
    "id_conductor": userId,
    "fecha": fechaActual,
    "username": userName
  };

  final response2 = await http.post(url2, body: data2);
  var value2 = response2.body;

  var convertirDecode = jsonDecode(value2);

  String element;
  int aumentador = 1;
  bool verificador = false;
  element = convertirDecode.toString();
  for (int i = 0; i < element.length; i++) {
    String caracter = "";
    caracter = element.substring(i, aumentador);
    if (caracter == "[") {
      verificador = true;
    }
    aumentador++;
  }

  if (verificador == false) {
    var c = {"error": "No hay servicios"};
    sendPort.send(jsonEncode(c));
  } else {
    for (int i = 0; i < convertirDecode.length; i++) {
      String fecha_hora1 = convertirDecode[i]["fecha_hora_servicio"].toString();
      var fecha_hora2 = fecha_hora1.split(" ");
      String fecha_hora3 = fecha_hora2[2];
      String fecha_hora4 = fecha_hora3.toLowerCase();
      String fecha_hora5 = fecha_hora4.toString();

      String fecha_hora_api1 = fechaRamdon.toString();
      var fecha_hora_api2 = fecha_hora_api1.split(" ");
      String fecha_hora_api3 = fecha_hora_api2[2];
      String fecha_hora_api4 = fecha_hora_api3.toString();

      String fecha_hora_ubicacion1 = fecha_hora2[1];
      var fecha_hora_ubicacion2 = fecha_hora_ubicacion1.split(":");
      String fecha_hora_ubicacion1_api = fecha_hora_api2[1];
      var fecha_hora_ubicacion2_api = fecha_hora_ubicacion1_api.split(":");
      var contenedor_minuto_normal = "";
      var contenedor_minuto_api = "";
      var contenedor_hora_api = "";
      var contenedor_hora_normal = "";

      for (int v = 0; v < 10; v++) {
        String contenedor_array = "0" + v.toString();
        if (contenedor_array == fecha_hora_ubicacion2[1].toString()) {
          contenedor_minuto_normal = v.toString();
          v = 10;
        } else {
          contenedor_minuto_normal = fecha_hora_ubicacion2[1].toString();
        }
      }

      for (int c = 0; c < 10; c++) {
        String contenedor_array = "0" + c.toString();
        if (contenedor_array == fecha_hora_ubicacion2_api[0].toString()) {
          contenedor_hora_api = c.toString();
          c = 10;
        } else {
          contenedor_hora_api = fecha_hora_ubicacion2_api[0].toString();
        }
      }

      for (int b = 0; b < 10; b++) {
        String contenedor_array = "0" + b.toString();
        if (contenedor_array == fecha_hora_ubicacion2_api[1].toString()) {
          contenedor_minuto_api = b.toString();
          b = 10;
        } else {
          contenedor_minuto_api = fecha_hora_ubicacion2_api[1].toString();
        }
      }

      for (int a = 0; a < 10; a++) {
        String contenedor_array = "0" + a.toString();
        if (contenedor_array == fecha_hora_ubicacion2[0].toString()) {
          contenedor_hora_normal = a.toString();
          a = 10;
        } else {
          contenedor_hora_normal = fecha_hora_ubicacion2[0].toString();
        }
      }

      if (fecha_hora5 == fecha_hora_api4) {
        if (fecha_hora_api4 == "am") {
          if (int.parse(contenedor_hora_api) == 12) {
            contenedor_hora_api = "0";
          }
        }

        if (fecha_hora5 == "am") {
          if (int.parse(contenedor_hora_normal) == 12) {
            contenedor_hora_normal = "0";
          }
        }

        if (fecha_hora_api4 == "pm") {
          if (int.parse(contenedor_hora_api) == 12) {
            contenedor_hora_api = "0";
          }
        }

        if (fecha_hora5 == "pm") {
          if (int.parse(contenedor_hora_normal) == 12) {
            contenedor_hora_normal = "0";
          }
        }

        if (int.parse(contenedor_hora_api) ==
            int.parse(contenedor_hora_normal)) {
          if (int.parse(contenedor_minuto_api) >
              int.parse(contenedor_minuto_normal)) {
            print(convertirDecode[i]);
            print("convertirDecode[i] ======================== 1");
            var array = {
              "id_servicio": convertirDecode[i]["id_servicio"],
              "id_horario": convertirDecode[i]["id_horario"],
              "descripcion": convertirDecode[i]["descripcion"],
              "ciudad": convertirDecode[i]["ciudad"],
              "direccion_llegada": convertirDecode[i]["direccion_llegada"],
              "direccion_salida": convertirDecode[i]["direccion_salida"],
              "id_paciente": convertirDecode[i]["id_paciente"],
              "documento": convertirDecode[i]["documento"],
              "nom_paciente": convertirDecode[i]["nom_paciente"],
              "fecha_hora_servicio": convertirDecode[i]["fecha_hora_servicio"],
              "lat": convertirDecode[i]["lat"],
              "lon": convertirDecode[i]["lon"],
              "estado": "inactivo"
            };
            arrayServicio.add(array);
          } else {
            print(convertirDecode[i]);
            print("convertirDecode[i] ======================== 2");

            var array = {
              "id_servicio": convertirDecode[i]["id_servicio"],
              "id_horario": convertirDecode[i]["id_horario"],
              "descripcion": convertirDecode[i]["descripcion"],
              "ciudad": convertirDecode[i]["ciudad"],
              "direccion_llegada": convertirDecode[i]["direccion_llegada"],
              "direccion_salida": convertirDecode[i]["direccion_salida"],
              "id_paciente": convertirDecode[i]["id_paciente"],
              "documento": convertirDecode[i]["documento"],
              "nom_paciente": convertirDecode[i]["nom_paciente"],
              "fecha_hora_servicio": convertirDecode[i]["fecha_hora_servicio"],
              "lat": convertirDecode[i]["lat"],
              "lon": convertirDecode[i]["lon"],
              "estado": "activo"
            };
            arrayServicio.add(array);
          }
        } else if (int.parse(contenedor_hora_api) <
            int.parse(contenedor_hora_normal)) {
          print(convertirDecode[i]);
          print("convertirDecode[i] ======================== 3");
          var array = {
            "id_servicio": convertirDecode[i]["id_servicio"],
            "id_horario": convertirDecode[i]["id_horario"],
            "descripcion": convertirDecode[i]["descripcion"],
            "ciudad": convertirDecode[i]["ciudad"],
            "direccion_llegada": convertirDecode[i]["direccion_llegada"],
            "direccion_salida": convertirDecode[i]["direccion_salida"],
            "id_paciente": convertirDecode[i]["id_paciente"],
            "documento": convertirDecode[i]["documento"],
            "nom_paciente": convertirDecode[i]["nom_paciente"],
            "fecha_hora_servicio": convertirDecode[i]["fecha_hora_servicio"],
            "lat": convertirDecode[i]["lat"],
            "lon": convertirDecode[i]["lon"],
            "estado": "activo"
          };
          arrayServicio.add(array);
        } else if (int.parse(contenedor_hora_api) >
            int.parse(contenedor_hora_normal)) {
          print(convertirDecode[i]);
          print("convertirDecode[i] ======================== 4");
          var array = {
            "id_servicio": convertirDecode[i]["id_servicio"],
            "id_horario": convertirDecode[i]["id_horario"],
            "descripcion": convertirDecode[i]["descripcion"],
            "ciudad": convertirDecode[i]["ciudad"],
            "direccion_llegada": convertirDecode[i]["direccion_llegada"],
            "direccion_salida": convertirDecode[i]["direccion_salida"],
            "id_paciente": convertirDecode[i]["id_paciente"],
            "documento": convertirDecode[i]["documento"],
            "nom_paciente": convertirDecode[i]["nom_paciente"],
            "fecha_hora_servicio": convertirDecode[i]["fecha_hora_servicio"],
            "lat": convertirDecode[i]["lat"],
            "lon": convertirDecode[i]["lon"],
            "estado": "inactivo"
          };
          arrayServicio.add(array);
        }
      } else if (fecha_hora_api4 == "am") {
        if (fecha_hora5 == "pm") {
          print(convertirDecode[i]);
          print("convertirDecode[i] ======================== 5");
          var array = {
            "id_servicio": convertirDecode[i]["id_servicio"],
            "id_horario": convertirDecode[i]["id_horario"],
            "descripcion": convertirDecode[i]["descripcion"],
            "ciudad": convertirDecode[i]["ciudad"],
            "direccion_llegada": convertirDecode[i]["direccion_llegada"],
            "direccion_salida": convertirDecode[i]["direccion_salida"],
            "id_paciente": convertirDecode[i]["id_paciente"],
            "documento": convertirDecode[i]["documento"],
            "nom_paciente": convertirDecode[i]["nom_paciente"],
            "fecha_hora_servicio": convertirDecode[i]["fecha_hora_servicio"],
            "lat": convertirDecode[i]["lat"],
            "lon": convertirDecode[i]["lon"],
            "estado": "activo"
          };
          arrayServicio.add(array);
        }
      } else {
        print(convertirDecode[i]);
        print("convertirDecode[i] ======================== 6");
        var array = {
          "id_servicio": convertirDecode[i]["id_servicio"],
          "id_horario": convertirDecode[i]["id_horario"],
          "descripcion": convertirDecode[i]["descripcion"],
          "ciudad": convertirDecode[i]["ciudad"],
          "direccion_llegada": convertirDecode[i]["direccion_llegada"],
          "direccion_salida": convertirDecode[i]["direccion_salida"],
          "id_paciente": convertirDecode[i]["id_paciente"],
          "documento": convertirDecode[i]["documento"],
          "nom_paciente": convertirDecode[i]["nom_paciente"],
          "fecha_hora_servicio": convertirDecode[i]["fecha_hora_servicio"],
          "lat": convertirDecode[i]["lat"],
          "lon": convertirDecode[i]["lon"],
          "estado": "inactivo"
        };
        arrayServicio.add(array);
      }
    }

    sendPort.send(jsonEncode(arrayServicio));
  }
}

Future<void> obtenerTodo(List params) async {
  SendPort sendPort = params[0];
  String userId = params[1];
  String userName = params[2];
  int aumentador = 0;

  _timer = Timer.periodic(Duration(seconds: 1), (timer) {
    aumentador++;
    if (verificar == false) {
      if (aumentador == 3) {
        ConsultarServicio(sendPort, userId, userName);
        aumentador = 0;
        verificar = true;
      }
    } else {
      if (aumentador == 900) {
        ConsultarServicio(sendPort, userId, userName);
        aumentador = 0;
      }
    }
  });
  ;
}
