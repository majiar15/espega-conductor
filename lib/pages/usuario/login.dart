import 'dart:convert';
import 'dart:io';
import 'package:connection_status_bar/connection_status_bar.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:http/http.dart' as http;
import 'package:conductor/SizeConfig.dart';
import 'package:conductor/helpers/AppData.dart';


class LoginScreen extends StatefulWidget {
  @override
  _LoginScreenState createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {

  TextEditingController email = TextEditingController();
  TextEditingController password = TextEditingController();
  String compro;

  bool signin = true;

  Widget _buildEmailTF() {
    return new Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        new Padding(
          padding: EdgeInsets.only(left: SizeConfig.safeBlockHorizontal * 2.5),
          child: Text(
            'Usuario',
            style: TextStyle(
              color: Colors.black26,
              fontWeight: FontWeight.bold,
              fontFamily: 'OpenSans',
              fontSize: SizeConfig.safeBlockHorizontal * 4,
            ),
          ),
        ),
        new SizedBox(
          height: SizeConfig.safeBlockVertical * 1,
        ),
        new Container(
          decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.circular(SizeConfig.safeBlockHorizontal * 5),
            border: Border.all(
              width: 1,
              color: Colors.grey
            )
          ),
          child: Center(
            child: new TextField(
              controller: email,
              keyboardType: TextInputType.emailAddress,
              style: TextStyle(
                color: Colors.black38,
                fontFamily: 'OpenSans',
                fontSize: SizeConfig.safeBlockHorizontal * 4,
              ),
              decoration: new InputDecoration(
                  border: InputBorder.none,
                  contentPadding: EdgeInsets.all(SizeConfig.safeBlockVertical * 2.5),
                  prefixIcon: Icon(
                    Icons.account_circle,
                    color: Colors.black38,
                  ),
                  hintText: 'Digite usuario',
                  hintStyle: TextStyle(
                    color: Colors.black26,
                    fontFamily: 'Opensans',
                  )
              ),
            ),
          ),
        ),
      ],
    );
  }

  Widget _buildPasswordTF() {
    return new Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        new Padding(
          padding: EdgeInsets.only(left: SizeConfig.safeBlockHorizontal * 2.5),
          child: Text(
            'Contraseña',
            style: TextStyle(
              color: Colors.black26,
              fontWeight: FontWeight.bold,
              fontFamily: 'OpenSans',
              fontSize: SizeConfig.safeBlockHorizontal * 4,
            ),
          ),
        ),
        new SizedBox(
          height: SizeConfig.safeBlockVertical * 1,
        ),
        new Container(
          alignment: Alignment.centerLeft,
          decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.circular(SizeConfig.safeBlockHorizontal * 5),
            border: Border.all(
                width: 1,
                color: Colors.grey
            ),
          ),
          child: Center(
            child: new TextField(
              controller: password,
              keyboardType: TextInputType.text,
              obscureText: true,
              style: TextStyle(
                color: Colors.black38,
                fontFamily: 'OpenSans',
                fontSize: SizeConfig.safeBlockHorizontal * 4,
              ),
              decoration: new InputDecoration(
                  border: InputBorder.none,
                  contentPadding: EdgeInsets.all(SizeConfig.safeBlockVertical * 2.5),
                  prefixIcon: Icon(
                    Icons.lock,
                    color: Colors.black38,
                  ),
                  hintText: 'Digite contraseña',
                  hintStyle: TextStyle(
                    color: Colors.black26,
                    fontFamily: 'Opensans',
                  )
              ),
            ),
          ),
        ),
      ],
    );
  }

  Widget _buildLoginBtn() {
    return Container(
      padding: EdgeInsets.symmetric(
        vertical: SizeConfig.safeBlockVertical * 2,
      ),
      width: SizeConfig.safeBlockHorizontal * 100,
      child: RaisedButton(
        elevation: 0.0,
        onPressed: () {
          if (email.text.isEmpty || password.text.isEmpty) {
            _showMyDialogCampos();
          } else {
            LoguearUser();
          }
        },
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(SizeConfig.safeBlockHorizontal * 10),
        ),
        color: Colors.blue[300],
        child: Text(
          'Ingresar',
          style: TextStyle(
            color: Colors.white,
            letterSpacing: 1.0,
            fontSize: SizeConfig.safeBlockHorizontal * 4.5,
            fontWeight: FontWeight.bold,
            fontFamily: 'OpenSans',
          ),
        ),
      ),
    );
  }

  Widget buildSocialBtn() {
    return GestureDetector(
      onTap: () {
      },
      child: Container(
        margin: EdgeInsets.only(left: SizeConfig.safeBlockHorizontal * 2.0),
        width: 20,
        height: 20,
        decoration: BoxDecoration(
          shape: BoxShape.circle,
          image: DecorationImage(
              image: AssetImage("assets/logos/logo_login4.jpg"), fit: BoxFit.cover),
        ),
      ),
    );
  }

  void LoguearUser() async {

    setState(() {
      signin = false;
    });

    var url = "http://transporte-paciente.herokuapp.com/api/conductor/login";

    var data = {"username": email.text, "password": password.text};

    final res = await  http.post(url, body: data);

    if(res.statusCode == 200){

      var cont = jsonDecode(res.body);

      String mensaje = "";
      if (cont['resp'] == "error") {

        setState(() {
          signin = true;
        });

        _showMyDialogEmail();
      }else {
        var pasarString = cont['nombre'].toString();
        var pasarArray = pasarString.split(" ");
        var contenedorNombre = pasarArray[0];
        var convertir = contenedorNombre.toLowerCase();
        var mayuscula = convertir.capitalize();
        String id = cont['id'].toString();
        appData.id_paciente = id;
        appData.username = email.text.toString();
        Navigator.popAndPushNamed(context, "/inicio", arguments: {
          'id': id,
          'nombre': cont['nombre'].toString(),
          'documento': cont['documento'].toString(),
          'username': email.text,
          'password': password.text,
        });
      }

    }else{
      print("Hubo problema para conectar red");
    }

  }

  @override
  Widget build(BuildContext context) {

    SizeConfig().init(context);
    if(signin == false){
      return Scaffold(
        body: Container(
          color: Colors.white,
          child: Container(
            width: SizeConfig.safeBlockHorizontal * 100,
            height: SizeConfig.blockSizeVertical * 100,
            child: Column(
              children: [
                Container(
                    width: SizeConfig.safeBlockHorizontal * 100,
                    height: SizeConfig.safeBlockVertical * 90,
                    child: Center(
                      child: CircularProgressIndicator(
                        backgroundColor: Colors.black12,
                      ),
                    )
                ),
                Container(
                  width: SizeConfig.safeBlockHorizontal * 100,
                  height: SizeConfig.safeBlockVertical * 10,
                  child: Center(
                    child: Text(
                      "Cargando..",
                      style: TextStyle(
                          fontSize: SizeConfig.safeBlockHorizontal * 5,
                          color: Colors.black26,
                          fontFamily: 'OpenSans'
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      );
    }else{
      return Scaffold(
          body: Container(
            decoration: BoxDecoration(color: Colors.white),
            child: Center(
              child: Container(
                width: SizeConfig.safeBlockHorizontal * 100,
                height: SizeConfig.blockSizeVertical * 100,
                child:
                SingleChildScrollView(
                  child: Column(children: <Widget>[
                    Container(
                      width: SizeConfig.safeBlockHorizontal * 100,
                      height: SizeConfig.safeBlockVertical * 40,
                      decoration: BoxDecoration(
                          image: DecorationImage(
                              image: AssetImage('assets/logos/background.png'),
                              fit: BoxFit.fill)),
                      child: Column(
                        children: <Widget>[
                          Container(
                            width: SizeConfig.safeBlockHorizontal * 100,
                            height: SizeConfig.safeBlockVertical * 5,
                          ),
                          Container(
                            width: SizeConfig.safeBlockHorizontal * 100,
                            height: SizeConfig.safeBlockVertical * 15,
                            child: Padding(
                              padding: EdgeInsets.only(top: SizeConfig.safeBlockVertical * 5, left: SizeConfig.safeBlockHorizontal * 10),
                              child: Text(
                                "Iniciar sesion",
                                style: TextStyle(
                                    color: Colors.white,
                                    fontSize: SizeConfig.safeBlockHorizontal * 8,
                                    fontWeight: FontWeight.bold,
                                    fontFamily: 'Mejor',
                                ),
                              ),
                            ),
                          ),
                          Container(
                            width: SizeConfig.safeBlockHorizontal * 100,
                            height: SizeConfig.safeBlockVertical * 20,
                            child: Column(
                              children: [
                                Container(
                                  width: SizeConfig.safeBlockHorizontal * 100,
                                  height: SizeConfig.safeBlockVertical * 5,
                                ),
                                Container(
                                  width: SizeConfig.safeBlockHorizontal * 100,
                                  height: SizeConfig.safeBlockVertical * 11.5,
                                  child: Row(
                                    children: [
                                      Container(
                                        width: SizeConfig.safeBlockHorizontal * 72,
                                        height: SizeConfig.safeBlockVertical * 100,
                                      ),
                                      Container(
                                        width: SizeConfig.safeBlockHorizontal * 25,
                                        height: SizeConfig.safeBlockVertical * 100,
                                        child: buildSocialBtn(),
                                      ),
                                      Container(
                                        width: SizeConfig.safeBlockHorizontal * 3,
                                        height: SizeConfig.safeBlockVertical * 100,
                                      ),
                                    ],
                                  ),
                                ),
                                Container(
                                  width: SizeConfig.safeBlockHorizontal * 100,
                                  height: SizeConfig.safeBlockVertical * 3.5,
                                ),
                              ],
                            ),
                          ),
                        ],
                      ),
                    ),
                    Container(
                      width: SizeConfig.safeBlockHorizontal * 100,
                      height: SizeConfig.safeBlockVertical * 10,
                      child: Center(
                        child: Padding(
                          padding: EdgeInsets.only(bottom: SizeConfig.safeBlockVertical * 5),
                          child: Text(
                            "Ingreso conductor",
                            style: TextStyle(
                              color: Colors.black26,
                              fontWeight: FontWeight.bold,
                              fontFamily: 'OpenSans',
                              fontSize: SizeConfig.safeBlockHorizontal * 5,
                            ),
                          ),
                        ),
                      ),
                    ),
                    Container(
                      width: SizeConfig.safeBlockHorizontal * 100,
                      height: SizeConfig.safeBlockVertical * 3,
                    ),
                    Container(
                      width: SizeConfig.safeBlockHorizontal * 100,
                      height: SizeConfig.safeBlockVertical * 45,
                      child: Center(
                        child: Container(
                          width: SizeConfig.safeBlockHorizontal * 80,
                          height: SizeConfig.safeBlockVertical * 45,
                          child: Column(
                            children: [
                              Container(
                                width: SizeConfig.safeBlockHorizontal * 100,
                                height: SizeConfig.safeBlockVertical * 15,
                                child: _buildEmailTF(),
                              ),
                              Container(
                                width: SizeConfig.safeBlockHorizontal * 100,
                                height: SizeConfig.safeBlockVertical * 15,
                                child:  _buildPasswordTF(),
                              ),
                              Container(
                                width: SizeConfig.safeBlockHorizontal * 100,
                                height: SizeConfig.safeBlockVertical * 15,
                                child: Column(
                                  children: [
                                    Container(
                                      width: SizeConfig.safeBlockHorizontal * 100,
                                      height: SizeConfig.safeBlockVertical * 4,
                                    ),
                                    Container(
                                      width: SizeConfig.safeBlockHorizontal * 100,
                                      height: SizeConfig.safeBlockVertical * 11,
                                      child: _buildLoginBtn(),
                                    ),
                                  ],
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                    ),
                    Container(
                      width: SizeConfig.safeBlockHorizontal * 100,
                      height: SizeConfig.safeBlockVertical * 6,
                      child: Center(
                        child: GestureDetector(
                            onTap: () {},
                            child: Padding(
                              padding: EdgeInsets.only(bottom: SizeConfig.safeBlockVertical * 2),
                              child: Text(
                                "Espega",
                                style: TextStyle(
                                    color: Colors.black38,
                                    fontSize: SizeConfig.safeBlockHorizontal * 4,
                                    fontWeight: FontWeight.w400,
                                    fontFamily: 'OpenSans'
                                ),
                              ),
                            )
                        ),
                      ),
                    ),
                  ]),
                ),
              ),
            ),
          )
      );

    }

  }

  Future<void> _showMyDialogEmail() async {
    return showDialog<void>(
      context: context,
      barrierDismissible: false, // user must tap button!
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text(
            'Cuenta',
            style: TextStyle(
              color: Colors.grey[600],
              fontFamily: 'OpenSans',
              fontSize: SizeConfig.safeBlockHorizontal * 5.5,
            ),
          ),
          content: SingleChildScrollView(
            child: ListBody(
              children: <Widget>[
                Text(
                  'Credenciales inválidas.',
                  style: TextStyle(
                    color: Colors.grey,
                    fontFamily: 'OpenSans',
                    fontSize: SizeConfig.safeBlockHorizontal * 4,
                  ),
                ),
              ],
            ),
          ),
          actions: <Widget>[
            TextButton(
              child: Text(
                'Aceptar',
                style: TextStyle(
                  color: Colors.grey[600],
                  fontFamily: 'OpenSans',
                ),
              ),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }

  Future<void> _showMyDialogCampos() async {
    return showDialog<void>(
      context: context,
      barrierDismissible: false, // user must tap button!
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text(
            'Error',
            style: TextStyle(
              color: Colors.grey[600],
              fontFamily: 'OpenSans',
              fontSize: SizeConfig.safeBlockHorizontal * 5.5,
            ),
          ),
          content: SingleChildScrollView(
            child: ListBody(
              children: <Widget>[
                Text(
                  'Campos vacios.',
                  style: TextStyle(
                    color: Colors.grey,
                    fontFamily: 'OpenSans',
                    fontSize: SizeConfig.safeBlockHorizontal * 4,
                  ),
                ),
              ],
            ),
          ),
          actions: <Widget>[
            TextButton(
              child: Text(
                'Aceptar',
                style: TextStyle(
                  color: Colors.grey[600],
                  fontFamily: 'OpenSans',
                ),
              ),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }
  void _pasarRegister() {
    Navigator.of(context).pushNamed("/register");
  }
}

extension StringExtension on String {
  String capitalize() {
    return "${this[0].toUpperCase()}${this.substring(1)}";
  }
}


