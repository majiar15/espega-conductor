import 'dart:async';
import 'dart:convert';
import 'dart:isolate';
import 'package:http/http.dart' as http;
import 'package:conductor/pages/usuario/serviciosUsuario.dart';

Isolate _isolate;
Timer _timer;
int aumentador = 0;
int aumentador2 = 0;
var tiempo = 0;
var ubicacion = "";
var servicioEstado = false;
var resultadEstado = "";

Future<ReceivePort> VerificarHoraServicio(String username, servicio) async {
  ReceivePort receivePort = new ReceivePort();
  _isolate = await Isolate.spawn(
      obtenerTodo, [
    receivePort.sendPort,
    username,
    servicio
  ]);
  return receivePort;
}

stopVerificarHoraServicio(){
  if(_isolate != null){
    _isolate.kill(priority: Isolate.immediate);
  }
}

Future<void> obtenerTodo(List params) async {

  SendPort sendPort = params[0];
  String userName = params[1];
  var servicio = params[2];
  String fechas_horas = "";
  var convertir;
  convertir = jsonEncode(servicio);
  var convertirDecode;
  convertirDecode = jsonDecode(convertir);
  var contenedor = [];
  var arrayServicio = [];

  String element;
  int aumentador = 1;
  bool verificador = false;
  element = servicio.toString();
  for(int i = 0; i < element.length; i++){
    String caracter = "";
    caracter = element.substring(i, aumentador);
    if(caracter == "["){
      verificador = true;
    }
    aumentador++;
  }

  if(verificador == false){


    tiempo = 0;
    ubicacion = "no ubicar";
    servicioEstado = false;

  }else{

    final url = "http://transporte-paciente.herokuapp.com/api/generic/fecha/get";

    var data = {
      "username": userName
    };

    final response2 = await http.post(url, body: data);
    var cont = jsonDecode(response2.body);

    fechas_horas = cont['fecha_actual'].toString();

    for(int i = 0; i < convertirDecode.length; i++){

      String fecha_hora1 = convertirDecode[i]["fecha_hora_servicio"].toString();
      var fecha_hora2 = fecha_hora1.split(" ");
      String fecha_hora3 = fecha_hora2[2];
      String fecha_hora4 = fecha_hora3.toLowerCase();
      String fecha_hora5 = fecha_hora4.toString();
      String fecha_hora6 = fecha_hora2[0].toString();

      String fecha_hora_api1 = fechas_horas.toString();
      var fecha_hora_api2 = fecha_hora_api1.split(" ");
      String fecha_hora_api3 = fecha_hora_api2[2];
      String fecha_hora_api4 = fecha_hora_api3.toString();
      String fecha_hora_api5 = fecha_hora_api2[0].toString();

      if(fecha_hora6 == fecha_hora_api5){

        if(fecha_hora5 == fecha_hora_api4){

          if(convertirDecode[i]["estado"] == "activo"){
            contenedor = [];
            String fecha_hora_ubicacion1 = fecha_hora2[1];
            var fecha_hora_ubicacion2 = fecha_hora_ubicacion1.split(":");
            String fecha_hora_ubicacion1_api = fecha_hora_api2[1];
            var fecha_hora_ubicacion2_api = fecha_hora_ubicacion1_api.split(":");
            var contenedor_minuto_normal = "";
            var contenedor_minuto_api = "";
            var contenedor_hora_api = "";
            var contenedor_hora_normal = "";

            for (int v = 0; v < 10; v++) {
              String contenedor_array =  "0" + v.toString();
              if(contenedor_array == fecha_hora_ubicacion2[1].toString()){
                contenedor_minuto_normal = v.toString();
                v = 10;
              }else{
                contenedor_minuto_normal = fecha_hora_ubicacion2[1].toString();
              }
            }

            for (int c = 0; c < 10; c++) {
              String contenedor_array =  "0" + c.toString();
              if(contenedor_array == fecha_hora_ubicacion2_api[0].toString()){
                contenedor_hora_api = c.toString();
                c = 10;
              }else{
                contenedor_hora_api = fecha_hora_ubicacion2_api[0].toString();
              }
            }

            for (int b = 0; b < 10; b++) {
              String contenedor_array =  "0" + b.toString();
              if(contenedor_array == fecha_hora_ubicacion2_api[1].toString()){
                contenedor_minuto_api = b.toString();
                b = 10;
              }else{
                contenedor_minuto_api = fecha_hora_ubicacion2_api[1].toString();
              }
            }

            for (int a = 0; a < 10; a++) {
              String contenedor_array =  "0" + a.toString();
              if(contenedor_array == fecha_hora_ubicacion2[0].toString()){
                contenedor_hora_normal = a.toString();
                a = 10;
              }else{
                contenedor_hora_normal = fecha_hora_ubicacion2[0].toString();
              }
            }

            if(fecha_hora_api4 == "am"){
              if(int.parse(contenedor_hora_api) == 12){
                contenedor_hora_api = "0";
              }
            }

            if(fecha_hora5 == "am"){
              if(int.parse(contenedor_hora_normal) == 12){
                contenedor_hora_normal = "0";
              }
            }

            if(int.parse(contenedor_hora_api) == int.parse(contenedor_hora_normal)){
              if(int.parse(contenedor_minuto_api) > int.parse(contenedor_minuto_normal)){
              }else{
                arrayServicio.add(convertirDecode[i]);
              }
            }else if(int.parse(contenedor_hora_api) < int.parse(contenedor_hora_normal)) {
              arrayServicio.add(convertirDecode[i]);
            }else if(int.parse(contenedor_hora_api) > int.parse(contenedor_hora_normal)){
            }
          }

        }

      }

    }

    if(arrayServicio.length == 0){

      tiempo = 0;
      ubicacion = "no ubicar";
      servicioEstado = false;
      resultadEstado = "no hay servicios";

    }else{

      var convertirArray = jsonEncode(arrayServicio);
      var servicio_fecha = jsonDecode(convertirArray);

      String fecha_hora1 = servicio_fecha[0]["fecha_hora_servicio"].toString();
      var fecha_hora2 = fecha_hora1.split(" ");
      String fecha_hora3 = fecha_hora2[2];
      String fecha_hora4 = fecha_hora3.toLowerCase();
      String fecha_hora5 = fecha_hora4.toString();
      String fecha_hora6 = fecha_hora2[0].toString();

      String fecha_hora_api1 = fechas_horas.toString();
      var fecha_hora_api2 = fecha_hora_api1.split(" ");
      String fecha_hora_api3 = fecha_hora_api2[2];
      String fecha_hora_api4 = fecha_hora_api3.toString();
      String fecha_hora_api5 = fecha_hora_api2[0].toString();

      contenedor = [];
      String fecha_hora_ubicacion1 = fecha_hora2[1];
      var fecha_hora_ubicacion2 = fecha_hora_ubicacion1.split(":");
      String fecha_hora_ubicacion1_api = fecha_hora_api2[1];
      var fecha_hora_ubicacion2_api = fecha_hora_ubicacion1_api.split(":");
      var contenedor_minuto_normal = "";
      var contenedor_minuto_api = "";
      var contenedor_hora_api = "";
      var contenedor_hora_normal = "";


      for (int i = 0; i < 10; i++) {
        String contenedor_array =  "0" + i.toString();
        if(contenedor_array == fecha_hora_ubicacion2[1].toString()){
          contenedor_minuto_normal = i.toString();
          i = 10;
        }else{
          contenedor_minuto_normal = fecha_hora_ubicacion2[1].toString();
        }
      }

      for (int i = 0; i < 10; i++) {
        String contenedor_array =  "0" + i.toString();
        if(contenedor_array == fecha_hora_ubicacion2_api[0].toString()){
          contenedor_hora_api = i.toString();
          i = 10;
        }else{
          contenedor_hora_api = fecha_hora_ubicacion2_api[0].toString();
        }
      }

      for (int i = 0; i < 10; i++) {
        String contenedor_array =  "0" + i.toString();
        if(contenedor_array == fecha_hora_ubicacion2_api[1].toString()){
          contenedor_minuto_api = i.toString();
          i = 10;
        }else{
          contenedor_minuto_api = fecha_hora_ubicacion2_api[1].toString();
        }
      }

      for (int i = 0; i < 10; i++) {
        String contenedor_array =  "0" + i.toString();
        if(contenedor_array == fecha_hora_ubicacion2[0].toString()){
          contenedor_hora_normal = i.toString();
          i = 10;
        }else{
          contenedor_hora_normal = fecha_hora_ubicacion2[0].toString();
        }
      }

      if(int.parse(contenedor_minuto_normal) <= 29){
        contenedor = [];
        var resultadoContendor = 0;
        resultadoContendor = int.parse(contenedor_hora_normal) - 1;
        var resultado = 0;

        if(resultadoContendor == 0){

          resultado == 12;

        }else if(resultadoContendor > 0){

          resultado = resultadoContendor;

        }


        if(int.parse(contenedor_hora_api) == int.parse(contenedor_hora_normal)){

          if(int.parse(contenedor_minuto_api) < int.parse(contenedor_minuto_normal)){

            var resultado =  int.parse(contenedor_minuto_normal) - int.parse(contenedor_minuto_api);
            var resultado2 = resultado * 60;
            var tiempo1 = resultado2;

            tiempo = tiempo1;
            ubicacion = "ubicar";
            servicioEstado = false;


          }else if(int.parse(contenedor_minuto_api) > int.parse(contenedor_minuto_normal)){

            tiempo = 0;
            ubicacion = "no ubicar";
            servicioEstado = false;

          }

        }else if (int.parse(contenedor_hora_api) == resultado){

          var resultado2 = 0;
          resultado2 = 30 - int.parse(contenedor_minuto_normal);
          var resultado3 = 0;
          resultado3 = 60 - resultado2;

          if(int.parse(contenedor_minuto_api) < resultado3){

            var resultado4 = resultado3 - int.parse(contenedor_minuto_api);
            var resultado5 = resultado4 * 60;
            var tiempo2 = resultado5;

            tiempo = tiempo2;
            ubicacion = "no ubicar";
            servicioEstado = false;

          }else if (int.parse(contenedor_minuto_api) >= resultado3){

            var resultado6 = int.parse(contenedor_minuto_api) - resultado3;
            var resultado7 = 30 - resultado6;
            var resultado8 = resultado7 * 60;
            var tiempo3 = resultado8;

            tiempo = tiempo3;
            ubicacion = "ubicar";
            servicioEstado = false;

          }
        }else if(int.parse(contenedor_hora_api) < resultado){

          if(int.parse(contenedor_minuto_api) == 0){

            var resultado9 = resultado - int.parse(contenedor_hora_api);
            var hora2 = resultado9 * 60;
            var resultado10 = 30 - int.parse(contenedor_minuto_normal);
            var resultado11 = 60 - resultado10;
            var tiempoServicio = hora2 + resultado11;
            var resultadoTiempo = tiempoServicio * 60;
            var tiempo4 = resultadoTiempo;

            tiempo = tiempo4;
            ubicacion = "no ubicar";
            servicioEstado = false;

          }else if(int.parse(contenedor_minuto_api) > 0) {

            var resultado12 = 60 - int.parse(contenedor_minuto_api);
            var resultado13 = resultado - 1;
            var resultado14 = 0;

            if(resultado13 == 0){
              resultado14 = 12;
            }else if(resultado13 > 0){
              resultado14 = resultado13;
            }

            var resultado15 = resultado14 - int.parse(contenedor_hora_api);
            var resultado16 = resultado15 * 60;
            var resultado17 = 30 - int.parse(contenedor_minuto_normal);
            var resultado18 = 60 - resultado17;
            var resultado19 = resultado12 + resultado16 + resultado18;
            var resultado20 = resultado19 * 60;
            var tiempo5 = resultado20;

            tiempo = tiempo5;
            ubicacion = "no ubicar";
            servicioEstado = false;

          }

        } else {

          tiempo = 0;
          ubicacion = "no ubicar";
          servicioEstado = false;

        }

      }else if(int.parse(contenedor_minuto_normal) >= 30){

        if(int.parse(contenedor_hora_api) == int.parse(contenedor_hora_normal)){

          var resultado22 = 0;
          resultado22 = int.parse(contenedor_minuto_normal) - 30;

          if(int.parse(contenedor_minuto_api) < resultado22){

            var resultado23 = resultado22 - int.parse(contenedor_minuto_api);
            var resultado24 = resultado23 * 60;
            var tiempo6 = resultado24;

            tiempo = tiempo6;
            ubicacion = "no ubicar";
            servicioEstado = false;
            resultadEstado = "estoy aqui";

          }else if (int.parse(contenedor_minuto_api) >= resultado22){

            var resultado25 = int.parse(contenedor_minuto_api) - resultado22;
            var resultado26 = 30 - resultado25;
            var resultado27 = resultado26 * 60;
            var tiempo7 = resultado27;

            tiempo = tiempo7;
            ubicacion = "ubicar";
            servicioEstado = false;

          }

        }else if(int.parse(contenedor_hora_api) < int.parse(contenedor_hora_normal)){

          /*
          var horaActualRe = 0;
          horaActualRe = int.parse(contenedor_hora_normal) - 1;
          var horaActual = 0;

          if(horaActualRe == 0){
            horaActual = 12;
          }else if(horaActualRe > 0){
            horaActual = horaActualRe;
          }
           */

          if(int.parse(contenedor_minuto_api) == 0){

            var resultado28 = int.parse(contenedor_hora_normal) - int.parse(contenedor_hora_api);
            var hora3 = resultado28 * 60;
            var resultado29 = int.parse(contenedor_minuto_normal) - 30;
            var tiempoServicio2 = hora3 + resultado29;
            var resultadoTiempo2 = tiempoServicio2 * 60;
            var tiempo8 = resultadoTiempo2;

            tiempo = tiempo8;
            ubicacion = "no ubicar";
            servicioEstado = false;

          }else if(int.parse(contenedor_minuto_api) > 0) {

            var resultado30 = 60 - int.parse(contenedor_minuto_api);
            var resultado31 = int.parse(contenedor_hora_normal) - 1;
            var resultado32 = 0;

            if(resultado31 == 0){
              resultado32 = 12;
            }else if(resultado31 > 0){
              resultado32 = resultado31;
            }

            var resultado33 = resultado32 - int.parse(contenedor_hora_api);
            var resultado34 = resultado33 * 60;
            var resultado35 = int.parse(contenedor_minuto_normal) - 30;
            var resultado36 = resultado30 + resultado34 + resultado35;
            var resultado37 = resultado36 * 60;
            var tiempo9 = resultado37;

            tiempo = tiempo9;
            ubicacion = "no ubicar";
            servicioEstado = false;

          }

        }

      }

    }
  }

  contenedor = [{
    "tiempo" : tiempo,
    "ubicacion" : ubicacion,
    "servicio" : servicioEstado,
    "saludar" : resultadEstado
  }];

  sendPort.send(jsonEncode(contenedor));


}