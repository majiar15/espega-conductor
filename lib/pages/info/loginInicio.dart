import 'dart:convert';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:http/http.dart' as http;
import 'package:conductor/helpers/AppData.dart';
import 'package:conductor/pages/usuario/VerificarHoraServicio.dart';
import 'package:conductor/pages/usuario/serviciosUsuario.dart';

import '../../SizeConfig.dart';
import '../componentes.dart';

class LoginInicioScreen extends StatefulWidget {
  @override
  _LoginInicioScreenState createState() => _LoginInicioScreenState();
}

class _LoginInicioScreenState extends State<LoginInicioScreen> {



  void initState() {
    // TODO: implement initState
    super.initState();
  }

  Future<void> showMyDialogSignOut() async {
    return showDialog<void>(
      context: context,
      barrierDismissible: false, // user must tap button!
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text(
            'Cuenta',
            style: TextStyle(
              color: Colors.grey[600],
              fontFamily: 'OpenSans',
            ),
          ),
          content: SingleChildScrollView(
            child: ListBody(
              children: <Widget>[
                Text(
                  'Quieres cerrar sesion ?',
                  style: TextStyle(
                    color: Colors.grey,
                    fontFamily: 'OpenSans',
                  ),
                ),
              ],
            ),
          ),
          actions: <Widget>[
            TextButton(
              child: Text(
                'Si',
                style: TextStyle(
                  color: Colors.grey[600],
                  fontFamily: 'OpenSans',
                ),
              ),
              onPressed: () {

                stopActualizarServicio();
                stopVerificarHoraServicio();
                appData.timer?.cancel();

                Navigator.of(context).pushNamedAndRemoveUntil(
                    "/login", (Route<dynamic> route) => false);
              },
            ),
            TextButton(
              child: Text(
                'No',
                style: TextStyle(
                  color: Colors.grey[600],
                  fontFamily: 'OpenSans',
                ),
              ),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }

  Widget buildSocialBtn() {
    return GestureDetector(
      onTap: () {
      },
      child: Container(
        margin: EdgeInsets.only(left: SizeConfig.safeBlockHorizontal * 2.0),
        width: 150,
        height: 150,
        decoration: BoxDecoration(
          image: DecorationImage(
              image: AssetImage("assets/logos/logoPrincipal.png")),
        ),
      ),
    );
  }

  Widget buildBuscarBtn() {
    return GestureDetector(
      onTap: () {},
      child: Container(
        margin: EdgeInsets.only(right: SizeConfig.safeBlockHorizontal * 2),
        width: 20,
        height: 20,
        decoration: BoxDecoration(
          boxShadow: [
            BoxShadow(
              color: Colors.black26,
              blurRadius: 6.0,
              offset: Offset(0, 2),
            ),
          ],
          shape: BoxShape.circle,
          image: DecorationImage(
              image: AssetImage("assets/buscar.jpg"), fit: BoxFit.cover),
        ),
      ),
    );
  }

  Widget buildInicioBtn() {
    Map parametros = ModalRoute.of(context).settings.arguments;
    return GestureDetector(
      onTap: () {

        stopVerificarHoraServicio();
        appData.timer?.cancel();

        Navigator.pushNamed(context, "/inicio", arguments: {
          'id': parametros['id'],
          'nombre': parametros['nombre'],
          'username': parametros['username'],
          'password': parametros['password'],
        });
      },
      child: Container(
        margin: EdgeInsets.only(left: SizeConfig.safeBlockHorizontal * 1.0),
        width: 20,
        height: 20,
        decoration: BoxDecoration(
          shape: BoxShape.circle,
          image: DecorationImage(
              image: AssetImage("assets/inicio2.jpg"), fit: BoxFit.cover),
        ),
      ),
    );
  }

  Widget buildPerfilBtn() {
    Map parametros = ModalRoute.of(context).settings.arguments;
    return GestureDetector(
      onTap: () {
        Navigator.pushNamed(context, "/inicio/perfil", arguments: {
          'id': parametros['id'],
          'nombre': parametros['nombre'],
          'username': parametros['username'],
          'password': parametros['password'],
        });
      },
      child: Container(
        margin: EdgeInsets.only(left: SizeConfig.safeBlockHorizontal * 1.0),
        width: 20,
        height: 20,
        decoration: BoxDecoration(
          shape: BoxShape.circle,
          image: DecorationImage(
              image: AssetImage("assets/perfil_inicio.jpg"), fit: BoxFit.cover),
        ),
      ),
    );
  }

  Widget buildMapBtn() {
    return GestureDetector(
      onTap: () {
        showMyDialogSignOut();
      },
      child: Container(
        margin: EdgeInsets.only(left: SizeConfig.safeBlockHorizontal * 1.0),
        width: 20,
        height: 20,
        decoration: BoxDecoration(
          shape: BoxShape.circle,
          image: DecorationImage(
              image: AssetImage("assets/cerrar_inicio.jpg"), fit: BoxFit.cover),
        ),
      ),
    );
  }

  Widget buildInformationBtn() {
    Map parametros = ModalRoute.of(context).settings.arguments;
    return GestureDetector(
      onTap: () {
        Navigator.pushNamed(context, "/inicio/informacion", arguments: {
          'id': parametros['id'],
          'nombre': parametros['nombre'],
          'username': parametros['username'],
          'password': parametros['password'],
        });
      },
      child: Container(
        margin: EdgeInsets.only(left: SizeConfig.safeBlockHorizontal * 1.0),
        width: 20,
        height: 20,
        decoration: BoxDecoration(
          shape: BoxShape.circle,
          image: DecorationImage(
              image: AssetImage("assets/informacion.jpg"), fit: BoxFit.cover),
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {

    Map parametros = ModalRoute.of(context).settings.arguments;
    SizeConfig().init(context);
    return new WillPopScope(
        onWillPop: () async => false,
        child: Scaffold(
          body: new Container(
            decoration: new BoxDecoration(
              image: new DecorationImage(
                image: new AssetImage("assets/loginInicio.jpg"),
                fit: BoxFit.cover,
              ),
            ),
            child: null /* add child content content here */,
          ),
        ));
  }
}

extension StringExtension on String {
  String capitalize2() {
    return "${this[0].toUpperCase()}${this.substring(1)}";
  }
}