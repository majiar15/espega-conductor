import 'dart:convert';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:http/http.dart' as http;
import 'package:conductor/helpers/AppData.dart';
import 'package:conductor/pages/usuario/VerificarHoraServicio.dart';
import 'package:conductor/pages/usuario/serviciosUsuario.dart';

import '../../SizeConfig.dart';
import '../componentes.dart';

class InformacionScreen extends StatefulWidget {
  @override
  _InformacionScreenState createState() => _InformacionScreenState();
}

class _InformacionScreenState extends State<InformacionScreen> {


  String _nombre;

  TextEditingController servicio = TextEditingController();

  Future<Null> _selectDate(BuildContext context) async {
    final DateTime picked = await showDatePicker(
        context: context,
        initialDate: DateTime(2021),
        firstDate: DateTime(1901, 1),
        lastDate: DateTime(2100));
    if (picked != null && picked != DateTime(2021))
      setState(() {
        var pasarString = picked.toString();
        var convertirArray = pasarString.split(" ");
        servicio.value = TextEditingValue(text: convertirArray[0]);
      });
  }

  void initState() {
    // TODO: implement initState
    super.initState();
  }

  Future obtenerUsuario() async {
    Map parametros = ModalRoute.of(context).settings.arguments;
    final url = "http://transporte-paciente.herokuapp.com/api/paciente/servicios/list";
    var data = {
      "id_paciente": parametros['id'],
      "username": parametros['username'],
    };
    final response = await http.post(url, body: data);
    return response.body;
  }

  Future<void> showMyDialogSignOut() async {
    return showDialog<void>(
      context: context,
      barrierDismissible: false, // user must tap button!
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text(
            'Cuenta',
            style: TextStyle(
              color: Colors.grey[600],
              fontFamily: 'OpenSans',
            ),
          ),
          content: SingleChildScrollView(
            child: ListBody(
              children: <Widget>[
                Text(
                  'Quieres cerrar sesion ?',
                  style: TextStyle(
                    color: Colors.grey,
                    fontFamily: 'OpenSans',
                  ),
                ),
              ],
            ),
          ),
          actions: <Widget>[
            TextButton(
              child: Text(
                'Si',
                style: TextStyle(
                  color: Colors.grey[600],
                  fontFamily: 'OpenSans',
                ),
              ),
              onPressed: () {

                stopActualizarServicio();
                stopVerificarHoraServicio();
                appData.timer?.cancel();

                Navigator.of(context).pushNamedAndRemoveUntil(
                    "/login", (Route<dynamic> route) => false);
              },
            ),
            TextButton(
              child: Text(
                'No',
                style: TextStyle(
                  color: Colors.grey[600],
                  fontFamily: 'OpenSans',
                ),
              ),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }

  Widget _buildFechaTF() {
    return new GestureDetector(
        onTap: () => _selectDate(context),
        child: Container(
            alignment: Alignment.centerLeft,
            decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.circular(SizeConfig.safeBlockHorizontal * 4),
              boxShadow: [
                BoxShadow(
                  color: Colors.black12,
                  blurRadius: 6.0,
                  offset: Offset(0, 2),
                ),
              ],
            ),
            child: Padding(
              padding: EdgeInsets.only(left: 0),
              child: AbsorbPointer(
                child: TextFormField(
                  controller: servicio,
                  keyboardType: TextInputType.text,
                  style: TextStyle(
                    color: Colors.black38,
                    fontSize: SizeConfig.safeBlockHorizontal * 4,
                  ),
                  decoration: InputDecoration(
                    contentPadding: EdgeInsets.all(SizeConfig.safeBlockVertical * 2),
                    border: InputBorder.none,
                    hintText: 'Buscar',
                    hintStyle: TextStyle(
                      color: Colors.black26,
                      fontSize: SizeConfig.safeBlockHorizontal * 4,
                    ),
                  ),
                ),
              ),
            ))
    );
  }

  Widget buildBuscarTF() {
    return Container(
      alignment: Alignment.centerLeft,
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.circular(SizeConfig.safeBlockHorizontal * 4),
        boxShadow: [
          BoxShadow(
            color: Colors.black12,
            blurRadius: 6.0,
            offset: Offset(0, 2),
          ),
        ],
      ),
      child: new TextField(
        controller: servicio,
        keyboardType: TextInputType.emailAddress,
        style: TextStyle(
          color: Colors.black38,
          fontFamily: 'OpenSans',
          fontSize: SizeConfig.safeBlockHorizontal * 4,
        ),
        decoration: new InputDecoration(
          border: InputBorder.none,
          contentPadding: EdgeInsets.all(SizeConfig.safeBlockVertical * 2),
          hintText: 'Buscar',
          hintStyle: TextStyle(
            color: Colors.black26,
            fontFamily: 'Opensans',
          ),
        ),
      ),
    );
  }

  Widget buildSocialBtn() {
    return GestureDetector(
      onTap: () {
      },
      child: Container(
        margin: EdgeInsets.only(left: SizeConfig.safeBlockHorizontal * 2.0),
        width: 150,
        height: 150,
        decoration: BoxDecoration(
          image: DecorationImage(
              image: AssetImage("assets/logos/logoPrincipal.png")),
        ),
      ),
    );
  }

  Widget buildBuscarBtn() {
    return GestureDetector(
      onTap: () {},
      child: Container(
        margin: EdgeInsets.only(right: SizeConfig.safeBlockHorizontal * 2),
        width: 20,
        height: 20,
        decoration: BoxDecoration(
          boxShadow: [
            BoxShadow(
              color: Colors.black26,
              blurRadius: 6.0,
              offset: Offset(0, 2),
            ),
          ],
          shape: BoxShape.circle,
          image: DecorationImage(
              image: AssetImage("assets/buscar.jpg"), fit: BoxFit.cover),
        ),
      ),
    );
  }

  Widget buildInicioBtn() {
    Map parametros = ModalRoute.of(context).settings.arguments;
    return GestureDetector(
      onTap: () {

        stopVerificarHoraServicio();
        appData.timer?.cancel();

        Navigator.pushNamed(context, "/inicio", arguments: {
          'id': parametros['id'],
          'nombre': parametros['nombre'],
          'username': parametros['username'],
          'password': parametros['password'],
        });
      },
      child: Container(
        margin: EdgeInsets.only(left: SizeConfig.safeBlockHorizontal * 1.0),
        width: 20,
        height: 20,
        decoration: BoxDecoration(
          shape: BoxShape.circle,
          image: DecorationImage(
              image: AssetImage("assets/inicio2.jpg"), fit: BoxFit.cover),
        ),
      ),
    );
  }

  Widget buildPerfilBtn() {
    Map parametros = ModalRoute.of(context).settings.arguments;
    return GestureDetector(
      onTap: () {
        Navigator.pushNamed(context, "/inicio/perfil", arguments: {
          'id': parametros['id'],
          'nombre': parametros['nombre'],
          'username': parametros['username'],
          'password': parametros['password'],
        });
      },
      child: Container(
        margin: EdgeInsets.only(left: SizeConfig.safeBlockHorizontal * 1.0),
        width: 20,
        height: 20,
        decoration: BoxDecoration(
          shape: BoxShape.circle,
          image: DecorationImage(
              image: AssetImage("assets/perfil_inicio.jpg"), fit: BoxFit.cover),
        ),
      ),
    );
  }

  Widget buildMapBtn() {
    return GestureDetector(
      onTap: () {
        showMyDialogSignOut();
      },
      child: Container(
        margin: EdgeInsets.only(left: SizeConfig.safeBlockHorizontal * 1.0),
        width: 20,
        height: 20,
        decoration: BoxDecoration(
          shape: BoxShape.circle,
          image: DecorationImage(
              image: AssetImage("assets/cerrar_inicio.jpg"), fit: BoxFit.cover),
        ),
      ),
    );
  }

  Widget buildInformationBtn() {
    Map parametros = ModalRoute.of(context).settings.arguments;
    return GestureDetector(
      onTap: () {
        Navigator.pushNamed(context, "/inicio/informacion", arguments: {
          'id': parametros['id'],
          'nombre': parametros['nombre'],
          'username': parametros['username'],
          'password': parametros['password'],
        });
      },
      child: Container(
        margin: EdgeInsets.only(left: SizeConfig.safeBlockHorizontal * 1.0),
        width: 20,
        height: 20,
        decoration: BoxDecoration(
          shape: BoxShape.circle,
          image: DecorationImage(
              image: AssetImage("assets/informacion.jpg"), fit: BoxFit.cover),
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    obtenerUsuario();
    Map parametros = ModalRoute.of(context).settings.arguments;
    SizeConfig().init(context);
    return new WillPopScope(
        onWillPop: () async => false,
        child: Scaffold(
          body: AnnotatedRegion<SystemUiOverlayStyle>(
            value: SystemUiOverlayStyle.light,
            child: GestureDetector(
              onTap: () => FocusScope.of(context).unfocus(),
              child: Stack(
                children: <Widget>[
                  Container(
                    height: double.infinity,
                    width: double.infinity,
                    color: Colors.white,
                  ),
                  SingleChildScrollView(
                    child: Center(
                      child: Container(
                        width: SizeConfig.safeBlockHorizontal * 100,
                        height: SizeConfig.blockSizeVertical * 100,
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: <Widget>[
                            Container(
                              width: SizeConfig.safeBlockHorizontal * 100,
                              height: SizeConfig.safeBlockVertical * 5,
                            ),
                            Container(
                              width: SizeConfig.safeBlockHorizontal * 100,
                              height: SizeConfig.safeBlockVertical * 1,
                            ),
                            Center(
                              child: Container(
                                width: SizeConfig.safeBlockHorizontal * 96,
                                height: SizeConfig.safeBlockVertical * 85.5,
                                child: Row(
                                  children: [
                                    Container(
                                      width: SizeConfig.safeBlockHorizontal * 2,
                                      height: SizeConfig.safeBlockVertical * 100,
                                    ),
                                    Container(
                                      width: SizeConfig.safeBlockHorizontal * 91,
                                      height: SizeConfig.safeBlockVertical * 100,
                                      decoration: BoxDecoration(
                                        color: Colors.white,
                                        borderRadius: BorderRadius.circular(SizeConfig.safeBlockHorizontal * 4),
                                          border: Border.all(
                                              width: 1,
                                              color: Colors.grey
                                          )
                                      ),
                                      child: Column(
                                        children: [
                                          Container(
                                            width: SizeConfig.safeBlockHorizontal * 100,
                                            height: SizeConfig.safeBlockVertical * 33,
                                              child: Column(
                                                children: [
                                                  Container(
                                                    width: SizeConfig.safeBlockHorizontal * 100,
                                                    height: SizeConfig.safeBlockVertical * 2,
                                                  ),
                                                  Container(
                                                    width: SizeConfig.safeBlockHorizontal * 100,
                                                    height: SizeConfig.safeBlockVertical * 29,
                                                    child: Row(
                                                      children: [
                                                        Container(
                                                          width: SizeConfig.safeBlockHorizontal * 4.5,
                                                          height: SizeConfig.safeBlockVertical * 100,
                                                        ),
                                                        Container(
                                                          width: SizeConfig.safeBlockHorizontal * 81,
                                                          height: SizeConfig.safeBlockVertical * 100,
                                                          decoration: BoxDecoration(
                                                            color: Colors.grey[100],
                                                            borderRadius: BorderRadius.circular(SizeConfig.safeBlockHorizontal * 4),
                                                          ),
                                                          child: Column(
                                                            children: [
                                                              Container(
                                                                width: SizeConfig.safeBlockHorizontal * 100,
                                                                height: SizeConfig.safeBlockVertical * 9,
                                                                child: Center(
                                                                  child: Padding(
                                                                    padding: EdgeInsets.only(top: SizeConfig.safeBlockVertical * 4),
                                                                    child: Text(
                                                                      "Informacion",
                                                                      style: TextStyle(
                                                                        color: Colors.black38,
                                                                        fontWeight: FontWeight.bold,
                                                                        fontFamily: 'OpenSans',
                                                                        fontSize: SizeConfig.safeBlockHorizontal * 5,
                                                                      ),
                                                                    ),
                                                                  ),
                                                                ),
                                                              ),
                                                              Container(
                                                                width: SizeConfig.safeBlockHorizontal * 100,
                                                                height: SizeConfig.safeBlockVertical * 20,
                                                                child: Center(
                                                                  child: Padding(
                                                                    padding: EdgeInsets.only(bottom: SizeConfig.safeBlockVertical * 3),
                                                                    child: buildSocialBtn(),
                                                                  )
                                                                ),
                                                              ),
                                                            ],
                                                          )
                                                        ),
                                                        Container(
                                                          width: SizeConfig.safeBlockHorizontal * 4.5,
                                                          height: SizeConfig.safeBlockVertical * 100,
                                                        ),
                                                      ],
                                                    ),
                                                  ),
                                                  Container(
                                                    width: SizeConfig.safeBlockHorizontal * 100,
                                                    height: SizeConfig.safeBlockVertical * 2,
                                                  ),
                                                ],
                                              )
                                          ),
                                          Container(
                                            width: SizeConfig.safeBlockHorizontal * 100,
                                            height: SizeConfig.safeBlockVertical * 1,
                                            child: Row(
                                              children: [
                                                Container(
                                                  width: SizeConfig.safeBlockHorizontal * 3.5,
                                                  height: SizeConfig.safeBlockVertical * 100,
                                                ),
                                                Container(
                                                  width: SizeConfig.safeBlockHorizontal * 83,
                                                  height: SizeConfig.safeBlockVertical * 100,
                                                  child: Column(
                                                    children: [
                                                      Container(
                                                        width: SizeConfig.safeBlockHorizontal * 100,
                                                        height: SizeConfig.safeBlockVertical * 0.8,
                                                      ),
                                                      Container(
                                                        width: SizeConfig.safeBlockHorizontal * 100,
                                                        height: SizeConfig.safeBlockVertical * 0.2,
                                                        color: Colors.grey[200],
                                                      ),
                                                    ],
                                                  ),
                                                ),
                                                Container(
                                                  width: SizeConfig.safeBlockHorizontal * 3.5,
                                                  height: SizeConfig.safeBlockVertical * 100,
                                                ),
                                              ],
                                            ),
                                          ),
                                          Container(
                                            width: SizeConfig.safeBlockHorizontal * 100,
                                            height: SizeConfig.safeBlockVertical * 3,
                                          ),
                                          Container(
                                            width: SizeConfig.safeBlockHorizontal * 100,
                                            height: SizeConfig.safeBlockVertical * 46.5,
                                            child: Column(
                                              children: [
                                                Container(
                                                    width: SizeConfig.safeBlockHorizontal * 81,
                                                    height: SizeConfig.safeBlockVertical * 20,
                                                    decoration: BoxDecoration(
                                                      color: Colors.white,
                                                      borderRadius: BorderRadius.circular(SizeConfig.safeBlockHorizontal * 4),
                                                        border: Border.all(
                                                            width: 1,
                                                            color: Colors.grey[300]
                                                        )
                                                    ),
                                                    child: Column(
                                                      children: [
                                                        Container(
                                                          width: SizeConfig.safeBlockHorizontal * 100,
                                                          height: SizeConfig.safeBlockVertical * 7,
                                                          child: Padding(
                                                            padding: EdgeInsets.only(top: SizeConfig.safeBlockVertical * 3, left: SizeConfig.safeBlockHorizontal * 4),
                                                            child: Text(
                                                              "Espega SAS.",
                                                              style: TextStyle(
                                                                color: Colors.black38,
                                                                fontWeight: FontWeight.bold,
                                                                fontFamily: 'OpenSans',
                                                                fontSize: SizeConfig.safeBlockHorizontal * 4,
                                                              ),
                                                            ),
                                                          ),
                                                        ),
                                                        Container(
                                                          width: SizeConfig.safeBlockHorizontal * 100,
                                                          height: SizeConfig.safeBlockVertical * 3.5,
                                                          child: Padding(
                                                            padding: EdgeInsets.only( left: SizeConfig.safeBlockHorizontal * 0),
                                                            child: Column(
                                                              children: [
                                                                Text(
                                                                  "Esta plicacion fue diseñada para hacerte",
                                                                  style: TextStyle(
                                                                    color: Colors.black26,
                                                                    fontWeight: FontWeight.bold,
                                                                    fontFamily: 'OpenSans',
                                                                    fontSize: SizeConfig.safeBlockHorizontal * 4,
                                                                  ),
                                                                ),
                                                              ],
                                                            )
                                                          ),
                                                        ),
                                                        Container(
                                                          width: SizeConfig.safeBlockHorizontal * 100,
                                                          height: SizeConfig.safeBlockVertical * 3.5,
                                                          child: Padding(
                                                              padding: EdgeInsets.only( right: SizeConfig.safeBlockHorizontal * 2),
                                                              child: Column(
                                                                children: [
                                                                  Text(
                                                                    "la vida mas facil. tiene Funcionalidades",
                                                                    style: TextStyle(
                                                                      color: Colors.black26,
                                                                      fontWeight: FontWeight.bold,
                                                                      fontFamily: 'OpenSans',
                                                                      fontSize: SizeConfig.safeBlockHorizontal * 4,
                                                                    ),
                                                                  ),
                                                                ],
                                                              )
                                                          ),
                                                        ),
                                                        Container(
                                                          width: SizeConfig.safeBlockHorizontal * 100,
                                                          height: SizeConfig.safeBlockVertical * 3.5,
                                                          child: Padding(
                                                              padding: EdgeInsets.only( right: SizeConfig.safeBlockHorizontal * 3),
                                                              child: Column(
                                                                children: [
                                                                  Text(
                                                                    "como pedir servicios entre otras cosas",
                                                                    style: TextStyle(
                                                                      color: Colors.black26,
                                                                      fontWeight: FontWeight.bold,
                                                                      fontFamily: 'OpenSans',
                                                                      fontSize: SizeConfig.safeBlockHorizontal * 4,
                                                                    ),
                                                                  ),
                                                                ],
                                                              )
                                                          ),
                                                        ),
                                                      ],
                                                    )
                                                ),
                                                Container(
                                                  width: SizeConfig.safeBlockHorizontal * 100,
                                                  height: SizeConfig.safeBlockVertical * 2,
                                                ),
                                                Container(
                                                    width: SizeConfig.safeBlockHorizontal * 81,
                                                    height: SizeConfig.safeBlockVertical * 20,
                                                    decoration: BoxDecoration(
                                                      color: Colors.white,
                                                      borderRadius: BorderRadius.circular(SizeConfig.safeBlockHorizontal * 4),
                                                        border: Border.all(
                                                            width: 1,
                                                            color: Colors.grey[300]
                                                        )
                                                    ),
                                                    child: Column(
                                                      mainAxisAlignment: MainAxisAlignment.start,
                                                      children: [
                                                        Container(
                                                          width: SizeConfig.safeBlockHorizontal * 100,
                                                          height: SizeConfig.safeBlockVertical * 7,
                                                          child: Padding(
                                                            padding: EdgeInsets.only(top: SizeConfig.safeBlockVertical * 3, left: SizeConfig.safeBlockHorizontal * 4),
                                                            child: Text(
                                                              "App",
                                                              style: TextStyle(
                                                                color: Colors.black38,
                                                                fontWeight: FontWeight.bold,
                                                                fontFamily: 'OpenSans',
                                                                fontSize: SizeConfig.safeBlockHorizontal * 4,
                                                              ),
                                                            ),
                                                          ),
                                                        ),
                                                        Container(
                                                          width: SizeConfig.safeBlockHorizontal * 100,
                                                          height: SizeConfig.safeBlockVertical * 3.5,
                                                          child: Padding(
                                                              padding: EdgeInsets.only( right: SizeConfig.safeBlockHorizontal * 30),
                                                              child: Column(
                                                                children: [
                                                                  Text(
                                                                    "Creada por la empresa :",
                                                                    style: TextStyle(
                                                                      color: Colors.black26,
                                                                      fontWeight: FontWeight.bold,
                                                                      fontFamily: 'OpenSans',
                                                                      fontSize: SizeConfig.safeBlockHorizontal * 4,
                                                                    ),
                                                                  ),
                                                                ],
                                                              )
                                                          ),
                                                        ),
                                                        Container(
                                                          width: SizeConfig.safeBlockHorizontal * 100,
                                                          height: SizeConfig.safeBlockVertical * 3.5,
                                                          child: Padding(
                                                              padding: EdgeInsets.only( right: SizeConfig.safeBlockHorizontal * 50),
                                                              child: Column(
                                                                children: [
                                                                  Text(
                                                                    "Kinnova Lab",
                                                                    style: TextStyle(
                                                                      color: Colors.black26,
                                                                      fontWeight: FontWeight.bold,
                                                                      fontFamily: 'OpenSans',
                                                                      fontSize: SizeConfig.safeBlockHorizontal * 4,
                                                                    ),
                                                                  ),
                                                                ],
                                                              )
                                                          ),
                                                        ),
                                                        Container(
                                                          width: SizeConfig.safeBlockHorizontal * 100,
                                                          height: SizeConfig.safeBlockVertical * 3.5,
                                                          child: Padding(
                                                              padding: EdgeInsets.only( right: SizeConfig.safeBlockHorizontal * 22),
                                                              child: Column(
                                                                children: [
                                                                  Text(
                                                                    "Autores principales:  Equipo",
                                                                    style: TextStyle(
                                                                      color: Colors.black26,
                                                                      fontWeight: FontWeight.bold,
                                                                      fontFamily: 'OpenSans',
                                                                      fontSize: SizeConfig.safeBlockHorizontal * 4,
                                                                    ),
                                                                  ),
                                                                ],
                                                              )
                                                          ),
                                                        ),
                                                      ],
                                                    )
                                                ),
                                              ],
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),
                                    Container(
                                      width: SizeConfig.safeBlockHorizontal * 2,
                                      height: SizeConfig.safeBlockVertical * 100,
                                    ),
                                  ],
                                ),
                              ),
                            ),
                            Container(
                              width: SizeConfig.safeBlockHorizontal * 100,
                              height: SizeConfig.safeBlockVertical * 1,
                            ),
                            Center(
                              child: Container(
                                width: SizeConfig.safeBlockHorizontal * 98,
                                height: SizeConfig.safeBlockVertical * 11,
                                decoration: BoxDecoration(
                                  color: Colors.blue[200],
                                  borderRadius: BorderRadius.circular(SizeConfig.safeBlockHorizontal * 4),
                                  boxShadow: [
                                    BoxShadow(
                                      color: Colors.black12,
                                      blurRadius: 6.0,
                                      offset: Offset(0, 2),
                                    ),
                                  ],
                                ),
                                child: Column(
                                  children: [
                                    Container(
                                      width: SizeConfig.safeBlockHorizontal * 100,
                                      height: SizeConfig.safeBlockVertical * 2,
                                    ),
                                    Container(
                                      width: SizeConfig.safeBlockHorizontal * 100,
                                      height: SizeConfig.safeBlockVertical * 7,
                                      child: Row(
                                        children: [
                                          Container(
                                            width: SizeConfig.safeBlockHorizontal * 11,
                                            height: SizeConfig.safeBlockVertical * 100,
                                          ),
                                          Container(
                                            width: SizeConfig.safeBlockHorizontal * 16,
                                            height: SizeConfig.safeBlockVertical * 100,
                                            child: buildInicioBtn(),
                                          ),
                                          Container(
                                            width: SizeConfig.safeBlockHorizontal * 4,
                                            height: SizeConfig.safeBlockVertical * 100,
                                          ),
                                          Container(
                                            width: SizeConfig.safeBlockHorizontal * 16,
                                            height: SizeConfig.safeBlockVertical * 100,
                                            child: buildPerfilBtn(),
                                          ),
                                          Container(
                                            width: SizeConfig.safeBlockHorizontal * 4,
                                            height: SizeConfig.safeBlockVertical * 100,
                                          ),
                                          Container(
                                            width: SizeConfig.safeBlockHorizontal * 16,
                                            height: SizeConfig.safeBlockVertical * 100,
                                            child: buildInformationBtn(),
                                          ),
                                          Container(
                                            width: SizeConfig.safeBlockHorizontal * 4,
                                            height: SizeConfig.safeBlockVertical * 100,
                                          ),
                                          Container(
                                            width: SizeConfig.safeBlockHorizontal * 16,
                                            height: SizeConfig.safeBlockVertical * 100,
                                            child: buildMapBtn(),
                                          ),
                                          Container(
                                            width: SizeConfig.safeBlockHorizontal * 11,
                                            height: SizeConfig.safeBlockVertical * 100,
                                          ),
                                        ],
                                      ),
                                    ),
                                    Container(
                                      width: SizeConfig.safeBlockHorizontal * 100,
                                      height: SizeConfig.safeBlockVertical * 2,
                                    ),
                                  ],
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
        ));
  }
}

extension StringExtension on String {
  String capitalize2() {
    return "${this[0].toUpperCase()}${this.substring(1)}";
  }
}