import 'dart:async';
import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter_map/flutter_map.dart';
import 'package:conductor/helpers/AppData.dart';
import 'package:conductor/pages/usuario/VerificarHoraServicio.dart';
import 'package:geocoder/geocoder.dart';
import 'package:latlong/latlong.dart';
import 'package:http/http.dart' as http;

import '../../SizeConfig.dart';
import 'ActualizarUbicacion.dart';

class MapaConductor extends StatefulWidget {
  @override
  _MapaConductorState createState() => _MapaConductorState();
}

class _MapaConductorState extends State<MapaConductor> {


  var ubicacionMapa;

  Future obtenerConductor() async {
    Map parametros = ModalRoute.of(context).settings.arguments;
    final url = "http://transporte-paciente.herokuapp.com/api/conductor/login";
    var data = {
      "username": parametros['username'],
      "password": parametros['password'],
    };
    final response = await http.post(url, body: data);
    return jsonDecode(response.body);
  }

  Future obtenerUsuario(nombre, dired, fecha_hora, descripcion) async {

    var informacionUsuario = {
      "nombre" : nombre,
      "dired" : dired,
      "fecha_hora" : fecha_hora,
      "descripcion" : descripcion
    };

    var informacionUsuarioEncode = jsonEncode(informacionUsuario);
    var informacionUsuarioDecode = jsonDecode(informacionUsuarioEncode);

    return informacionUsuarioDecode;

  }

  Future<void> _showMyDialogConductor(lat, lon) async {

    final coordinates = new Coordinates(double.parse(lat), double.parse(lon));
    var addresses = await Geocoder.local.findAddressesFromCoordinates(coordinates);
    var first = addresses.first;
    var localizacion = first.addressLine.split(",");

    return showDialog<void>(
      context: context,
      barrierDismissible: false, // user must tap button!
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text(
            'Conductor',
            style: TextStyle(
              color: Colors.grey[600],
              fontFamily: 'OpenSans',
              fontSize: SizeConfig.safeBlockHorizontal * 5.5,
            ),
          ),
          content: SingleChildScrollView(
            child: FutureBuilder(
                future: obtenerConductor(),
                builder: (context, snapshot) {
                  if (snapshot.hasError) print(snapshot.error);
                  return snapshot.hasData
                      ? new getByIdConductor(
                        list: snapshot.data,
                        pais: localizacion[3],
                        departamento: localizacion[2],
                        ciudad: localizacion[1],
                        direccion: localizacion[0]
                  )
                      : new Container(
                    width: SizeConfig.safeBlockHorizontal * 100,
                    height: SizeConfig.safeBlockVertical * 6,
                    child: Row(
                      children: [
                        Container(
                          width: SizeConfig.safeBlockHorizontal * 27,
                          height: SizeConfig.safeBlockVertical * 100,
                        ),
                        Container(
                          width: SizeConfig.safeBlockHorizontal * 10,
                          height: SizeConfig.safeBlockVertical * 100,
                          child: CircularProgressIndicator(),
                        ),
                        Container(
                          width: SizeConfig.safeBlockHorizontal * 27,
                          height: SizeConfig.safeBlockVertical * 100,
                        ),
                      ],
                    ),
                  );
                }),
          ),
          actions: <Widget>[
            TextButton(
              child: Text(
                'Aceptar',
                style: TextStyle(
                  color: Colors.grey[600],
                  fontFamily: 'OpenSans',
                ),
              ),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }

  Future<void> _showMyDialogUsuario(nombre, dired, fecha_hora, descripcion, lat, lon) async {

    final coordinates = new Coordinates(double.parse(lat), double.parse(lon));
    var addresses = await Geocoder.local.findAddressesFromCoordinates(coordinates);
    var first = addresses.first;
    var localizacion = first.addressLine.split(",");

    return showDialog<void>(
      context: context,
      barrierDismissible: false, // user must tap button!
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text(
            'Usuario',
            style: TextStyle(
              color: Colors.grey[600],
              fontFamily: 'OpenSans',
              fontSize: SizeConfig.safeBlockHorizontal * 5.5,
            ),
          ),
          content: SingleChildScrollView(
            child: FutureBuilder(
                future: obtenerUsuario(nombre, dired, fecha_hora, descripcion),
                builder: (context, snapshot) {
                  if (snapshot.hasError) print(snapshot.error);
                  return snapshot.hasData
                      ? new getByIdUsuario(
                      list: snapshot.data,
                      pais: localizacion[3],
                      departamento: localizacion[2],
                      ciudad: localizacion[1],
                      direccion: localizacion[0]

                  )
                      : new Container(
                    width: SizeConfig.safeBlockHorizontal * 100,
                    height: SizeConfig.safeBlockVertical * 6,
                    child: Row(
                      children: [
                        Container(
                          width: SizeConfig.safeBlockHorizontal * 27,
                          height: SizeConfig.safeBlockVertical * 100,
                        ),
                        Container(
                          width: SizeConfig.safeBlockHorizontal * 10,
                          height: SizeConfig.safeBlockVertical * 100,
                          child: CircularProgressIndicator(),
                        ),
                        Container(
                          width: SizeConfig.safeBlockHorizontal * 27,
                          height: SizeConfig.safeBlockVertical * 100,
                        ),
                      ],
                    ),
                  );
                }),
          ),
          actions: <Widget>[
            TextButton(
              child: Text(
                'Aceptar',
                style: TextStyle(
                  color: Colors.grey[600],
                  fontFamily: 'OpenSans',
                ),
              ),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }

  Future obtenerUbicacion() async {

    print(appData.servicios_paciente);
    var servicios = await ActualizarUbicacionUsuario(appData.servicios_paciente);
    servicios.listen((data) {
      if(mounted) setState(() {
        ubicacionMapa = jsonDecode(data);
      });
    });

  }

  void initState() {
    // TODO: implement initState

    WidgetsBinding.instance.addPostFrameCallback((_) async {
      if(mounted) setState(() {
        obtenerUbicacion();
      });
    });

  }

  @override
  Widget build(BuildContext context) {
    if(ubicacionMapa == null){
      return Scaffold(
        body: Container(
          width: SizeConfig.blockSizeHorizontal* 100,
          height: SizeConfig.blockSizeVertical * 100,
          color: Colors.white,
              child: Column(
                children: [
                  Container(
                    width: SizeConfig.blockSizeHorizontal* 100,
                    height: SizeConfig.blockSizeVertical * 20,
                  ),
                  Container(
                    width: SizeConfig.blockSizeHorizontal* 100,
                    height: SizeConfig.blockSizeVertical * 10,
                    child: Row(
                      children: [
                        Container(
                          width: SizeConfig.blockSizeHorizontal* 15,
                          height: SizeConfig.blockSizeVertical * 100,
                        ),
                        Container(
                          width: SizeConfig.blockSizeHorizontal* 70,
                          height: SizeConfig.blockSizeVertical * 100,
                          child: Center(
                            child: Text(
                              "Podras ver los movimientos del paciente 30 min antes de la hora estipulada",
                              style: TextStyle(
                                color: Colors.black38,
                                fontWeight: FontWeight.bold,
                                fontFamily: 'OpenSans',
                                fontSize: SizeConfig.safeBlockHorizontal * 4.5,
                              ),
                            ),
                          ),
                        ),
                        Container(
                          width: SizeConfig.blockSizeHorizontal* 15,
                          height: SizeConfig.blockSizeVertical * 100,
                        ),
                      ],
                    ),
                  ),
                  Container(
                    width: SizeConfig.blockSizeHorizontal* 100,
                    height: SizeConfig.blockSizeVertical * 20,
                  ),
                  Container(
                    width: SizeConfig.blockSizeHorizontal* 100,
                    height: SizeConfig.blockSizeVertical * 10,
                    child: Center(
                      child: CircularProgressIndicator(
                        backgroundColor: Colors.black12,
                      ),
                    ),
                  ),
                  Container(
                    width: SizeConfig.blockSizeHorizontal* 100,
                    height: SizeConfig.blockSizeVertical * 40,
                  ),
                ],
              )
          ),
      );
    }else{
      String element;
      int aumentador = 1;
      bool verificador = false;
      element = ubicacionMapa.toString();
      for(int i = 0; i < element.length; i++){
        String caracter = "";
        caracter = element.substring(i, aumentador);
        if(caracter == "["){
          verificador = true;
        }
        aumentador++;
      }
      Map parametros = ModalRoute.of(context).settings.arguments;
      SizeConfig().init(context);
      return Scaffold(
        floatingActionButton: Padding(
          padding: EdgeInsets.only(
              bottom: SizeConfig.safeBlockVertical * 3,
              left: SizeConfig.safeBlockHorizontal * 50
          ),
          child: FloatingActionButton(
            heroTag: "btn1",
            backgroundColor: Colors.white,
            child: Container(
              width: SizeConfig.safeBlockVertical * 25,
              height: SizeConfig.safeBlockHorizontal * 25,
              decoration: BoxDecoration(
                shape: BoxShape.circle,
                image: DecorationImage(
                    image: AssetImage("assets/flecha1.jpg"), fit: BoxFit.cover),
              ),
            ),
            onPressed: () => {

              stopActualizarUbicacionUsuario(),
              stopVerificarHoraServicio(),
              appData.timer?.cancel(),

              Navigator.pushNamed(context, "/inicio", arguments: {
                'id': parametros['id'],
                'nombre': parametros['nombre'],
                'username': parametros['username'],
                'password': parametros['password']
              })
            },
          ),
        ),
        body: FlutterMap(
          options: MapOptions(
            center: LatLng(double.parse(appData.ubicacion_principal_lat), double.parse(appData.ubicacion_principal_lng)),
            zoom: SizeConfig.safeBlockHorizontal * 2.0,
          ),
          layers: [
            TileLayerOptions(
                urlTemplate: "https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png",
                subdomains: ['a', 'b', 'c']
            ),
            MarkerLayerOptions(
              markers: [
                if(verificador == true)...[
                  for(int i = 0; i < ubicacionMapa.length ?? 0; i++)...[
                    Marker(
                        width: 70.0,
                        height: 70.0,
                        point: LatLng(double.parse(ubicacionMapa[i]["lat"].toString()), double.parse(ubicacionMapa[i]["lon"].toString())),
                        builder: (ctx) =>
                            Container(
                              child: GestureDetector(
                                  onTap: () {

                                    _showMyDialogUsuario(

                                        ubicacionMapa[i]["nom_paciente"].toString(),
                                        ubicacionMapa[i]["direccion_llegada"].toString(),
                                        ubicacionMapa[i]["fecha_hora_servicio"].toString(),
                                        ubicacionMapa[i]["descripcion"].toString(),
                                        ubicacionMapa[i]["lat"].toString(),
                                        ubicacionMapa[i]["lon"].toString()
                                    );
                                    },
                                  child: new Image.asset("assets/logos/usuario.png")
                              ),
                            )
                    ),
                  ],
                ],
                Marker(
                    width: 70.0,
                    height: 70.0,
                    point: LatLng(double.parse(appData.ubicacion_principal_lat), double.parse(appData.ubicacion_principal_lng)),
                    builder: (ctx) =>
                        Container(
                          child: GestureDetector(
                              onTap:() {
                                _showMyDialogConductor(
                                    appData.ubicacion_principal_lat.toString(),
                                    appData.ubicacion_principal_lng.toString());
                              },
                              child: new Image.asset("assets/logos/conductor.png")
                          ),
                        )
                ),
              ],
            ),
          ],
        ),
      );
    }
  }
}

class getByIdConductor extends StatefulWidget {

  final list;
  var pais;
  var departamento;
  var ciudad;
  var direccion;

  getByIdConductor({this.list, this.pais, this.departamento, this.ciudad, this.direccion});

  @override
  _getByIdConductorState createState() => _getByIdConductorState();
}

class _getByIdConductorState extends State<getByIdConductor> {
  @override
  Widget build(BuildContext context) {
    String nombre = widget.list['nombre'];
    String contNombre = nombre.toLowerCase();
    String nombreDef = contNombre.capitalize2();
    return ListBody(
      children: <Widget>[
        Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Container(
              child: Column(
                children: [
                  Text(
                    "Nombre del conductor",
                    style: TextStyle(
                      color: Colors.grey,
                      fontFamily: 'OpenSans',
                      fontWeight: FontWeight.bold,
                      fontSize: SizeConfig.safeBlockHorizontal * 4,
                    ),
                  ),
                  Text(
                    nombreDef,
                    style: TextStyle(
                      color: Colors.grey,
                      fontFamily: 'OpenSans',
                      fontSize: SizeConfig.safeBlockHorizontal * 4,
                    ),
                  ),
                ],
              ),
            ),
            SizedBox(
              height: SizeConfig.blockSizeVertical * 2,
            ),
            Container(
              child: Column(
                children: [
                  Text(
                    "Celular",
                    style: TextStyle(
                      color: Colors.grey,
                      fontFamily: 'OpenSans',
                      fontWeight: FontWeight.bold,
                      fontSize: SizeConfig.safeBlockHorizontal * 4,
                    ),
                  ),
                  Text(
                    widget.list['movil'].toString(),
                    style: TextStyle(
                      color: Colors.grey,
                      fontFamily: 'OpenSans',
                      fontSize: SizeConfig.safeBlockHorizontal * 4,
                    ),
                  ),
                ],
              ),
            ),
            SizedBox(
              height: SizeConfig.blockSizeVertical * 2,
            ),
            Container(
              child: Column(
                children: [
                  Text(
                    "Documento",
                    style: TextStyle(
                      color: Colors.grey,
                      fontFamily: 'OpenSans',
                      fontWeight: FontWeight.bold,
                      fontSize: SizeConfig.safeBlockHorizontal * 4,
                    ),
                  ),
                  Text(
                    widget.list['documento'].toString(),
                    style: TextStyle(
                      color: Colors.grey,
                      fontFamily: 'OpenSans',
                      fontSize: SizeConfig.safeBlockHorizontal * 4,
                    ),
                  ),
                ],
              ),
            ),
            SizedBox(
              height: SizeConfig.blockSizeVertical * 2,
            ),
            Container(
              child: Column(
                children: [
                  Text(
                    "Direccion",
                    style: TextStyle(
                      color: Colors.grey,
                      fontFamily: 'OpenSans',
                      fontWeight: FontWeight.bold,
                      fontSize: SizeConfig.safeBlockHorizontal * 4,
                    ),
                  ),
                  Text(
                    widget.direccion.toString(),
                    style: TextStyle(
                      color: Colors.grey,
                      fontFamily: 'OpenSans',
                      fontSize: SizeConfig.safeBlockHorizontal * 4,
                    ),
                  ),
                ],
              ),
            ),
            SizedBox(
              height: SizeConfig.blockSizeVertical * 2,
            ),
            Container(
              child: Column(
                children: [
                  Text(
                    "Pais",
                    style: TextStyle(
                      color: Colors.grey,
                      fontFamily: 'OpenSans',
                      fontWeight: FontWeight.bold,
                      fontSize: SizeConfig.safeBlockHorizontal * 4,
                    ),
                  ),
                  Text(
                    widget.pais.toString(),
                    style: TextStyle(
                      color: Colors.grey,
                      fontFamily: 'OpenSans',
                      fontSize: SizeConfig.safeBlockHorizontal * 4,
                    ),
                  ),
                ],
              ),
            ),
            SizedBox(
              height: SizeConfig.blockSizeVertical * 2,
            ),
            Container(
              child: Column(
                children: [
                  Text(
                    "Departamento",
                    style: TextStyle(
                      color: Colors.grey,
                      fontFamily: 'OpenSans',
                      fontWeight: FontWeight.bold,
                      fontSize: SizeConfig.safeBlockHorizontal * 4,
                    ),
                  ),
                  Text(
                    widget.departamento.toString(),
                    style: TextStyle(
                      color: Colors.grey,
                      fontFamily: 'OpenSans',
                      fontSize: SizeConfig.safeBlockHorizontal * 4,
                    ),
                  ),
                ],
              ),
            ),
            SizedBox(
              height: SizeConfig.blockSizeVertical * 2,
            ),
            Container(
              child: Column(
                children: [
                  Text(
                    "Ciudad",
                    style: TextStyle(
                      color: Colors.grey,
                      fontFamily: 'OpenSans',
                      fontWeight: FontWeight.bold,
                      fontSize: SizeConfig.safeBlockHorizontal * 4,
                    ),
                  ),
                  Text(
                    widget.ciudad.toString(),
                    style: TextStyle(
                      color: Colors.grey,
                      fontFamily: 'OpenSans',
                      fontSize: SizeConfig.safeBlockHorizontal * 4,
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ],
    );
  }
}


class getByIdUsuario extends StatefulWidget {

  final list;
  var pais;
  var departamento;
  var ciudad;
  var direccion;

  getByIdUsuario({this.list, this.pais, this.departamento, this.ciudad, this.direccion});
  @override
  _getByIdUsuarioState createState() => _getByIdUsuarioState();
}

class _getByIdUsuarioState extends State<getByIdUsuario> {

  @override
  Widget build(BuildContext context) {
    String nombre = widget.list['nombre'];
    String contNombre = nombre.toLowerCase();
    String nombreDef = contNombre.capitalize2();
    return ListBody(
      children: <Widget>[
        Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Container(
              child: Column(
                children: [
                  Text(
                    "Nombre del usuario",
                    style: TextStyle(
                      color: Colors.grey,
                      fontFamily: 'OpenSans',
                      fontWeight: FontWeight.bold,
                      fontSize: SizeConfig.safeBlockHorizontal * 4,
                    ),
                  ),
                  Text(
                    nombreDef,
                    style: TextStyle(
                      color: Colors.grey,
                      fontFamily: 'OpenSans',
                      fontSize: SizeConfig.safeBlockHorizontal * 4,
                    ),
                  ),
                ],
              ),
            ),
            SizedBox(
              height: SizeConfig.blockSizeVertical * 2,
            ),
            Container(
              child: Column(
                children: [
                  Text(
                    "Direccion Destino",
                    style: TextStyle(
                      color: Colors.grey,
                      fontFamily: 'OpenSans',
                      fontWeight: FontWeight.bold,
                      fontSize: SizeConfig.safeBlockHorizontal * 4,
                    ),
                  ),
                  Text(

                    widget.list['dired'].toString(),
                    style: TextStyle(
                      color: Colors.grey,
                      fontFamily: 'OpenSans',
                      fontSize: SizeConfig.safeBlockHorizontal * 4,
                    ),
                  ),
                ],
              ),
            ),
            SizedBox(
              height: SizeConfig.blockSizeVertical * 2,
            ),
            Container(
              child: Column(
                children: [
                  Text(
                    "Fecha y Horario",
                    style: TextStyle(
                      color: Colors.grey,
                      fontFamily: 'OpenSans',
                      fontWeight: FontWeight.bold,
                      fontSize: SizeConfig.safeBlockHorizontal * 4,
                    ),
                  ),
                  Text(

                    widget.list['fecha_hora'].toString(),
                    style: TextStyle(
                      color: Colors.grey,
                      fontFamily: 'OpenSans',
                      fontSize: SizeConfig.safeBlockHorizontal * 4,
                    ),
                  ),
                ],
              ),
            ),
            SizedBox(
              height: SizeConfig.blockSizeVertical * 2,
            ),
            Container(
              child: Column(
                children: [
                  Text(
                    "Descripcion",
                    style: TextStyle(
                      color: Colors.grey,
                      fontFamily: 'OpenSans',
                      fontWeight: FontWeight.bold,
                      fontSize: SizeConfig.safeBlockHorizontal * 4,
                    ),
                  ),
                  Text(

                    widget.list['descripcion'].toString(),
                    style: TextStyle(
                      color: Colors.grey,
                      fontFamily: 'OpenSans',
                      fontSize: SizeConfig.safeBlockHorizontal * 4,
                    ),
                  ),
                ],
              ),
            ),
            SizedBox(
              height: SizeConfig.blockSizeVertical * 2,
            ),
            Container(
              child: Column(
                children: [
                  Text(
                    "Direccion Salidad",
                    style: TextStyle(
                      color: Colors.grey,
                      fontFamily: 'OpenSans',
                      fontWeight: FontWeight.bold,
                      fontSize: SizeConfig.safeBlockHorizontal * 4,
                    ),
                  ),
                  Text(
                    widget.direccion.toString(),
                    style: TextStyle(
                      color: Colors.grey,
                      fontFamily: 'OpenSans',
                      fontSize: SizeConfig.safeBlockHorizontal * 4,
                    ),
                  ),
                ],
              ),
            ),
            SizedBox(
              height: SizeConfig.blockSizeVertical * 2,
            ),
            Container(
              child: Column(
                children: [
                  Text(
                    "Pais",
                    style: TextStyle(
                      color: Colors.grey,
                      fontFamily: 'OpenSans',
                      fontWeight: FontWeight.bold,
                      fontSize: SizeConfig.safeBlockHorizontal * 4,
                    ),
                  ),
                  Text(
                    widget.pais.toString(),
                    style: TextStyle(
                      color: Colors.grey,
                      fontFamily: 'OpenSans',
                      fontSize: SizeConfig.safeBlockHorizontal * 4,
                    ),
                  ),
                ],
              ),
            ),
            SizedBox(
              height: SizeConfig.blockSizeVertical * 2,
            ),
            Container(
              child: Column(
                children: [
                  Text(
                    "Departamento",
                    style: TextStyle(
                      color: Colors.grey,
                      fontFamily: 'OpenSans',
                      fontWeight: FontWeight.bold,
                      fontSize: SizeConfig.safeBlockHorizontal * 4,
                    ),
                  ),
                  Text(
                    widget.departamento.toString(),
                    style: TextStyle(
                      color: Colors.grey,
                      fontFamily: 'OpenSans',
                      fontSize: SizeConfig.safeBlockHorizontal * 4,
                    ),
                  ),
                ],
              ),
            ),
            SizedBox(
              height: SizeConfig.blockSizeVertical * 2,
            ),
            Container(
              child: Column(
                children: [
                  Text(
                    "Ciudad",
                    style: TextStyle(
                      color: Colors.grey,
                      fontFamily: 'OpenSans',
                      fontWeight: FontWeight.bold,
                      fontSize: SizeConfig.safeBlockHorizontal * 4,
                    ),
                  ),
                  Text(
                    widget.ciudad.toString(),
                    style: TextStyle(
                      color: Colors.grey,
                      fontFamily: 'OpenSans',
                      fontSize: SizeConfig.safeBlockHorizontal * 4,
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ],
    );
  }
}

extension StringExtension on String {
  String capitalize2() {
    return "${this[0].toUpperCase()}${this.substring(1)}";
  }
}