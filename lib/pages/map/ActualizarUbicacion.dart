import 'dart:async';
import 'dart:convert';
import 'dart:isolate';
import 'package:http/http.dart' as http;

Isolate _isolate;
Timer _timer;

Future<ReceivePort> ActualizarUbicacionUsuario(serviciosUbicacion) async {
  ReceivePort receivePort = new ReceivePort();
  _isolate = await Isolate.spawn(
      obtenerUbicacionUsuario, [
    receivePort.sendPort,
    serviciosUbicacion
  ]);
  return receivePort;
}

stopActualizarUbicacionUsuario() {
  if(_isolate != null){
    _isolate.kill(priority: Isolate.immediate);
  }
}

void ConsultarUbicacionUsuario(sendPort, serviciosUbicar) async {

  var serviciosEncode = jsonEncode(serviciosUbicar);

  sendPort.send(serviciosEncode);

}

Future<void> obtenerUbicacionUsuario(List params) async {

  SendPort sendPort = params[0];
  var serviciosUbicar = params[1];


  _timer = Timer.periodic(Duration(seconds: 3), (timer) {
    ConsultarUbicacionUsuario(sendPort, serviciosUbicar);
  });

}