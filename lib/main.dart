import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'pages/info/informacion.dart';
import 'pages/map/mapa.dart';
import 'pages/servicio/listarServicio.dart';
import 'pages/splashScreen.dart';
import 'pages/usuario/Perfil.dart';
import 'pages/usuario/inicio.dart';
import 'pages/usuario/login.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return new MaterialApp(
        debugShowCheckedModeBanner: false,
        title: 'EspegaApp Conductor',
        theme: new ThemeData(
          primarySwatch: Colors.blue,
          fontFamily: 'OpenSans',
          splashColor: Colors.blue[800],
          visualDensity: VisualDensity.adaptivePlatformDensity,
        ),
        home: SplashScreenPage(),
        routes: <String, WidgetBuilder>{
          "/login": (BuildContext context) => new LoginScreen(),
          "/inicio": (BuildContext context) => InicioScreen(),
          "/inicio/map": (BuildContext context) => MapaConductor(),
          "/inicio/servicio": (BuildContext context) => ListarServicioScreen(),
          "/inicio/informacion": (BuildContext context) => InformacionScreen(),
          "/inicio/perfil": (BuildContext context) => Perfil(),
        });
  }
}

